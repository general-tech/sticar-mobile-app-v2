package sti_car.com.sticar;

import android.app.Application;
import android.content.Context;

import com.sti_car.rschsticar10.R;

import sti_car.com.sticar.DaggerAppComponent;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by femmy on 08/12/2017.
 */

public class App extends Application {

    protected AppComponent appComponent;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath("fonts/Cairo-Regular.ttf").setFontAttrId(R.attr.fontPath).build());
    }



    public AppComponent getComponent(){
        if(appComponent == null){
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .build();
        }
        return appComponent;
    }


}
