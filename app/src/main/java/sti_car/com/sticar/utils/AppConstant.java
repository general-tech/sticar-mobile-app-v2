package sti_car.com.sticar.utils;

import android.Manifest;

/**
 * Created by femmy on 09/12/2017.
 */

public class AppConstant {

    public static final String APP_TAG = "APPTAG-STICAR-----";

    public static final boolean APP_IS_PRODUCTION = false;

    public static final String APP_LANGUAGE = "en";

    public static final String SHARED_PREF_KEY = "session_pref";

    public static boolean IS_DRIVER = true;

    public static final String SESSION_TYPE_DRIVER = "DRIVER";

    public static final int NETWORK_SERIVICE_PERIOD = 20000;

    public static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE };

    public static final String INTENT_FILTER_KEY_GPS_EVENT = "service-gps-event";

    //TRACK SERVICE
    public static final String TRACK_SERIVCE_MSG_KEY_UPDATE_DISTANCE = "distance";

    public static final String TRACK_SERIVCE_MSG_KEY_UPDATE_CREDIT = "credit";

    public static final String TRACK_SERIVCE_MSG_KEY_UPDATE_TOTAL = "updateTotal";

    public static final String TRACK_SERIVCE_MSG_KEY_UPDATE_TOTAL_TODAY = "UPDATE_TOTAL_TODAY";

    public static final String TRACK_SERIVCE_MSG_KEY_UPDATE_TOTAL_TODAY_DISTANCE = "UPDATE_TOTAL_TODAY_DISTANCE";

    public static final String TRACK_SERIVCE_MSG_KEY_UPDATE_TOTAL_TODAY_CREDIT = "UPDATE_TOTAL_TODAY_CREDIT";

    public static final String TRACK_SERIVCE_MSG_KEY_UPDATE_TOTAL_CURRENT = "UPDATE_TOTAL_CURRENT";

    public static final String TRACK_SERIVCE_MSG_KEY_UPDATE = "update";

    public static final String TRACK_SERIVCE_MSG_KEY_DAILY_LIMIT = "dailyLimit";

    public static final String TRACK_SERIVCE_MSG_KEY_UPDATE_LIMIT = "updateLimit";

    public static final String TRACK_SERVICE_NOTIFICATION_CHANNEL = "sticar_notification_compat_channel";

    public static final int TRACK_SERVICE_NOTIFICATION_TRIP_START_STOP_ID = 999;

    public static final int TRACK_SERVICE_NOTIFICATION_ALERT_ID = 998;


    // ERROR MESSAGE
    public static final String ERROR_MSG_INTENET_NOT_CONNECTED = "Not connected to internet";

    public static final String EXTRA_CHECK_ACTIVATION_KEY = "activate";

    public static final String EXTRA_CHECK_ACTIVATION_VALUE = "activate";

    public static final String EXTRA_REGISTER_KEY = "TAG_REGISTER";

    public static final String FLAG_TRIP_STARTED = "tripStart";

    public static final String FLAG_TRIP_STOPPED = "tripStop";

    public static final String EXTRA_ADS_KEY = "ADVERTISER";

    public static final String EXTRA_MESSAGES_KEY = "MESSAGES";

    public static final String EXTRA_UNPOSTED_TRIP_TRIPUUID = "unposted_trip_tripUUID";

    public static final String EXTRA_UNPOSTED_TRIP_ARRAY_TRACKS = "unposted_trip_arraytrack";

    public static final int NOTIFICATION_DELETE_NOTIF = 99;

    public static final String NOTIFICATION_NOTIF = "notification";

    public static final String EXTRA_KEY_VEHICLE_TYPE = "vehicle_type";

    public static final String EXTRA_VEHICLE_TYPE_CAR = "vehicle_type_car";

    public static final String EXTRA_VEHICLE_TYPE_MOTOR = "vehicle_type_motor";

    public static final String EXTRA_VEHICLE_TYPE_BOTH = "vehicle_type_both";

    public static final String EXTRA_CAMPAIGN_TYPE_KEY = "campaign_type";

    public static final String EXTRA_NOTIFICATION_UUID_KEY = "notification_uuid";

    public static final int EXTRA_CAMPAIGN_TYPE_CAR = 1;

    public static final int EXTRA_CAMPAIGN_TYPE_MOTOR = 2;


    // HERE MAPS
    public static final int HERE_MAPS_DEFAULT_ZOOM_LEVEL = 17;
    public static final Double HERE_MAPS_DEFAULT_LATITUDE = -6.1753924;
    public static final Double HERE_MAPS_DEFAULT_LONGITUDE = 106.8249641;
}
