package sti_car.com.sticar.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import sti_car.com.sticar.services.ConnectivityReceiver;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by femmy on 14/12/2017.
 */

public class AppUtil {

    public static int getPeriod() {
        int period = 1000; // 1 s
        return period;
    }

    public static int getDistancePeriod() {
        int distance = 10;
        return distance;
    }

    public static int CurrencyToInteger(String curr){
        if(!curr.equalsIgnoreCase("")){
            DecimalFormat decimalFormat     = new DecimalFormat("#,##0");
            String numberAsString           = decimalFormat.format(Double.valueOf(curr));
            String ys                       = numberAsString.replace(",", "");
            int num                         = Integer.parseInt(ys);
            return num;
        } else {
            return 0;
        }

    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);


    }

    public static long getDifferenceDays(Date d1, Date d2) {
        Log.d(AppConstant.APP_TAG, "D1 : "+d1.getTime()+" - D2 : "+d2.getTime());
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static final String formatDistance(Double val) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("");
        dfs.setGroupingSeparator('.');
        dfs.setMonetaryDecimalSeparator('.');
        ((DecimalFormat) nf).setDecimalFormatSymbols(dfs);
        nf.setMinimumFractionDigits(1);
        return nf.format(val);
    }

    public static final String formatCurrency(String currency, Double val) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol(currency + " ");
        dfs.setGroupingSeparator('.');
        dfs.setMonetaryDecimalSeparator(',');
        ((DecimalFormat) nf).setDecimalFormatSymbols(dfs);
        nf.setMinimumFractionDigits(0);
        return nf.format(val);
    }

    public static String formatHour(String hour) {
        if (hour != null && hour.length() > 0) {
            hour = hour.substring(0, hour.length() - 3);
        }
        return hour;
    }

    public static final String formatCurrency(Double val) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("");
        dfs.setGroupingSeparator('.');
        dfs.setMonetaryDecimalSeparator('.');
        ((DecimalFormat) nf).setDecimalFormatSymbols(dfs);
        nf.setMinimumFractionDigits(0);
        return nf.format(val);
    }

    public static final String formatCurrencyIDR(Double val) {
        String retval = "Rp "+formatCurrency(val)+",-";
        return retval;
    }

    public static void showImage(Context context, String uri, ImageView imageView) {
        if(!uri.startsWith("http")) {
            uri = "http://cdnsandbox.sti-car.com/"+uri;
        }
        Glide.with(context)
                .load(uri)
                .into(imageView);
    }

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isConnectedToInternet(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else return false;
    }

    public static boolean checkGPSStatus(Context ctx){
        LocationManager locationManager = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if ( locationManager == null ) {
            locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        }
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex){}
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex){}

        if(!gps_enabled && !network_enabled){
            return false;
        } else {
            return true;
        }
    }

    public static void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        //final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        final String EMAIL_PATTERN = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String toCamelCase(String s){
        String[] parts = s.split("_");
        String camelCaseString = "";
        for (String part : parts){
            camelCaseString = camelCaseString + toProperCase(part);
        }
        return camelCaseString;
    }

    public static String toProperCase(String s) {
        return s.substring(0, 1).toUpperCase() +
                s.substring(1).toLowerCase();
    }

    public static int getDayofWeek(String datestr){
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            Date date = simpleDateFormat.parse(datestr);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.DAY_OF_WEEK);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String strLimit(String str, int limit){
        if(str.length() > limit){
            return str.substring(0, limit)+"...";
        } else {
            return str;
        }
    }

    public static String datetimeToDateString(String mdate){
        String pattern_old = "yyyy-MM-dd HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern_old);
        try {
            Date date = simpleDateFormat.parse(mdate);
            return new SimpleDateFormat("MMM , dd yyyy").format(date);
        }catch (Exception e){
            return  "";
        }

    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        //Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }


}

