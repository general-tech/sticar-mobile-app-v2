package sti_car.com.sticar.utils;

import android.content.Context;
import android.location.Location;
import android.provider.Settings;

import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.data.source.sp.SPConstant;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by femmy on 21/12/2017.
 */
@Named("AppHelper")
public class AppHelper {

    SharedPrefHelper mSPHelper;

    Context mCtx;

    @Inject
    public AppHelper(@Named("AppContext") Context ctx){
        mCtx = ctx;
        mSPHelper = new SharedPrefHelper(ctx.getSharedPreferences(AppConstant.SHARED_PREF_KEY, Context.MODE_PRIVATE));
    }

    public void saveLastLola(Location l){
        if(l != null){
            String sLat = String.valueOf(l.getLatitude());
            String sLng = String.valueOf(l.getLongitude());
            mSPHelper.put(SPConstant.SP_KEY_LAST_LATITUDE, sLat);
            mSPHelper.put(SPConstant.SP_KEY_LAST_LONGITUDE, sLng);
        }
    }

    public String getDeviceID(){
        String android_id = Settings.Secure.getString(mCtx.getContentResolver(),Settings.Secure.ANDROID_ID);
        return android_id;
    }

}
