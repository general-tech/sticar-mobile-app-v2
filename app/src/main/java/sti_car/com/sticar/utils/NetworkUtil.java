package sti_car.com.sticar.utils;

import android.content.Context;

import sti_car.com.sticar.data.source.api.ApiHelper;
import sti_car.com.sticar.data.source.api.RestApi;
import sti_car.com.sticar.data.source.sp.SPConstant;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.utils.annot.PerActivity;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by femmy on 11/12/2017.
 */
@PerActivity
public class NetworkUtil {
    SharedPrefHelper mSPHelper;

    @Inject
    public NetworkUtil(@Named("AppContext") Context ctx){
        mSPHelper = new SharedPrefHelper(ctx.getSharedPreferences(AppConstant.SHARED_PREF_KEY, Context.MODE_PRIVATE));
    }

    public RestApi getRetrofitV2() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.readTimeout(AppConstant.NETWORK_SERIVICE_PERIOD, TimeUnit.MILLISECONDS);
        httpClient.connectTimeout(AppConstant.NETWORK_SERIVICE_PERIOD, TimeUnit.MILLISECONDS);
        httpClient.addInterceptor(logging);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Content-TypeReportHistory", "application/json")
                        .header("Accept-Language", AppConstant.APP_LANGUAGE)
                        .header("Token", mSPHelper.get(SPConstant.SP_TOKEN,SPConstant.SP_EMPTY_STRING))
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        return new Retrofit.Builder()
                .baseUrl(ApiHelper.getUrlV2())
                .client(httpClient.build())
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(RestApi.class);
    }
}
