package sti_car.com.sticar.utils.annot;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by femmy on 11/12/2017.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ApplicationContext {
}
