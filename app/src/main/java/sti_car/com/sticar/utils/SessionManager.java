package sti_car.com.sticar.utils;

import android.content.Context;

import com.google.gson.Gson;

import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.utils.annot.PerActivity;
import sti_car.com.sticar.data.source.sp.SPConstant;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by femmy on 09/12/2017.
 */
@PerActivity
public class SessionManager {

    private Context context;

    private SharedPrefHelper mSPHelper;

    @Inject
    public SessionManager(@Named("AppContext") Context ctx ){
        context = ctx;
        mSPHelper = new SharedPrefHelper(ctx.getSharedPreferences(AppConstant.SHARED_PREF_KEY, Context.MODE_PRIVATE));
    }

    public UserAccount getUserAccount(){
        return new Gson().fromJson(mSPHelper.get(SPConstant.SP_USER_ACCOUNT,SPConstant.SP_EMPTY_STRING), UserAccount.class);
    }

    public void setToken(String token) {
        mSPHelper.put(SPConstant.SP_TOKEN, token);
    }

    public String getToken() {
        return mSPHelper.get(SPConstant.SP_TOKEN,SPConstant.SP_EMPTY_STRING);
    }

    public void setUserAccount(UserAccount userAccount) {
        if (userAccount != null) {
            String data = new Gson().toJson(userAccount);
            mSPHelper.put(SPConstant.SP_USER_ACCOUNT, data);
            setToken(userAccount.getData().getmToken());
        } else {
            //new UserClientService(this.context).logout();
            mSPHelper.put(SPConstant.SP_USER_ACCOUNT, SPConstant.SP_EMPTY_STRING);
        }
    }

    public boolean isLoggedIn(){
        UserAccount mUserAccount    = getUserAccount();
        String token                = getToken();
        if(mUserAccount != null && token != null){
            return true;
        } else {
            return false;
        }
    }
}
