package sti_car.com.sticar.ui.profilemain;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.UserData;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.ui.historycampaign.HistoryCampaignActivity;
import sti_car.com.sticar.ui.settingaccount.SettingAccountActivity;
import sti_car.com.sticar.ui.settingbank.SettingBankActivity;
import sti_car.com.sticar.ui.settingpassword.SettingPasswordActivity;
import sti_car.com.sticar.ui.settingvehicle.SettingVehicleActivity;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.utils.SessionManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileMainActivity extends AppCompatActivity implements
    ProfileMainContract.View
    , View.OnClickListener{

    @Inject
    ProfileMainPresenter presenter;

    ProfileMainComponent actvComponent;

    private LinearLayout llEditAkun, llEditBank, llEditPassword, llEditVehicle, llHistoryCampaign;
    private CircleImageView imgProfile;
    private SweetAlertDialog mDialogLoader;
    private TextView tvName, tvEmail;

    private Bitmap myBitmap;
    private Uri picUri;
    private SessionManager mSessManager;
    private UserData mUserData;
    private UserProfile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_main);

        actvComponent = DaggerProfileMainComponent.builder()
                .profileMainModule(new ProfileMainModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        llEditAkun          = (LinearLayout) findViewById(R.id.ll_profile_main_info_account);
        llEditAkun.setOnClickListener(this);
        llEditBank          = (LinearLayout) findViewById(R.id.ll_profile_main_info_bank);
        llEditBank.setOnClickListener(this);
        llEditVehicle       = (LinearLayout) findViewById(R.id.ll_profile_main_info_vehicle);
        llEditVehicle.setOnClickListener(this);
        llEditPassword      = (LinearLayout) findViewById(R.id.ll_profile_main_edit_password);
        llEditPassword.setOnClickListener(this);
        llHistoryCampaign   = (LinearLayout) findViewById(R.id.ll_profile_main_history_campaign);
        llHistoryCampaign.setOnClickListener(this);

        imgProfile          = (CircleImageView) findViewById(R.id.profile_main_image);

        imgProfile.setOnClickListener(this);


        mSessManager        = new SessionManager(this);
        mUserData           = mSessManager.getUserAccount().getData();
        profile             = mSessManager.getUserAccount().getUserProfile();

        tvName              = (TextView) findViewById(R.id.tv_profile_main_name);
        tvEmail             = (TextView) findViewById(R.id.tv_profile_main_email);
        if(profile != null){
            AppUtil.showImage(getApplicationContext(), profile.getPhotoURL(), imgProfile);
            tvName.setText(profile.getFullname());
            tvEmail.setText(profile.getEmail());
        }

        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == llEditAkun.getId()){
            Intent i = new Intent(ProfileMainActivity.this, SettingAccountActivity.class);
            startActivity(i);
        } else if(v.getId() == llEditBank.getId()){
            Intent i = new Intent(ProfileMainActivity.this, SettingBankActivity.class);
            startActivity(i);
        } else if(v.getId() == llEditVehicle.getId()){
            Intent i = new Intent(ProfileMainActivity.this, SettingVehicleActivity.class);
            startActivity(i);
        } else if(v.getId() == llEditPassword.getId()){
            Intent i = new Intent(ProfileMainActivity.this, SettingPasswordActivity.class);
            startActivity(i);
        } else if(v.getId() == llHistoryCampaign.getId()){
            Intent i = new Intent(ProfileMainActivity.this, HistoryCampaignActivity.class);
            startActivity(i);
        } else if(v.getId() == imgProfile.getId()){
            setImageProfile();
        }
    }

    private void setImageProfile(){
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap;
        if (resultCode == Activity.RESULT_OK) {
            if (getPickImageResultUri(data) != null) {
                picUri = getPickImageResultUri(data);
                try {
                    myBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), picUri);
                    myBitmap = rotateImageIfRequired(myBitmap, picUri);
                    myBitmap = getResizedBitmap(myBitmap, 500);
                    imgProfile.setImageBitmap(myBitmap);
                    presenter.uploadProfilePic(AppUtil.encodeTobase64(myBitmap));
                    mDialogLoader.show();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                bitmap = (Bitmap) data.getExtras().get("data");
                myBitmap = bitmap;
                presenter.uploadProfilePic(AppUtil.encodeTobase64(myBitmap));
                imgProfile.setImageBitmap(myBitmap);
                mDialogLoader.show();
            }
        }
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }


        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {
        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public void onSuccessUploadProfilePic(String url) {
        mDialogLoader.dismiss();
    }

    @Override
    public void onErrorUploadProfilePic(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }
}
