package sti_car.com.sticar.ui.registeraccount;

import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

import dagger.Component;

/**
 * Created by femmy on 13/01/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={RegisteraccountModule.class})
public interface RegisteraccountComponent {
    void inject(RegisteraccountActivity activity);
}
