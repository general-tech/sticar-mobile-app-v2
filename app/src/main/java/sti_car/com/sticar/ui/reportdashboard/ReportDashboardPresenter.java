package sti_car.com.sticar.ui.reportdashboard;

import android.content.Context;
import android.os.CountDownTimer;

import javax.inject.Inject;
import javax.inject.Named;

import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Report.Lifetime;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 06/02/2018.
 */

public class ReportDashboardPresenter extends BasePresenter<ReportDashboardContract.View>
        implements ReportDashboardContract.Presenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public ReportDashboardPresenter(ReportDashboardContract.View view) {
        super(view);
    }

    @Override
    public void requestReportTotal() {
        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {}

            public void onFinish() {
                Lifetime report = new Lifetime();
                report.setCredit(1655000);
                view.onSuccessRequestReport(report);
            }
        }.start();
    }
}
