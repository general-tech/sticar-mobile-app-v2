package sti_car.com.sticar.ui.campaigndetail;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sti_car.rschsticar10.R;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.CampaignDetail;
import sti_car.com.sticar.ui.main.MainActivity;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.ui.campaigndetail.DaggerCampaigndetailComponent;
import sti_car.com.sticar.utils.AppConstant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import javax.inject.Inject;

public class CampaigndetailActivity extends AppCompatActivity implements
    CampaigndetailContract.View
    ,View.OnClickListener
    ,OnMapReadyCallback {

    @Inject
    CampaigndetailPresenter presenter;

    CampaigndetailComponent actvComponent;

    private GoogleMap mMap;
    private View map;

    private TextView tvContractMonth,tvContractStart,tvContractEnd,
            tvContractType,tvCOntractArea,tvContractDailyKm,tvContractAvg, tvCmapaignEmpty;
    private Button btnJoin;
    private ImageView ivHeaderCampaign;
    private ProgressDialog dialogJoinCampaign;

    private String idWrap, sWrappingUUID,sCampaignUUID;

    private SweetAlertDialog mDialogLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaigndetail);

        actvComponent = DaggerCampaigndetailComponent.builder()
                .campaigndetailModule(new CampaigndetailModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idWrap = extras.getString(AppConstant.EXTRA_ADS_KEY);
        }
        presenter.initCampaign(idWrap);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        tvContractMonth         = (TextView) findViewById(R.id.tvContractMonth);
        tvContractStart         = (TextView) findViewById(R.id.tvContractStart);
        tvContractEnd           = (TextView) findViewById(R.id.tvContractEnd);
        tvContractType          = (TextView) findViewById(R.id.tvWrappingType);
        tvCOntractArea          = (TextView) findViewById(R.id.tvContractArea);
        tvContractDailyKm       = (TextView) findViewById(R.id.tvContractDailyKm);
        tvContractAvg           = (TextView) findViewById(R.id.tvContractAvg);
        tvCmapaignEmpty         = (TextView) findViewById(R.id.tvCampaignEmpty);
        btnJoin                 = (Button) findViewById(R.id.btnJoin);
        btnJoin.setOnClickListener(this);
        ivHeaderCampaign        = (ImageView) findViewById(R.id.iv_header_campaign);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dialogJoinCampaign = new ProgressDialog(CampaigndetailActivity.this);
        dialogJoinCampaign.setMessage("Please Wait!!");
        dialogJoinCampaign.setCancelable(false);
        dialogJoinCampaign.setProgressStyle(ProgressDialog.STYLE_SPINNER);


        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
        mDialogLoader.show();

        //Map Render
        //map = (View) findViewById(R.id.map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapCampaign);
        mapFragment.getMapAsync(CampaigndetailActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCampaignInitialized(Campaign campaign) {
        getSupportActionBar().setTitle(campaign.getName());
        sWrappingUUID   = campaign.getWrappingUuid();
        sCampaignUUID   = campaign.getUuid();
        String month    = campaign.getDuration() < 2 ? " " + getResources().getString(R.string.month)
                : " " + getResources().getString(R.string.month) + "s";
        tvContractMonth.setText(campaign.getDuration() + month);
        tvCOntractArea.setText(campaign.getArea());
        tvContractDailyKm.setText(String.valueOf(campaign.getDailyKm()) + " " + getResources().getString(R.string.kmperday));
        tvContractAvg.setText(AppUtil.formatCurrencyIDR(campaign.getWrappingCredit()));
        tvContractType.setText(campaign.getWrappingType());
        tvContractStart.setText(campaign.getStartDateFormat());
        tvContractEnd.setText(campaign.getEndDateFormat());
        Glide.with(CampaigndetailActivity.this).load(campaign.getImageUrl()).dontAnimate().into(ivHeaderCampaign);
        if(campaign.getStatus() == Campaign.CAMPAIGN_STATUS_ACTIVE){
            btnJoin.setVisibility(View.VISIBLE);
            tvCmapaignEmpty.setVisibility(View.GONE);
        } else {
            btnJoin.setVisibility(View.GONE);
            tvCmapaignEmpty.setVisibility(View.VISIBLE);
            tvCmapaignEmpty.setText(campaign.getMessage());
        }
        addMarker(new LatLng(campaign.getLatitude(),campaign.getLongitude()),campaign.getArea());
        mDialogLoader.dismiss();
    }

    private void addMarker(LatLng latLng, String tittle) {
        mMap.addMarker(new MarkerOptions().position(latLng).title(tittle));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
        mMap.addCircle(new CircleOptions().center(latLng).radius(100).fillColor(R.color.holo_blue_light).strokeWidth(1));
    }

    @Override
    public void errorIniatializedCampaign() {
        Toast.makeText(this, "Error loading campaign",Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void errorJoinCampaign(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(this, msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void successJoinCampaign() {
        mDialogLoader.dismiss();
        Intent i = new Intent(CampaigndetailActivity.this, MainActivity.class);
        i.putExtra(AppConstant.EXTRA_MESSAGES_KEY, "");
        startActivity(i);
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnJoin.getId()){
            mDialogLoader.setTitle("Please wait...");
            mDialogLoader.show();
            presenter.joinCampaign(sWrappingUUID, sCampaignUUID);
        } 
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
    }
}
