package sti_car.com.sticar.ui.registersuccess;

import javax.inject.Inject;

import sti_car.com.sticar.ui.base.BasePresenter;

/**
 * Created by femmy on 25/01/2018.
 */

public class RegisterSuccessPresenter extends BasePresenter<RegisterSuccessContract.View>
    implements RegisterSuccessContract.Presenter{

    @Inject
    public RegisterSuccessPresenter(RegisterSuccessContract.View view) {
        super(view);
    }
}
