package sti_car.com.sticar.ui.tnc;

import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Reponses.TncPolicy;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TncActivity extends AppCompatActivity implements TncContract.View{

    @Inject
    TncPresenter presenter;

    TncComponent actvComponent;

    private TextView tvContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tnc);

        actvComponent = DaggerTncComponent.builder()
                .tncModule(new TncModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(TncActivity.this);
        setupWindowAnimations();
        renderView();
        presenter.getTnc();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        tvContent = (TextView) findViewById(R.id.tv_content_tnc);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupWindowAnimations() {
        Slide fade = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            fade = (Slide) TransitionInflater.from(this).inflateTransition(R.transition.activity_slide);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(fade);
        }
    }

    @Override
    public void successRequestTnc(TncPolicy mTncPolicy) {
        tvContent.setText(Html.fromHtml(mTncPolicy.getData()));
    }

    @Override
    public void errorRequestTnc(String msg) {
        Toast.makeText(TncActivity.this, msg, Toast.LENGTH_SHORT).show();
    }
}
