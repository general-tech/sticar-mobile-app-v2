package sti_car.com.sticar.ui.redeem;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 06/02/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules=RedeemModule.class)
public interface RedeemComponent {
    void inject(RedeemActivity activity);
}

