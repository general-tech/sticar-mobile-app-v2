package sti_car.com.sticar.ui.reportdashboard;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 06/02/2018.
 */

@Module
public class ReportDashboardModule {
    private final ReportDashboardContract.View view;

    public ReportDashboardModule(ReportDashboardContract.View v){
        view = v;
    }

    @Provides
    ReportDashboardContract.View provideView(){
        return view;
    }
}

