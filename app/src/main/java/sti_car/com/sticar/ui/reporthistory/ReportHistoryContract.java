package sti_car.com.sticar.ui.reporthistory;

import java.util.ArrayList;

import sti_car.com.sticar.data.models.TransactionRedeem;

/**
 * Created by femmy on 06/02/2018.
 */

public class ReportHistoryContract {

    interface View {
        void onSuccessRequestHistory(ArrayList<TransactionRedeem> transRedeem);

        void onFailedReuqestHistory(String msg);
    }

    interface Presenter {
        void requestRedeemHistory();
    }
}
