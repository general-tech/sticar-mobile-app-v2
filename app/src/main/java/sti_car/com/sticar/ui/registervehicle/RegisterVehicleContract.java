package sti_car.com.sticar.ui.registervehicle;

import java.util.ArrayList;

import sti_car.com.sticar.data.models.CarBrand;
import sti_car.com.sticar.data.models.CarModel;
import sti_car.com.sticar.data.models.MotorBrand;
import sti_car.com.sticar.data.models.MotorModel;
import sti_car.com.sticar.data.models.VehicleColor;

/**
 * Created by femmy on 23/01/2018.
 */

public class RegisterVehicleContract {

    interface View {
        void onSuccessRequestCarBrands(ArrayList<CarBrand> brands);

        void onErrorRequestCarBrands(String msg);

        void onSuccessRequestMotorBrands(ArrayList<MotorBrand> brands);

        void onErrorRequestMotorBrands(String msg);

        void onSuccessRequestCarModels(ArrayList<CarModel> models);

        void onErrorRequestCarModels(String msg);

        void onSuccessRequestMotorModels(ArrayList<MotorModel> models);

        void onErrorRequestMotorModels(String msg);

        void onSuccessRequestVehicleColors(ArrayList<VehicleColor> colors);

        void onErrorRequestVehicleColors(String msg);

    }

    interface Presenter {

        void requestCarBrands();

        void requestMotorBrands();

        void requestCarModel(int id);

        void requestMotorModel(int id);

        void requestVehicleColor();
    }
}
