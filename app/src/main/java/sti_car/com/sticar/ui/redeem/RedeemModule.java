package sti_car.com.sticar.ui.redeem;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 06/02/2018.
 */

@Module
public class RedeemModule {

    private final RedeemContract.View view;

    public RedeemModule(RedeemContract.View view) {
        this.view = view;
    }

    @Provides
    RedeemContract.View provideView(){
        return view;
    }
}
