package sti_car.com.sticar.ui.settingbank;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Bank;
import sti_car.com.sticar.data.models.BankAccount;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.Reponses.BankAccountResponse;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.ui.registerbank.RegisterBankActivity;
import sti_car.com.sticar.utils.SessionManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingBankActivity extends AppCompatActivity
    implements SettingBankContract.View
        ,View.OnClickListener
        ,View.OnTouchListener{

    @Inject
    SettingBankPresenter presenter;

    SettingBankComponent actvComponent;

    private Spinner spinnerBank;

    private EditText etBankAccountNo;

    private Button btnSaveBank;

    private SweetAlertDialog mDialogLoader;

    private UserProfile mProfile;

    private boolean isVerified = false;


    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            setNeedConfirmedCondition();
        }

        @Override
        public void afterTextChanged(Editable s) {}
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_bank);

        actvComponent = DaggerSettingBankComponent.builder()
                .settingBankModule(new SettingBankModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void renderView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        spinnerBank         = (Spinner) findViewById(R.id.sp_bank_name);
        etBankAccountNo     = (EditText) findViewById(R.id.et_setting_account_no);
        etBankAccountNo.setOnTouchListener(this);
        etBankAccountNo.addTextChangedListener(mTextWatcher);

        btnSaveBank         = (Button) findViewById(R.id.btn_setting_save_bank);
        btnSaveBank.setOnClickListener(this);

        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
        mDialogLoader.setCancelable(false);
        mDialogLoader.show();

        mProfile = new SessionManager(this).getUserAccount().getData().getProfile();
        mDialogLoader.show();
        presenter.requestBankList();
    }


    private void setNeedConfirmedCondition(){
        mProfile.setBankAccountVerified(false);
        mProfile.setBankAccountNumber("");
        mProfile.setBankAccountName("");
        btnSaveBank.setText("Verifikasi Akun Bank");
        isVerified = false;
    }

    private void setConfirmedCondition(){
        isVerified = true;
        btnSaveBank.setText("Simpan");
    }

    @Override
    public void onRequestBankListSuccess(final List<Bank> banks) {
        ArrayList<String> mBankStringList = new ArrayList<String>();
        for (int i = 0; i < banks.size(); i++){
            Bank mBank = banks.get(i);
            mBankStringList.add(mBank.getName());
        }

        ArrayAdapter<String> bankNameAdapter = new ArrayAdapter<String>(SettingBankActivity.this,
                R.layout.spinner_layout_custom_green, mBankStringList);
        bankNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBank.setAdapter(bankNameAdapter);
        spinnerBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Bank bank       = banks.get(position);
                mProfile.setBankCode(bank.getCode());
                setNeedConfirmedCondition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        spinnerBank.setSelection(bankNameAdapter.getPosition(mProfile.getBankCode()));
        etBankAccountNo.setText(mProfile.getBankAccountNumber());
        mDialogLoader.dismiss();

    }

    @Override
    public void onRequestBankListFailed(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        mDialogLoader.dismiss();
    }

    @Override
    public void onBankAccountValid(BankAccountResponse bankAccountResponse) {
        mDialogLoader.dismiss();
        mProfile.setBankAccountNumber(bankAccountResponse.getAccountNumber());
        mProfile.setBankAccountName(bankAccountResponse.getAccountHolder());
        mProfile.setBankAccountVerified(true);
        setConfirmedCondition();
        Toast.makeText(SettingBankActivity.this, "Valid Bank Account",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBankAccountInvalid(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        mDialogLoader.dismiss();
    }

    @Override
    public void onSavingDataBankSuccess() {
        mDialogLoader.dismiss();
        Toast.makeText(this, "Data bank berhasil disimpan", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorSavingDataBank(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnSaveBank.getId()){
            if(isVerified){
                mDialogLoader.show();
                presenter.saveDateBank(mProfile);
            } else {
                String sAccNo = etBankAccountNo.getText().toString();
                BankAccount mbankAccount = new BankAccount();
                mbankAccount.setBankCode(mProfile.getBankCode());
                mbankAccount.setBankAccountNumber(sAccNo);
                mDialogLoader.show();
                presenter.checkBankAccountHolder(mbankAccount);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
