package sti_car.com.sticar.ui.registerconfirm;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 23/01/2018.
 */

@Module
public class RegisterConfirmModule {
    private final RegisterConfirmContract.View view;

    public RegisterConfirmModule(RegisterConfirmContract.View v){
        view = v;
    }

    @Provides
    RegisterConfirmContract.View provideView(){
        return view;
    }
}

