package sti_car.com.sticar.ui.reporthistory;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 06/02/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={ReportHistoryModule.class})
public interface ReportHistoryComponent {
    void inject(ReportHistoryActivity activity);
}