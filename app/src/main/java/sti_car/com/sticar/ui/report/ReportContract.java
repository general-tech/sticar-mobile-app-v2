package sti_car.com.sticar.ui.report;

import sti_car.com.sticar.data.models.Reponses.ResponseList;
import sti_car.com.sticar.data.models.Report.History;
import sti_car.com.sticar.data.models.Report.Report;

/**
 * Created by femmy on 11/01/2018.
 */

public class ReportContract {

    interface View {
        void successRequestReport(Report mReport);

        void errorRequestReport(String msg);
    }

    interface CalculationView {
        void successRequestReport(Report mReport);

        void errorRequestReport(String msg);
    }

    interface HistoryView {

        void successRequest(ResponseList<History> response);

        void errorRequest(String msg);
    }

    interface Presenter {
        void requestReport();
    }

    interface CalculationPresenter {
        void getCalculationReport();
    }

    interface HistoryPresenter {

        void requestReportHistory();
    }
}
