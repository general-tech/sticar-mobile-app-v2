package sti_car.com.sticar.ui.registervehicle;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 23/01/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={RegisterVehicleModule.class})
public interface RegisterVehicleComponent {
    void inject(RegisterVehicleActivity activity);
}
