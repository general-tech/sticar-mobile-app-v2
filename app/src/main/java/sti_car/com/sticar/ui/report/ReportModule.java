package sti_car.com.sticar.ui.report;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 11/01/2018.
 */
@Module
public class ReportModule {

    private final ReportContract.View view;


    public ReportModule(ReportContract.View v){
        view = v;
    }

    @Provides
    ReportContract.View provideView(){
        return view;
    }
}
