package sti_car.com.sticar.ui.privacypolicy;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 23/01/2018.
 */

@Module
public class PrivacyPolicyModule {

    private final PrivacyPolicyContract.View view;

    public PrivacyPolicyModule(PrivacyPolicyContract.View v){
        view = v;
    }

    @Provides
    PrivacyPolicyContract.View provideView(){
        return view;
    }
}
