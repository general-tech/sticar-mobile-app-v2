package sti_car.com.sticar.ui.notification;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 10/02/2018.
 */

@Module
public class NotificationModule {

    private final NotificationContract.View view;

    public NotificationModule(NotificationContract.View view) {
        this.view = view;
    }

    @Provides
    NotificationContract.View provideView(){
        return view;
    }
}
