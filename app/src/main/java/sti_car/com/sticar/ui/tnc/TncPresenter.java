package sti_car.com.sticar.ui.tnc;

import android.util.Log;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Reponses.TncPolicy;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.NetworkUtil;

/**
 * Created by femmy on 23/01/2018.
 */

public class TncPresenter extends BasePresenter<TncContract.View> implements  TncContract.Presenter{

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public TncPresenter(TncContract.View view) {
        super(view);
    }

    @Override
    public void getTnc() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getTNC()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<TncPolicy>() {
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {
                    view.errorRequestTnc("Error request terms and conditions, Try again later.");
                }

                @Override
                public void onNext(TncPolicy tncPolicy) {
                    view.successRequestTnc(tncPolicy);
                }
            })
        );
    }
}
