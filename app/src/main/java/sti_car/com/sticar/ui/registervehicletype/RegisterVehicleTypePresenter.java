package sti_car.com.sticar.ui.registervehicletype;

import javax.inject.Inject;

import sti_car.com.sticar.ui.base.BasePresenter;

/**
 * Created by femmy on 25/01/2018.
 */

public class RegisterVehicleTypePresenter extends BasePresenter<RegisterVehicleTypeContract.View>
        implements RegisterVehicleTypeContract.Presenter{

    @Inject
    public RegisterVehicleTypePresenter(RegisterVehicleTypeContract.View view) {
        super(view);
    }
}
