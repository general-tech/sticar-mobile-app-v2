package sti_car.com.sticar.ui.login;

/**
 * Created by femmy on 11/12/2017.
 */

public class LoginContract {

    interface View {

        void onLoginFailed(String msg);

        void onLoginSuccess();

        void onPermissionDenied();

    }

    interface Presenter {
        void doLogin(String email, String password);
    }
}
