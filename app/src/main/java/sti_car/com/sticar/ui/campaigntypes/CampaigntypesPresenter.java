package sti_car.com.sticar.ui.campaigntypes;

import android.content.Context;

import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppHelper;
import sti_car.com.sticar.utils.SessionManager;
import sti_car.com.sticar.utils.NetworkUtil;

import javax.inject.Inject;
import javax.inject.Named;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 09/01/2018.
 */

public class CampaigntypesPresenter extends BasePresenter<CampaigntypesContract.View> {

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Named("AppHelper")
    @Inject
    AppHelper mAppHelper;

    @Inject
    public CampaigntypesPresenter(CampaigntypesContract.View view) {
        super(view);
    }
}
