package sti_car.com.sticar.ui.reportcalculation;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 06/02/2018.
 */

@Module
public class ReportCalculationModule {
    private final ReportCalculationContract.View view;

    public ReportCalculationModule(ReportCalculationContract.View v){
        view = v;
    }

    @Provides
    ReportCalculationContract.View provideView(){
        return view;
    }
}
