package sti_car.com.sticar.ui.login;

import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

import dagger.Component;

/**
 * Created by femmy on 11/12/2017.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules=LoginModule.class)
public interface LoginComponent {
    void inject(LoginActivity activity);
}
