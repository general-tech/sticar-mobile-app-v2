package sti_car.com.sticar.ui.settingaccount;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.utils.SessionManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingAccountActivity extends AppCompatActivity implements
        SettingAccountContract.View
        ,View.OnClickListener
        ,View.OnTouchListener{

    @Inject
    SettingAccountPresenter presenter;

    SettingAccountComponent actvComponent;

    private EditText etFullname, etEmail, etPhone;
    private Button btnSave;
    private SweetAlertDialog mDialogLoader;

    private UserProfile mProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_account);

        actvComponent = DaggerSettingAccountComponent.builder()
                .settingAccountModule(new SettingAccountModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProfile        = new SessionManager(this).getUserAccount().getData().getProfile();

        etFullname      = (EditText) findViewById(R.id.et_setting_fullname);
        etFullname.setText(mProfile.getFullname());
        etFullname.setOnTouchListener(this);
        etEmail         = (EditText) findViewById(R.id.et_setting_email);
        etEmail.setText(mProfile.getEmail());
        etEmail.setOnTouchListener(this);
        etPhone         = (EditText) findViewById(R.id.et_setting_phone);
        etPhone.setText(mProfile.getPhoneNumber());
        etPhone.setOnTouchListener(this);
        btnSave         = (Button) findViewById(R.id.btn_setting_save_account);
        btnSave.setOnClickListener(this);

        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
        mDialogLoader.setCancelable(false);

    }

    @Override
    public void onSuccessSaveAccount(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(this, msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorSaveAccount(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(this, msg,Toast.LENGTH_SHORT).show();
    }

    private Boolean validateForm(String fullname, String email, String phone){
        Boolean result = true;
        String errorMsg = null;
        if (fullname.trim().length() < 5) {
            result = false;
            errorMsg = getResources().getString(R.string.nameerror);
        }  else if (!AppUtil.emailValidator(email)) {
            result = false;
            errorMsg = getResources().getString(R.string.emailerror);
        }  else if (phone.length() > 14 || phone.length() < 4) {
            result = false;
            errorMsg = getResources().getString(R.string.phoneerror);
        }

        if(errorMsg != null) {
            Toast.makeText(this, errorMsg, Toast.LENGTH_SHORT).show();
        }

        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnSave.getId()){
            String fullname = etFullname.getText().toString();
            String email    = etEmail.getText().toString();
            String phone    = etPhone.getText().toString();

            if(validateForm(fullname, email, phone)){
                mDialogLoader.show();
                mProfile.setFullname(fullname);
                mProfile.setEmail(email);
                mProfile.setPhoneNumber(phone);
                presenter.saveAccount(mProfile);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
