package sti_car.com.sticar.ui.registerconfirm;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 23/01/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={RegisterConfirmModule.class})
public interface RegisterConfirmComponent {
    void inject(RegisterConfirmActivity activity);
}

