package sti_car.com.sticar.ui.redeem;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.Transaction;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserData;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 06/02/2018.
 */

public class RedeemPresenter extends BasePresenter<RedeemContract.View>
        implements RedeemContract.Presenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public RedeemPresenter(RedeemContract.View view) {
        super(view);
    }

    @Override
    public void checkCurrentRedeem() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().checkCurrentRedeem()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<Transaction>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onCheckCurrentRedeemError(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<Transaction> response) {
                        if(response.getCode() == 200){
                            view.onCheckCurrentRedeemSuccess(response.getData());
                        } else {
                            view.onCheckCurrentRedeemError(response.getMessage());
                        }
                    }
                })
        );
    }

    @Override
    public void requestRedeem(Transaction transaction) {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().requestRedeem(transaction)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<Transaction>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onRequestRedeemError(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<Transaction> response) {
                        if(response.getCode() == 200){
                            view.onRequestRedeemSuccess();
                        } else {
                            view.onRequestRedeemError(response.getMessage());
                        }
                    }
                })
        );
    }
}
