package sti_car.com.sticar.ui.splash;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import sti_car.com.sticar.App;
import sti_car.com.sticar.ui.login.LoginActivity;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.ui.main.MainActivity;
import sti_car.com.sticar.ui.splash.DaggerSplashComponent;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity
        implements SplashContract.View {

    @Inject
    SplashPresenter presenter;

    SplashComponent actvComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        actvComponent = DaggerSplashComponent.builder()
                .splashModule(new SplashModule(this))
                .appComponent(App.get(this).getComponent()).build();

        actvComponent.inject(this);
        presenter.init();


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.init();
    }

    @Override
    public void throwToLogin() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void gotoMain() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onDataUpdated(UserAccount mUserAcc) {
        //Toast.makeText(this,"--------------onDataUpdated-------------",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorProcess(String s) {

    }

    @Override
    public void showToastMsg(String s) {
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnavailableGPS() {
        showReqLocationDialog();
    }


    private void showReqLocationDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Location Services Not Active");
        builder.setMessage("Please enable Location Services and GPS");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                // Show location settings when the user acknowledges the alert dialog
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

}
