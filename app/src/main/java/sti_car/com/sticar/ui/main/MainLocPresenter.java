package sti_car.com.sticar.ui.main;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppHelper;

import javax.inject.Inject;
import javax.inject.Named;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by femmy on 20/12/2017.
 */

public class MainLocPresenter extends BasePresenter<MainContract.LocView>
        implements MainContract.LocPresenter {

    @Named("AppContext")
    @Inject
    Context context;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("AppHelper")
    @Inject
    AppHelper mAppHelper;

    MainActivity activity;

    LocationManager mLocService;

    @Inject
    public MainLocPresenter(MainContract.LocView view) {
        super(view);
        activity = (MainActivity) view;
    }


    @Override
    public void setLocationListener() {
        Location mLastLocation = null;
        boolean isGPSEnabled, isNetworkEnabled;
        try {
            mLocService = (LocationManager) context.getSystemService(LOCATION_SERVICE);
            // getting GPS status
            isGPSEnabled = mLocService.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = mLocService.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                view.onNoNetworkProvider();
            } else {
                if (isGPSEnabled) {
                    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mLocService.requestLocationUpdates(LocationManager.GPS_PROVIDER, AppUtil.getPeriod(),AppUtil.getDistancePeriod(), activity);
                    if (mLocService != null) {
                        mLastLocation = mLocService.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }

                // if GPS Enabled get lat/long using GPS Services
                if (isNetworkEnabled) {
                    if (mLastLocation == null) {
                        mLocService.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, AppUtil.getPeriod(),AppUtil.getDistancePeriod(), activity);
                        if (mLocService != null) {
                            mLastLocation = mLocService.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        }
                    }
                }
            }

            if(mLastLocation != null){
                view.onLocationListenerSet(mLastLocation);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unsetLocationListener() {
        mLocService.removeUpdates(activity);
    }


}
