package sti_car.com.sticar.ui.report;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 11/01/2018.
 */
@Module
public class ReportCalculationModule {

    private final ReportContract.CalculationView view;

    public ReportCalculationModule(ReportContract.CalculationView v){
        view = v;
    }

    @Provides
    ReportContract.CalculationView provideCalculationView(){
        return view;
    }
}
