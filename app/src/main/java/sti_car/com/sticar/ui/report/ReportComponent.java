package sti_car.com.sticar.ui.report;

import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

import dagger.Component;

/**
 * Created by femmy on 11/01/2018.
 */
@PerActivity
@Component(dependencies = AppComponent.class,
        modules={ReportModule.class,ReportCalculationModule.class, ReportHistoryModule.class})
public interface ReportComponent {
    void inject(ReportActivity activity);

    void inject(ReportCalculationActivity activity);

    void inject(ReportHistoryActivity activity);
}
