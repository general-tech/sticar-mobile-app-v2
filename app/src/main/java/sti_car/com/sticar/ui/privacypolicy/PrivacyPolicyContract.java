package sti_car.com.sticar.ui.privacypolicy;

import sti_car.com.sticar.data.models.Reponses.TncPolicy;

/**
 * Created by femmy on 23/01/2018.
 */

public class PrivacyPolicyContract {

    interface View {
        void successRequestPolicy(TncPolicy mtncPolicy);

        void errorRequestPolicy(String msg);
    }

    interface Presenter {
        void getPolicy();
    }
}
