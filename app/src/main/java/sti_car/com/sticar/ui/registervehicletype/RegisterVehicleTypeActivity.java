package sti_car.com.sticar.ui.registervehicletype;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Register;
import sti_car.com.sticar.ui.registervehicle.RegisterVehicleActivity;
import sti_car.com.sticar.utils.AppConstant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterVehicleTypeActivity extends AppCompatActivity implements
        View.OnClickListener
        , RegisterVehicleTypeContract.View{

    @Inject
    RegisterVehicleTypePresenter presenter;

    RegisterVehicleTypeComponent actvComponent;

    private Button btnTypeCar, btnTypeMotor, btnTypeBoth;
    private Register mRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_register_vehicle_type);

        actvComponent = DaggerRegisterVehicleTypeComponent.builder()
                .registerVehicleTypeModule(new RegisterVehicleTypeModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mRegister = (Register) extras.getSerializable(AppConstant.EXTRA_REGISTER_KEY);
        }
    }

    private void renderView(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnTypeCar      = (Button) findViewById(R.id.btn_vehicle_type_car);
        btnTypeCar.setOnClickListener(this);
        btnTypeMotor    = (Button) findViewById(R.id.btn_vehicle_type_motor);
        btnTypeMotor.setOnClickListener(this);
        btnTypeBoth     = (Button) findViewById(R.id.btn_vehicle_type_both);
        btnTypeBoth.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(RegisterVehicleTypeActivity.this, RegisterVehicleActivity.class);
        if(v.getId() == btnTypeCar.getId()){
            i.putExtra(AppConstant.EXTRA_KEY_VEHICLE_TYPE, AppConstant.EXTRA_VEHICLE_TYPE_CAR);
        } else if(v.getId() == btnTypeMotor.getId()){
            i.putExtra(AppConstant.EXTRA_KEY_VEHICLE_TYPE, AppConstant.EXTRA_VEHICLE_TYPE_MOTOR);
        } else if(v.getId() == btnTypeBoth.getId()){
            i.putExtra(AppConstant.EXTRA_KEY_VEHICLE_TYPE, AppConstant.EXTRA_VEHICLE_TYPE_BOTH);
        }
        i.putExtra(AppConstant.EXTRA_REGISTER_KEY, mRegister);
        startActivity(i);
    }
}
