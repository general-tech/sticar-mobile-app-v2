package sti_car.com.sticar.ui.splash;

import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

import dagger.Component;

/**
 * Created by femmy on 08/12/2017.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules={SplashModule.class})
public interface SplashComponent {
    void inject(SplashActivity activity);
}
