package sti_car.com.sticar.ui.feedback;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 22/01/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={FeedbackModule.class})
public interface FeedbackComponent {
    void inject(FeedbackActivity feedbackActivity);
}
