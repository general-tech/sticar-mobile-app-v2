package sti_car.com.sticar.ui.main;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.Reponses.StartTrip;
import sti_car.com.sticar.data.models.Reponses.TrackFeed;
import sti_car.com.sticar.data.models.Report.Periodic;
import sti_car.com.sticar.data.models.SendDataTracker;
import sti_car.com.sticar.data.models.SendTrack;
import sti_car.com.sticar.data.models.TodayTrip;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserData;
import sti_car.com.sticar.data.models.UserHome;
import sti_car.com.sticar.data.models.realm.TripToday;
import sti_car.com.sticar.data.source.realm.RealmHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.data.models.ContractUUIDBody;
import sti_car.com.sticar.data.models.Report.Today;
import sti_car.com.sticar.data.source.sp.SPConstant;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 12/12/2017.
 */

public class MainPresenter extends BasePresenter<MainContract.View> implements MainContract.Presenter {

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription mSubscriptions;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("Realm")
    @Inject
    Realm realm;

    @Named("RealmHelper")
    @Inject
    RealmHelper mRealmHelper;

    @Inject
    public MainPresenter(MainContract.View view) {
        super(view);
    }

    @Override
    public void startTrip(){
        mSubscriptions.add(mNetworkUtil.getRetrofitV2().startTrip()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<Response<TodayTrip>>() {
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {
                    view.startingTripFailed(e.getMessage());
                }

                @Override
                public void onNext(Response<TodayTrip> response) {
                    if(response.getCode() == 200 ){
                        UserAccount mUserAccount    = mSessionManager.getUserAccount();
                        mUserAccount.getData().setTodayTrip(response.getData());
                        UserData mData = mUserAccount.getData();
                        mData.setTodayTrip(response.getData());
                        mUserAccount.setData(mData);
                        mSessionManager.setUserAccount(mUserAccount);
                        view.onTripStarted();
                    } else {
                        view.startingTripFailed(response.getMessage());
                    }
                }
            })
        );
    }

    @Override
    public void logout() {
        if(!realm.isInTransaction()){
            realm.beginTransaction();
        }
        realm.deleteAll();
        realm.commitTransaction();
//        realm.close();
        mSessionManager.setUserAccount(null);
        view.onUserAccountLoggedOut();
    }

    @Override
    public void initLocalUserAccountData() {
        UserAccount mUserAcc = mSessionManager.getUserAccount();
        if(mUserAcc != null){
            view.onSuccessIntiLocalUserAccountData(mUserAcc);
        } else {
            view.onErrorIntiLocalUserAccountData();
        }
    }

    @Override
    public void initHomeData() {
        mSubscriptions.add(mNetworkUtil.getRetrofitV2().getHomeData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<UserAccount>() {
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {
                    view.onErrorInitHomeData(e.getMessage());
                }

                @Override
                public void onNext(UserAccount mUserAccount) {
                    UserAccount mUserAcc    = mSessionManager.getUserAccount();
                    TodayTrip mTrip         = mUserAccount.getData().getTodayTrip();
                    Campaign mCampaign      = mUserAccount.getData().getCampaign();
                    UserData mData          = mUserAcc.getData();
                    mData.setTodayTrip(mTrip);
                    mData.setCampaign(mCampaign);
                    mUserAcc.setData(mData);
                    mSessionManager.setUserAccount(mUserAcc);
                    checkAndPostUnpostedTrip();
                }
            })
        );
    }



    @Override
    public void checkAndPostUnpostedTrip() {
        String tripUUID         = SPHelper.get(SPConstant.SP_KEY_UNPOSTED_TRIP_TRIPUUID,"");
        String strArrayTrack    = SPHelper.get(SPConstant.SP_KEY_UNPOSTED_TRIP_ARRAYTRACK,"");
        if(!tripUUID.equalsIgnoreCase("") && !strArrayTrack.equalsIgnoreCase("")){
            Log.d(AppConstant.APP_TAG, "-------------dasdasdsadasd--------------");
            SendDataTracker sendDataTracker = new SendDataTracker();
            sendDataTracker.setTripUUID(tripUUID);
            Gson gson = new Gson();
            ArrayList<SendTrack> mSendTrackSent = gson.fromJson(strArrayTrack, new TypeToken<ArrayList<SendTrack>>(){}.getType());
            sendDataTracker.setTrips(mSendTrackSent);


            mSubscriptions.add(mNetworkUtil.getRetrofitV2().sendTrackerFeed(sendDataTracker)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<TrackFeed>() {
                    @Override
                    public void onCompleted() {
                        //sendMessage("hideDialog", "");
                    }

                    @Override
                    public void onError(Throwable e) {
                        useLocalTodayDataTrip();
                    }

                    @Override
                    public void onNext(TrackFeed responsTrackFeed) {
                        // if send data status not equal 200 then throwback trank sent to array track in order to try resend in the next push
                        if(Integer.parseInt(responsTrackFeed.getCode()) != 200){
                            SPHelper.deleteSavedData(SPConstant.SP_KEY_UNPOSTED_TRIP_TRIPUUID);
                            SPHelper.deleteSavedData(SPConstant.SP_KEY_UNPOSTED_TRIP_ARRAYTRACK);
                        }
                        useLocalTodayDataTrip();
                    }
                })
            );
        } else {
            syncLocalDataWithServer();
            view.onSuccessInitHomeData(mSessionManager.getUserAccount());
        }
    }

    private void useLocalTodayDataTrip(){
        UserAccount userAccount = mSessionManager.getUserAccount();
        double credit           = userAccount.getCurrentPriceCampaign();
        TripToday mTripToday    = mRealmHelper.getTripToday();

        double newTotalDistanceToday          = mTripToday.getDistanceTodayDouble(); // In KM
        DecimalFormat df = new DecimalFormat("####0.00");
        newTotalDistanceToday = Double.valueOf(df.format(newTotalDistanceToday));
        double newTotalCreditToday            = newTotalDistanceToday * credit;
        UserData mData          = userAccount.getData();
        TodayTrip trip          = mData.getTodayTrip();
        trip.setCredit(newTotalCreditToday);
        trip.setDistance(newTotalDistanceToday);
        mData.setTodayTrip(trip);
        userAccount.setData(mData);
        view.onSuccessInitHomeData(userAccount);
    }

    private void syncLocalDataWithServer(){
        UserAccount mUserAcc    = mSessionManager.getUserAccount();
        TodayTrip mTrip         = mUserAcc.getData().getTodayTrip();

        TripToday mTripToday    = mRealmHelper.getTripToday();
        realm.beginTransaction();
        mTripToday.setDistanceToday(String.valueOf(mTrip.getDistance()));
        mTripToday.setCreditToday(String.valueOf(mTrip.getCredit()));
        realm.commitTransaction();
    }
}
