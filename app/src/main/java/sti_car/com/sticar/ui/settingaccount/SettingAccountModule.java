package sti_car.com.sticar.ui.settingaccount;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 14/02/2018.
 */

@Module
public class SettingAccountModule {
    private final SettingAccountContract.View view;

    public SettingAccountModule(SettingAccountContract.View v){
        view = v;
    }

    @Provides
    SettingAccountContract.View provideView(){
        return view;
    }
}

