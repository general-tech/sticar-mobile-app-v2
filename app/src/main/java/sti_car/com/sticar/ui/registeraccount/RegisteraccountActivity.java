package sti_car.com.sticar.ui.registeraccount;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.ui.login.LoginActivity;
import sti_car.com.sticar.ui.registerbank.RegisterBankActivity;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.data.models.Register;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import javax.inject.Inject;

public class RegisteraccountActivity extends AppCompatActivity implements
        RegisteraccountContract.View
        ,View.OnClickListener
        ,TextView.OnEditorActionListener
        ,View.OnTouchListener{

    @Inject
    RegisteraccountPresenter presenter;

    RegisteraccountComponent actvComponent;

    private ScrollView svWrapperMain;

    private EditText etName,etEmail,etPhone,etPassword,etPassconf;
    private TextView tvLogin;
    private Button btnNext;

    private String fullname, email, phoneNumber, password, confirmPassword, cekEmail;

    private SweetAlertDialog mDialogLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_account);

        actvComponent = DaggerRegisteraccountComponent.builder()
                .registeraccountModule(new RegisteraccountModule(this))
                .appComponent(App.get(this).getComponent()).build();

        actvComponent.inject(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        renderView();
    }

    private void renderView(){
        svWrapperMain           = (ScrollView) findViewById(R.id.sv_wrapper_main);
        svWrapperMain.fullScroll(View.FOCUS_DOWN);
        etName                  = (EditText) findViewById(R.id.et_fullname);
        etName.setOnTouchListener(this);
        etEmail                 = (EditText) findViewById(R.id.et_email);
        etEmail.setOnTouchListener(this);
        etPhone                 = (EditText) findViewById(R.id.et_phone);
        etPhone.setOnTouchListener(this);
        etPassword              = (EditText) findViewById(R.id.et_password);
        etPassword.setOnTouchListener(this);
        etPassconf              = (EditText) findViewById(R.id.et_passconf);
        etPassconf.setOnTouchListener(this);
        etPassconf.setOnEditorActionListener(this);
        tvLogin                 = (TextView) findViewById(R.id.tv_login);
        tvLogin.setOnClickListener(this);
        tvLogin.setText(Html.fromHtml("Sudah memiliki akun? <b>Masuk</b>"));
        btnNext                 = (Button) findViewById(R.id.btn_register);
        btnNext.setOnClickListener(this);

        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
        mDialogLoader.setCancelable(false);

    }

    private void enableBtnNext(){
        btnNext.setText("Daftar");
        btnNext.setEnabled(true);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnNext.getId()){

            fullname        = etName.getText().toString();
            email           = etEmail.getText().toString();
            phoneNumber     = etPhone.getText().toString();
            password        = etPassword.getText().toString();
            confirmPassword = etPassconf.getText().toString();
            if(validateForm()){
                btnNext.setEnabled(false);
                btnNext.setText("Checking email...");
                presenter.checkEmail(email);
                mDialogLoader.show();
            }
        } else if(v.getId() == tvLogin.getId()) {
            Intent i = new Intent(RegisteraccountActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    }

    private Boolean validateForm(){
        Boolean result = true;
        String errorMsg = null;
        if (fullname.trim().length() < 5) {
            result = false;
            errorMsg = getResources().getString(R.string.nameerror);
        }  else if (!AppUtil.emailValidator(email)) {
            result = false;
            errorMsg = getResources().getString(R.string.emailerror);
        }  else if (phoneNumber.length() > 14 || phoneNumber.length() < 4) {
            result = false;
            errorMsg = getResources().getString(R.string.phoneerror);
        } else if (password.isEmpty() || confirmPassword.isEmpty()) {
            result = false;
            errorMsg = getResources().getString(R.string.passnull);
        } else if (password.length() < 6 || confirmPassword.length() < 6) {
            result = false;
            errorMsg = getResources().getString(R.string.passmin);
        } else if (!password.equals(confirmPassword)) {
            result = false;
            errorMsg = getResources().getString(R.string.passerror);
        }

        if(errorMsg != null) {
            Toast.makeText(RegisteraccountActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
        }

        return result;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
            //doNext();
        }
        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        v.setFocusable(true);
        v.setFocusableInTouchMode(true);
        return false;
    }

    @Override
    public void onEmailValid() {
        mDialogLoader.dismiss();
        enableBtnNext();
        Register mRegister = new Register();
        mRegister.setName(fullname);
        mRegister.setEmail(email);
        mRegister.setPhoneNumber(phoneNumber);
        mRegister.setPassword(password);
        mRegister.setConfirmPassword(confirmPassword);
        Intent i = new Intent(RegisteraccountActivity.this, RegisterBankActivity.class);
        i.putExtra(AppConstant.EXTRA_REGISTER_KEY, mRegister);
        startActivity(i);
    }

    @Override
    public void onEmailInvalid(String msg) {
        mDialogLoader.dismiss();
        enableBtnNext();
        Toast.makeText(RegisteraccountActivity.this, msg, Toast.LENGTH_SHORT).show();
    }
}
