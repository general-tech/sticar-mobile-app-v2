package sti_car.com.sticar.ui.forgetpassword;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.ui.login.LoginActivity;
import sti_car.com.sticar.utils.AppUtil;

public class ForgetPasswordActivity extends AppCompatActivity
    implements ForgetPasswordContract.View
        ,View.OnTouchListener
        ,View.OnClickListener{

    @Inject
    ForgetPasswordPresenter presenter;

    ForgetPasswordComponent actvComponent;

    private EditText etEmail;

    private Button btnSubmit;

    private TextView tvLogin;

    private SweetAlertDialog mDialogLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        actvComponent = DaggerForgetPasswordComponent.builder()
                .forgetPasswordModule(new ForgetPasswordModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    public void renderView(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        etEmail         = (EditText) findViewById(R.id.et_email);
        etEmail.setOnTouchListener(this);
        btnSubmit       = (Button) findViewById(R.id.btn_forget_pass);
        btnSubmit.setOnClickListener(this);
        tvLogin                 = (TextView) findViewById(R.id.tv_login);
        tvLogin.setOnClickListener(this);


        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnSubmit.getId()){
            String email = etEmail.getText().toString();
            if (AppUtil.emailValidator(email)) {
                mDialogLoader.show();
                presenter.submitForgetPassword(email);
            } else {
                Toast.makeText(ForgetPasswordActivity.this, "Email tidak valid",Toast.LENGTH_SHORT).show();
            }
            presenter.submitForgetPassword(email);
        } else if(v.getId() == tvLogin.getId()){
            Intent i = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onSuccessSubmitForgetPassword(String msg) {
        mDialogLoader.dismiss();
        etEmail.setText("");
        Toast.makeText(ForgetPasswordActivity.this, msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorSubmitForgetPassword(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(ForgetPasswordActivity.this, msg,Toast.LENGTH_SHORT).show();
    }
}
