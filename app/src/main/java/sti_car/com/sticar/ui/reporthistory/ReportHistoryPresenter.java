package sti_car.com.sticar.ui.reporthistory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import javax.inject.Inject;

import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.TransactionRedeem;
import sti_car.com.sticar.ui.base.BasePresenter;

/**
 * Created by femmy on 06/02/2018.
 */

public class ReportHistoryPresenter extends BasePresenter<ReportHistoryContract.View>
    implements ReportHistoryContract.Presenter{

    @Inject
    public ReportHistoryPresenter(ReportHistoryContract.View view) {
        super(view);
    }


    @Override
    public void requestRedeemHistory() {
        String json = "{'status':true,'code':200,'data':[{'transaction_id':1,'driver_id':2,'transaction_type_id':1,'total':500000,'datetime':'2018-02-14 19:00','status':1},{'transaction_id':1,'driver_id':2,'transaction_type_id':1,'total':500000,'datetime':'2018-02-14 19:00','status':1},{'transaction_id':1,'driver_id':2,'transaction_type_id':1,'total':500000,'datetime':'2018-02-14 19:00','status':1},{'transaction_id':1,'driver_id':2,'transaction_type_id':1,'total':500000,'datetime':'2018-02-14 19:00','status':1},{'transaction_id':1,'driver_id':2,'transaction_type_id':1,'total':500000,'datetime':'2018-02-14 19:00','status':1}],'message':'Successfully show report','language':'id'}";
        Gson gson = new Gson();
        Response<ArrayList<TransactionRedeem>> response = gson.fromJson(json,new TypeToken<Response<ArrayList<TransactionRedeem>>>(){}.getType());
        view.onSuccessRequestHistory(response.getData());

    }
}
