package sti_car.com.sticar.ui.campaigns;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 09/01/2018.
 */
@Module
public class CampaignsModule {

    private final CampaignsContract.View view;

    public CampaignsModule(CampaignsContract.View v){
        view = v;
    }

    @Provides
    CampaignsContract.View provideView(){
        return view;
    }
}
