package sti_car.com.sticar.ui.main;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 12/12/2017.
 */

@Module
public class MainModule {

    private final MainContract.View view;

    private final MainContract.LocView locView;


    public MainModule(MainContract.View v){
        view = v;
        locView = (MainContract.LocView) v;
    }

    @Provides
    MainContract.View provideView(){
        return view;
    }

    @Provides
    MainContract.LocView provideLocView(){
        return locView;
    }


}
