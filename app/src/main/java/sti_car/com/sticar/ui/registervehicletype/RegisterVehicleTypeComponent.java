package sti_car.com.sticar.ui.registervehicletype;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 25/01/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={RegisterVehicleTypeModule.class})
public interface RegisterVehicleTypeComponent {
    void inject(RegisterVehicleTypeActivity activity);
}