package sti_car.com.sticar.ui.historycampaign;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 14/02/2018.
 */

@Module
public class HistoryCampaignModule {

    private final HistoryCampaignContract.View view;

    public HistoryCampaignModule(HistoryCampaignContract.View view) {
        this.view = view;
    }

    @Provides
    HistoryCampaignContract.View provideView(){
        return view;
    }
}

