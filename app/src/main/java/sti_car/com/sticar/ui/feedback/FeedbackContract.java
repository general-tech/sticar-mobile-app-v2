package sti_car.com.sticar.ui.feedback;

/**
 * Created by femmy on 22/01/2018.
 */

public class FeedbackContract {

    interface View {
        void successSubmitFeeback();

        void errorSubmitSubject(String msg);
    }

    interface Presenter {
        void submitFeedback(String subject, String content);
    }

}
