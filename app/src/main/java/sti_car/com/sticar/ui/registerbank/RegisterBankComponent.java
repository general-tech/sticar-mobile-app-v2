package sti_car.com.sticar.ui.registerbank;

import sti_car.com.sticar.utils.annot.PerActivity;
import sti_car.com.sticar.AppComponent;

import dagger.Component;

/**
 * Created by femmy on 14/01/2018.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules={RegisterBankModule.class})
public interface RegisterBankComponent {
    void inject(RegisterBankActivity activity);
}
