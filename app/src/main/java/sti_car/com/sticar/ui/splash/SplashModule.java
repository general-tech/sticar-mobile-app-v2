package sti_car.com.sticar.ui.splash;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 08/12/2017.
 */
@Module
public class SplashModule {

    private final SplashContract.View view;

    public SplashModule(SplashContract.View v){
        view = v;
    }

    @Provides
    SplashContract.View provideView(){
        return view;
    }
}
