package sti_car.com.sticar.ui.forgetpassword;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 08/02/2018.
 */

public class ForgetPasswordPresenter extends BasePresenter<ForgetPasswordContract.View>
    implements  ForgetPasswordContract.Presenter{
    @Inject
    NetworkUtil mNetworkUtil;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription mSubscriptions;

    @Inject
    public ForgetPasswordPresenter(ForgetPasswordContract.View view) {
        super(view);
    }

    @Override
    public void submitForgetPassword(String email) {
        mSubscriptions.add(mNetworkUtil.getRetrofitV2().submitForgetPass(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<Boolean>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorSubmitForgetPassword(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<Boolean> response) {
                        if (response.getCode() == 200) {
                            if(response.getData()){
                                view.onSuccessSubmitForgetPassword(response.getMessage());
                            } else {
                                view.onErrorSubmitForgetPassword(response.getMessage());
                            }
                        } else {
                            view.onErrorSubmitForgetPassword(response.getMessage());
                        }
                    }
                })
        );
    }
}
