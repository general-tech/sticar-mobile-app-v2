package sti_car.com.sticar.ui.registeraccount;

/**
 * Created by femmy on 13/01/2018.
 */

public class RegisteraccountContract {

    interface View {
        void onEmailValid();

        void onEmailInvalid(String msg);
    }

    interface Presenter {
        void checkEmail(String email);
    }
}
