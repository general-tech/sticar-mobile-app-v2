package sti_car.com.sticar.ui.settingbank;

import java.util.List;

import sti_car.com.sticar.data.models.Bank;
import sti_car.com.sticar.data.models.BankAccount;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.Reponses.BankAccountResponse;
import sti_car.com.sticar.data.models.UserProfile;

/**
 * Created by femmy on 14/02/2018.
 */

public class SettingBankContract {

    interface View {

        void onRequestBankListSuccess(List<Bank> banks);

        void onRequestBankListFailed(String msg);


        void onBankAccountValid(BankAccountResponse bankAccountResponse);

        void onBankAccountInvalid(String msg);

        void onSavingDataBankSuccess();

        void onErrorSavingDataBank(String msg);

    }

    interface Presenter {
        void requestBankList();

        void checkBankAccountHolder(BankAccount mbankAccount);

        void saveDateBank(UserProfile profile);
    }
}
