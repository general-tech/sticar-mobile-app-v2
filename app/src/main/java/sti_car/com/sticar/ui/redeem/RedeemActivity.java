package sti_car.com.sticar.ui.redeem;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Transaction;
import sti_car.com.sticar.ui.tnc.TncActivity;
import sti_car.com.sticar.utils.AppUtil;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RedeemActivity extends AppCompatActivity
        implements RedeemContract.View
        , View.OnClickListener{

    @Inject
    RedeemPresenter presenter;

    RedeemComponent actvComponent;

    private RelativeLayout rlWrapNoPhoto, rlWrapHasPhoto;

    private ImageView imgRedeem;

    private ImageButton imgbtnClearImage;

    private TextView tvLinkTNC;

    private CheckBox cbRedeemTnc;

    private Bitmap myBitmap;
    private Uri picUri;

    private Button btnRedeem;

    private SweetAlertDialog mDialogLoader;

    private Transaction mTrans = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem);

        actvComponent = DaggerRedeemComponent.builder()
                .redeemModule(new RedeemModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rlWrapNoPhoto       = (RelativeLayout) findViewById(R.id.rl_wrap_icon_no_photo);
        rlWrapNoPhoto.setOnClickListener(this);
        rlWrapHasPhoto      = (RelativeLayout) findViewById(R.id.rl_wrap_photo);
        btnRedeem           = (Button) findViewById(R.id.btn_redeem);
        btnRedeem.setOnClickListener(this);
        imgRedeem           = (ImageView) findViewById(R.id.img_redeem);
        tvLinkTNC           = (TextView)findViewById(R.id.tv_tnc_link);
        tvLinkTNC.setOnClickListener(this);
        imgbtnClearImage    = (ImageButton) findViewById(R.id.btn_clear_img);
        imgbtnClearImage.setOnClickListener(this);

        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
        mDialogLoader.setCancelable(false);

        cbRedeemTnc         = (CheckBox) findViewById(R.id.cbx_agree_tnc);
        presenter.checkCurrentRedeem();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == rlWrapNoPhoto.getId()){
            setImageRedeem();
        } else if(v.getId() == btnRedeem.getId()){
            if(cbRedeemTnc.isChecked()){
                if(myBitmap != null){
                    String imgBlob = AppUtil.encodeTobase64(myBitmap);
                    Transaction transaction = new Transaction();
                    transaction.setImgBlob(imgBlob);
                    presenter.requestRedeem(transaction);
                    mDialogLoader.show();
                }
            }
        } else if(v.getId() == tvLinkTNC.getId()){
            Intent i = new Intent(RedeemActivity.this, TncActivity.class);
            startActivity(i);
        } else if(v.getId() == imgbtnClearImage.getId()){
            clearImage();
        }
    }


    private void setImageRedeem(){
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap;
        if (resultCode == Activity.RESULT_OK) {
            if (getPickImageResultUri(data) != null) {
                picUri = getPickImageResultUri(data);
                try {
                    myBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), picUri);
                    myBitmap = rotateImageIfRequired(myBitmap, picUri);
                    myBitmap = getResizedBitmap(myBitmap, 500);
                    storeImage();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                bitmap = (Bitmap) data.getExtras().get("data");
                myBitmap = bitmap;
                storeImage();
            }
        }
    }

    private void storeImage(){
        imgRedeem.setImageBitmap(myBitmap);
        rlWrapNoPhoto.setVisibility(View.GONE);
        rlWrapHasPhoto.setVisibility(View.VISIBLE);
    }

    private void clearImage(){
        myBitmap = null;
        imgRedeem.setImageBitmap(null);
        rlWrapNoPhoto.setVisibility(View.VISIBLE);
        rlWrapHasPhoto.setVisibility(View.GONE);
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }


        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {
        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public void onRequestRedeemSuccess() {
        mDialogLoader.dismiss();
        Toast.makeText(this,"Request redeem berhasil dikirim.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestRedeemError(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(this,msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheckCurrentRedeemSuccess(Transaction trans) {
        mTrans = trans;
    }

    @Override
    public void onCheckCurrentRedeemError(String msg) {

    }
}
