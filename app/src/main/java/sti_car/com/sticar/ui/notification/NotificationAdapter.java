package sti_car.com.sticar.ui.notification;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sti_car.rschsticar10.R;

import java.util.List;

import sti_car.com.sticar.data.models.Notification;
import sti_car.com.sticar.ui.notificationdetail.NotificationdetailActivity;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;

/**
 * Created by femmy on 11/02/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter{

    private List<Notification> notifs;

    private Context context;


    public class NotifViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgNotif;
        private TextView tvNotifTitle, tvNotifExceprt;
        private LinearLayout llNotif;

        public NotifViewHolder(View itemView) {
            super(itemView);
            llNotif         = (LinearLayout) itemView.findViewById(R.id.ll_notif);
            imgNotif        = (ImageView) itemView.findViewById(R.id.img_notif);
            tvNotifTitle    = (TextView) itemView.findViewById(R.id.tv_notif_title);
            tvNotifExceprt  = (TextView) itemView.findViewById(R.id.tv_notif_excerpt);
        }
    }

    public NotificationAdapter(Context mContext, List<Notification> mNotifs){
        context     = mContext;
        notifs      = mNotifs;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notification, parent, false);
        return new NotifViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Notification mNotif       = notifs.get(position);
        NotifViewHolder notifHOlder     = (NotifViewHolder) holder;
        notifHOlder.tvNotifTitle.setText(mNotif.getTitle());
        notifHOlder.tvNotifExceprt.setText(AppUtil.strLimit(mNotif.getExcerpt(),60));
        Glide.with(context).load(mNotif.getImageUrl()).placeholder(R.drawable.default_shortlogo)
                .dontAnimate().into(notifHOlder.imgNotif);
        notifHOlder.llNotif.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String uuid = mNotif.getNotificationUUID();
                Intent i = new Intent(context, NotificationdetailActivity.class);
                i.putExtra(AppConstant.EXTRA_NOTIFICATION_UUID_KEY,uuid);
                context.startActivity(i);
            }
        });
    }

    public void setData(List<Notification> mNotifs){
        notifs = mNotifs;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return notifs.size();
    }
}
