package sti_car.com.sticar.ui.registervehicletype;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 25/01/2018.
 */

@Module
public class RegisterVehicleTypeModule {
    private final RegisterVehicleTypeContract.View view;

    public RegisterVehicleTypeModule(RegisterVehicleTypeContract.View v){
        view = v;
    }

    @Provides
    RegisterVehicleTypeContract.View provideView(){
        return view;
    }
}

