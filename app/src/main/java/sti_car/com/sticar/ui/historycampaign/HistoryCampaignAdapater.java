package sti_car.com.sticar.ui.historycampaign;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sti_car.rschsticar10.R;

import java.util.List;

import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.utils.AppUtil;

/**
 * Created by femmy on 17/02/2018.
 */

public class HistoryCampaignAdapater extends RecyclerView.Adapter {

    private List<Campaign> campaigns;

    private Context context;

    public class CampaignViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgCampaign;
        private TextView tvCampaignName, tvCampaignDate;
        private CardView cardViewCampaign;

        public CampaignViewHolder(View itemView) {
            super(itemView);
            cardViewCampaign            = (CardView) itemView.findViewById(R.id.cardview_campaign_history);
            imgCampaign                 = (ImageView) itemView.findViewById(R.id.img_campaign_history);
            tvCampaignName              = (TextView) itemView.findViewById(R.id.tv_campaign_history_name);
            tvCampaignDate              = (TextView) itemView.findViewById(R.id.tv_campaign_history_date);
        }
    }

    public HistoryCampaignAdapater(Context mContext, List<Campaign> mCampaigns){
        context         = mContext;
        campaigns       = mCampaigns;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_campaign_history, parent, false);
        return new CampaignViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Campaign campaign                       = campaigns.get(position);
        CampaignViewHolder campaignHolder       = (CampaignViewHolder) holder;
        campaignHolder.tvCampaignName.setText(AppUtil.strLimit(campaign.getName(),30) );
        campaignHolder.tvCampaignDate.setText(AppUtil.datetimeToDateString(campaign.getStartAt()));
        Glide.with(context).load(campaign.getImageUrl()).placeholder(R.drawable.default_shortlogo)
                .dontAnimate().into(campaignHolder.imgCampaign);
    }

    public void setData(List<Campaign> mCampaigns){
        campaigns = mCampaigns;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return campaigns.size();
    }
}
