package sti_car.com.sticar.ui.forgetpassword;

/**
 * Created by femmy on 08/02/2018.
 */

public class ForgetPasswordContract {

    interface View {
        void onSuccessSubmitForgetPassword(String msg);

        void onErrorSubmitForgetPassword(String msg);
    }

    interface Presenter {
        void submitForgetPassword(String email);
    }
}
