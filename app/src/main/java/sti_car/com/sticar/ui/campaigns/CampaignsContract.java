package sti_car.com.sticar.ui.campaigns;

import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.CampaignItem;
import sti_car.com.sticar.data.models.UserAccount;

import java.util.ArrayList;

/**
 * Created by femmy on 09/01/2018.
 */

public class CampaignsContract {

    interface View {
        void campaignListLoaded(ArrayList<Campaign> campaignsList);

        void campaignListErrorLoaded();

        void onUserInitialized(UserAccount mUserAccount);
    }

    interface Presenter {

        void loadCampaignList(int limit);

        void initUser();

    }
}
