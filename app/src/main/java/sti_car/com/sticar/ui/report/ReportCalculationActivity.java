package sti_car.com.sticar.ui.report;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.sti_car.rschsticar10.R;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Report.Monthly;
import sti_car.com.sticar.data.models.Report.Report;
import sti_car.com.sticar.data.models.Report.Today;
import sti_car.com.sticar.ui.reportcalculation.ReportCalculationContract;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.data.models.Report.Daily;
import sti_car.com.sticar.ui.report.DaggerReportComponent;

import java.util.ArrayList;

import javax.inject.Inject;

public class ReportCalculationActivity extends AppCompatActivity {

    @Inject
    ReportCalculationPresenter presenter;

    ReportComponent actvComponent;

    private LineChart daily,monthly;
    private TextView tvDailyKm,tvDailyCredit,tvTodayKm,tvTodayCredit,tvMonthlyKm,tvMonthlyCredit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_calculation);

//        actvComponent = DaggerReportComponent.builder()
//                .reportCalculationModule(new ReportCalculationModule(this))
//                .appComponent(App.get(this).getComponent()).build();
//        actvComponent.inject(this);
//        renderView();
//        presenter.getCalculationReport();
    }

    private void renderView(){
//        setupChartDaily();
//        setupChartMonthly();
//        monthly         = (LineChart) findViewById(R.id.monthlyChart);
//
//        //TextView
//        tvDailyKm       = (TextView) findViewById(R.id.tvDailyKm);
//        tvDailyCredit   = (TextView) findViewById(R.id.tvDailyCredit);
//        tvTodayKm       = (TextView) findViewById(R.id.tvTodayKm);
//        tvTodayCredit   = (TextView) findViewById(R.id.tvTodayCredit);
//        tvMonthlyKm     = (TextView) findViewById(R.id.tvMonthlyKm);
//        tvMonthlyCredit = (TextView) findViewById(R.id.tvMonthCredit);

    }

//    private void setupChartDaily() {
//        daily           = (LineChart) findViewById(R.id.dailyChart);
//        Description desc = new Description();
//        desc.setText("");
//        daily.setDescription(desc);
//        daily.setTouchEnabled(true);
//        daily.setDragEnabled(true);
//        daily.setScaleEnabled(true);
//        daily.setPinchZoom(false);
//        daily.setDoubleTapToZoomEnabled(false);
//        daily.setDrawGridBackground(false);
//        daily.getAxisLeft().setDrawGridLines(false);
//        daily.getAxisLeft().setDrawLabels(false);
//        daily.getAxisRight().setEnabled(false);
//        daily.getXAxis().setDrawGridLines(false);
//        daily.getXAxis().setDrawAxisLine(true);
//        daily.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
//        daily.getXAxis().setTextColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.green_light));
//        daily.getXAxis().setAvoidFirstLastClipping(true);
//        daily.getXAxis().setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                switch (((int) value)) {
//                    case 0:
//                        return getResources().getString(R.string.sun);
//                    case 1:
//                        return getResources().getString(R.string.mon);
//                    case 2:
//                        return getResources().getString(R.string.tue);
//                    case 3:
//                        return getResources().getString(R.string.wed);
//                    case 4:
//                        return getResources().getString(R.string.thu);
//                    case 5:
//                        return getResources().getString(R.string.fri);
//                    case 6:
//                        return getResources().getString(R.string.sat);
//                    default:
//                        break;
//                }
//                return null;
//            }
//        });
//    }
//
//    private void setupChartMonthly(){
//        monthly         = (LineChart) findViewById(R.id.monthlyChart);
//        Description desc = new Description();
//        desc.setText("");
//        monthly.setDescription(desc);
//        monthly.setTouchEnabled(true);
//        monthly.setDragEnabled(true);
//        monthly.setScaleEnabled(true);
//        monthly.setPinchZoom(false);
//        monthly.setDoubleTapToZoomEnabled(false);
//        monthly.setDrawGridBackground(false);
//
//        monthly.getAxisLeft().setDrawGridLines(false);
//        monthly.getAxisLeft().setDrawLabels(false);
//
//        monthly.getAxisRight().setEnabled(false);
//
//        monthly.getXAxis().setDrawGridLines(false);
//        monthly.getXAxis().setDrawAxisLine(true);
//        monthly.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
//        monthly.getXAxis().setTextColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.green_light));
//        monthly.getXAxis().setAvoidFirstLastClipping(true);
//        monthly.getXAxis().setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                switch (((int) value)) {
//                    case 0:
//                        return getResources().getString(R.string.sun);
//                    case 1:
//                        return getResources().getString(R.string.mon);
//                    case 2:
//                        return getResources().getString(R.string.tue);
//                    case 3:
//                        return getResources().getString(R.string.wed);
//                    case 4:
//                        return getResources().getString(R.string.thu);
//                    case 5:
//                        return getResources().getString(R.string.fri);
//                    case 6:
//                        return getResources().getString(R.string.sat);
//                    default:
//                        break;
//                }
//                return null;
//            }
//        });
//    }
//
//    @Override
//    public void successRequestReport(Report mReport) {
//        final Daily mReportDaily        = mReport.getDaily();
//        final Monthly mReportMonthly    = mReport.getMonthly();
//        if (mReportDaily != null) {
//            if (mReportDaily.getCharts() != null) {
//                if (mReportDaily.getCharts().size() > 0) {
//                    ArrayList<Entry> arrayList = new ArrayList<Entry>();
//                    for (int i = 0; i < mReportDaily.getCharts().size(); i++) {
//                        arrayList.add(new Entry(i, mReportDaily.getCharts().get(i).getDistance().floatValue()));
//                        daily.getXAxis().setValueFormatter(new IAxisValueFormatter() {
//                            @Override
//                            public String getFormattedValue(float value, AxisBase axis) {
//                                if((int) value >= 0 && (int) value < 7){
//                                    return mReportDaily.getCharts().get((int) value).getDay();
//                                }
//                                return null;
//                            }
//                        });
//                    }
//                    LineDataSet dataSetDaily = new LineDataSet(arrayList, "");
//                    dataSetDaily.setHighlightEnabled(false);
//                    dataSetDaily.setLineWidth(4f);
//                    dataSetDaily.setCircleRadius(4.5f);
//                    dataSetDaily.setDrawValues(true);
//                    dataSetDaily.setColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.yellow));
//                    dataSetDaily.setCircleColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.green_light));
//                    dataSetDaily.setValueTextSize(10);
//
//                    LineData data = new LineData(dataSetDaily);
//                    daily.setData(data);
//                    daily.animateXY(2000, 2000);
//                    daily.invalidate();
//                }
//            }
//            tvDailyKm.setText(String.valueOf(mReportDaily.getDistance())+" Km");
//            tvDailyCredit.setText("Rp. "+ AppUtil.formatCurrency(mReportDaily.getCredit()));
//        }
//
//        Today mTodayReport = mReport.getToday();
//        if (mReport.getToday() != null) {
//            tvTodayKm.setText(String.valueOf(mTodayReport.getDistance())+" Km");
//            tvTodayCredit.setText("Rp. "+ AppUtil.formatCurrency(mReportDaily.getCredit()));
//        }
//
//        if (mReportMonthly != null) {
//            if (mReportMonthly.getCharts() != null) {
//                if (mReportMonthly.getCharts().size() > 0) {
//                    ArrayList<Entry> arrayList = new ArrayList<Entry>();
//                    for (int i = 0; i < mReportMonthly.getCharts().size(); i++) {
//                        arrayList.add(new Entry(i, mReportMonthly.getCharts().get(i).getDistance().floatValue()));
//                    }
//
//                    LineDataSet dataSetMonthly = new LineDataSet(arrayList, "");
//                    dataSetMonthly.setHighlightEnabled(false);
//                    dataSetMonthly.setLineWidth(4f);
//                    dataSetMonthly.setCircleRadius(4.5f);
//                    dataSetMonthly.setDrawValues(true);
//                    dataSetMonthly.setColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.yellow));
//                    dataSetMonthly.setCircleColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.green_light));
//                    dataSetMonthly.setValueTextSize(10);
//
//                    LineData data = new LineData(dataSetMonthly);
//                    monthly.setData(data);
//                    monthly.animateXY(2000, 2000);
//                    monthly.invalidate();
//                }
//            }
//            tvMonthlyKm.setText(String.valueOf(mReportMonthly.getDistance())+" Km");
//            tvMonthlyCredit.setText("Rp. "+AppUtil.formatCurrency(mReportMonthly.getCredit()));
//        }
//
//    }
//
//    @Override
//    public void errorRequestReport(String msg) {
//        Toast.makeText(ReportCalculationActivity.this, msg,Toast.LENGTH_SHORT).show();
//    }
}
