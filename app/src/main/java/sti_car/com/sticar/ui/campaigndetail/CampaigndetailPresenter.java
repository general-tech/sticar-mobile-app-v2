package sti_car.com.sticar.ui.campaigndetail;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.CampaignDetail;
import sti_car.com.sticar.data.models.Reponses.JoinAds;
import sti_car.com.sticar.data.models.JoinAdsParam;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.StatusCampaign;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserData;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 09/01/2018.
 */

public class CampaigndetailPresenter extends BasePresenter<CampaigndetailContract.View> implements CampaigndetailContract.Presenter {

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public CampaigndetailPresenter(CampaigndetailContract.View view) {
        super(view);
    }

    @Override
    public void initCampaign(String idWrap) {
        if(!idWrap.isEmpty()) {
            retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getDetailCampaign(idWrap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<Campaign>>() {
                    @Override
                    public void onCompleted(){}

                    @Override
                    public void onError(Throwable e){
                        Log.d(AppConstant.APP_TAG, "Error : "+e.getMessage());
                        Toast.makeText(context, "Error here 2 : "+e.getMessage(),Toast.LENGTH_SHORT).show();
                        view.errorIniatializedCampaign();
                    }

                    @Override
                    public void onNext(Response<Campaign> response){
                        if(response.getCode() == 200){
                            view.onCampaignInitialized(response.getData());
                        } else {
                            Toast.makeText(context, "Error here 3",Toast.LENGTH_SHORT).show();
                            view.errorIniatializedCampaign();
                        }
                    }
                })
            );
        } else {
            Toast.makeText(context, "Error here 1",Toast.LENGTH_SHORT).show();
            view.errorIniatializedCampaign();
        }
    }

    private void updateAccountStatusCampaign(StatusCampaign mStatCampaign){
        UserAccount mUserAccount = mSessionManager.getUserAccount();
        UserData mUserData = mUserAccount.getData();
        mUserData.setStatusCampaign(mStatCampaign);
        mUserAccount.setData(mUserData);
        mSessionManager.setUserAccount(mUserAccount);
    }

    @Override
    public void joinCampaign(String wrappingUUID, String campaignUUID) {
        if(!wrappingUUID.equalsIgnoreCase("") && !campaignUUID.equalsIgnoreCase("")){
            JoinAdsParam mJoinAdsParam = new JoinAdsParam();
            mJoinAdsParam.setWrappingUUID(wrappingUUID);
            mJoinAdsParam.setCampaignUUID(campaignUUID);
            retrofitSubscription.add(mNetworkUtil.getRetrofitV2().joinCampaign(mJoinAdsParam)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<Campaign>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.errorJoinCampaign(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<Campaign> response) {
                        if (response.getCode() == 200) {
                            UserAccount mUserAcc = mSessionManager.getUserAccount();
                            mUserAcc.setCampaign(response.getData());
                            mSessionManager.setUserAccount(mUserAcc);
                            view.successJoinCampaign();
                        } else {
                            view.errorJoinCampaign(response.getMessage());
                        }
                    }
                })
            );
        } else {
            String msg = "Invalid post. ";
            view.errorJoinCampaign(msg);
        }
    }
}
