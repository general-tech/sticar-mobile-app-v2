package sti_car.com.sticar.ui.privacypolicy;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 23/01/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={PrivacyPolicyModule.class})
public interface PrivacyPolicyComponent {
    void inject(PrivacyPolicyActivity feedbackActivity);
}
