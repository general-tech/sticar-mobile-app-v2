package sti_car.com.sticar.ui.login;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.gson.Gson;

import io.realm.Realm;
import sti_car.com.sticar.data.models.User;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static android.provider.Settings.Secure.ANDROID_ID;

/**
 * Created by femmy on 11/12/2017.
 */

public class LoginPresenter extends BasePresenter<LoginContract.View> implements LoginContract.Presenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Named("Realm")
    @Inject
    Realm realm;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription mSubscriptions;

    @Inject
    public LoginPresenter(LoginContract.View view) {
        super(view);
        //mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void doLogin(String email, String password) {
        if(!checkSelfPermissionLocation()){
            view.onPermissionDenied();
        } else if(!formValidation(email,password)){
            view.onLoginFailed("Invalid email or password");
        } else {
            String android_id = Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID);;
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            user.setDeviceId(android_id);
            postLoginToServer(user);
        }
    }

    private boolean checkSelfPermissionLocation(){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    private boolean formValidation(String email, String password){
        if(email.trim().isEmpty()){
            return false;
        } else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()) {
            return false;
        } else if(email.trim().isEmpty()) {
            return false;
        }
        return true;
    }

    private void postLoginToServer(User user){
        mSubscriptions.add(mNetworkUtil.getRetrofitV2().login(user)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<UserAccount>() {
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {
                    view.onLoginFailed(e.getMessage());
                }

                @Override
                public void onNext(UserAccount mUserAcc) {
                    if (mUserAcc != null) {
                        mUserAcc.setSessionAsLoggedIn();
                        //
                        mUserAcc = appendProfile(mUserAcc);

                        mSessionManager.setUserAccount(mUserAcc);
                        reinitRealm();
                        view.onLoginSuccess();
                    } else {
                        view.onLoginFailed("Login failed, Recheck your email and password.");
                    }
                }
            })
        );
    }

    private UserAccount appendProfile(UserAccount userAccount){
        String json = "{'fullname':'Femmy Pramana','email':'femmy@sti-car.com','phone_number':'0924324234324','photo_url':'http://res.cloudinary.com/sticar/image/upload/v1516794073/default-photo_rafyug.jpg','bank_code':'BCA','bank_account_number':'70103434324','bank_account_name':'femmy pramana','bank_account_verified':true,'car_brand_id':3,'car_brand_name':'Honda','car_model_id':15,'car_model_name':'Mobilio','car_color_id':11,'car_color_name':'Putih','motorcycle_brand_id':3,'motorcycle_brand_name':'Honda','motorcycle_model_id':34,'motorcycle_model_name':'BeAT','motorcycle_color_id':1,'motorcycle_color_name':'Marun'}";

        Gson gson = new Gson();
        UserProfile profile = gson.fromJson(json,UserProfile.class);
        userAccount.setProfile(profile);
        // new TypeToken<List<Staff>>(){}.getType()

        return userAccount;
    }


    private void reinitRealm(){
        if(realm.isClosed()){
            Realm.init(context);
            realm = Realm.getDefaultInstance();
        }
    }

}
