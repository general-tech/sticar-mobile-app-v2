package sti_car.com.sticar.ui.registerbank;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Bank;
import sti_car.com.sticar.data.models.BankAccount;
import sti_car.com.sticar.data.models.BankBranch;
import sti_car.com.sticar.data.models.Register;
import sti_car.com.sticar.data.models.Reponses.BankAccountResponse;
import sti_car.com.sticar.ui.registervehicle.RegisterVehicleActivity;
import sti_car.com.sticar.ui.registervehicletype.RegisterVehicleTypeActivity;
import sti_car.com.sticar.utils.AppConstant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterBankActivity extends AppCompatActivity implements
        RegisterBankContract.View
        ,View.OnClickListener
        ,TextView.OnEditorActionListener
        ,View.OnTouchListener{

    @Inject
    RegisterBankPresenter presenter;

    RegisterBankComponent actvComponent;

    private Spinner spBankName;

    private EditText etAccountNo;

    private Button btnSubmit;

    private String sBankId, sAccNo;

    private TextView tvSkipBank;

    private boolean isVerified = false;

    private SweetAlertDialog mDialogLoader;

    private Register mRegister;

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            setResetCondition();
        }

        @Override
        public void afterTextChanged(Editable s) {}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_register_bank);

        actvComponent = DaggerRegisterBankComponent.builder()
                .registerBankModule(new RegisterBankModule(this))
                .appComponent(App.get(this).getComponent()).build();

        actvComponent.inject(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mRegister = (Register) extras.getSerializable(AppConstant.EXTRA_REGISTER_KEY);
        }
        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        spBankName          = (Spinner) findViewById(R.id.sp_bank_name);
        etAccountNo         = (EditText) findViewById(R.id.et_account_no);
        etAccountNo.setOnTouchListener(this);
        etAccountNo.addTextChangedListener(mTextWatcher);

        btnSubmit           = (Button) findViewById(R.id.btn_next);
        btnSubmit.setOnClickListener(this);

        tvSkipBank          = (TextView) findViewById(R.id.tv_skip_bank);
        tvSkipBank.setOnClickListener(this);

        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
        mDialogLoader.setCancelable(false);
        mDialogLoader.show();
        presenter.requestBankList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnSubmit.getId()){
            if(isVerified){
                Intent i = new Intent(RegisterBankActivity.this, RegisterVehicleTypeActivity.class);
                i.putExtra(AppConstant.EXTRA_REGISTER_KEY,mRegister);
                startActivity(i);
            } else {
                sAccNo = etAccountNo.getText().toString();
                BankAccount mbankAccount = new BankAccount();
                mbankAccount.setBankCode(sBankId);
                mbankAccount.setBankAccountNumber(sAccNo);
                presenter.checkBankAccountHolder(mbankAccount);
                btnSubmit.setEnabled(false);
                btnSubmit.setText("Checking Bank Account...");
                mDialogLoader.show();
            }

        } else if(v.getId() == tvSkipBank.getId()){
            Intent i = new Intent(RegisterBankActivity.this, RegisterVehicleTypeActivity.class);
            i.putExtra(AppConstant.EXTRA_REGISTER_KEY,mRegister);
            startActivity(i);
        }
    }

    @Override
    public void onRequestBankListSuccess(final List<Bank> banks) {
        ArrayList<String> mBankStringList = new ArrayList<String>();
        for (int i = 0; i < banks.size(); i++){
            Bank mBank = banks.get(i);
            mBankStringList.add(mBank.getName());
        }

        ArrayAdapter<String> bankNameAdapter = new ArrayAdapter<String>(RegisterBankActivity.this,
                R.layout.spinner_layout_custom, mBankStringList);
        bankNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBankName.setAdapter(bankNameAdapter);
        spBankName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Bank bank       = banks.get(position);
                sBankId         = bank.getCode();
                setResetCondition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        mDialogLoader.dismiss();
    }

    @Override
    public void onRequestBankListFailed(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(RegisterBankActivity.this, msg,Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onBankAccountValid(BankAccountResponse bankAccountResponse) {
        mDialogLoader.dismiss();
        btnSubmit.setEnabled(true);
        btnSubmit.setText("Verifikasi Akun Bank");
        mRegister.setBankCode(sBankId);
        mRegister.setBankAccountNumber(bankAccountResponse.getAccountNumber());
        mRegister.setBankAccountName(bankAccountResponse.getAccountHolder());
        mRegister.setBankAccountVerified(true);
        setConfirmedCondition();
        Toast.makeText(RegisterBankActivity.this, "Valid Bank Account",Toast.LENGTH_SHORT).show();
//        Intent i = new Intent(RegisterBankActivity.this, RegisterVehicleTypeActivity.class);
//        i.putExtra(AppConstant.EXTRA_REGISTER_KEY, mRegister);
//        startActivity(i);
    }

    @Override
    public void onBankAccountInvalid(String msg) {
        mDialogLoader.dismiss();
        btnSubmit.setEnabled(true);
        Toast.makeText(RegisterBankActivity.this, msg,Toast.LENGTH_SHORT).show();
        setResetCondition();
    }

    private void setResetCondition(){
        mRegister.setBankCode("");
        mRegister.setBankAccountNumber("");
        mRegister.setBankAccountName("");
        mRegister.setBankAccountVerified(false);
        isVerified = false;
        btnSubmit.setText("Verifikasi Akun Bank");
        tvSkipBank.setVisibility(View.VISIBLE);
    }

    private void setConfirmedCondition(){
        isVerified = true;
        btnSubmit.setText("Selanjutnya");
        tvSkipBank.setVisibility(View.INVISIBLE);
    }
}
