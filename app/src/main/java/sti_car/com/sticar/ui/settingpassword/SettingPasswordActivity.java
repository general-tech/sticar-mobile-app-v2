package sti_car.com.sticar.ui.settingpassword;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.utils.SessionManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingPasswordActivity extends AppCompatActivity
        implements SettingPasswordContract.View
        ,View.OnClickListener
        ,TextView.OnEditorActionListener
        ,View.OnTouchListener {

    @Inject
    SettingPasswordPresenter presenter;

    SettingPasswordComponent actvComponent;

    private EditText etOldpass, etNewpass, etConpass;
    private Button btnSavePassword;
    private SweetAlertDialog mDialogLoader;

    private UserProfile mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_password);

        actvComponent = DaggerSettingPasswordComponent.builder()
                .settingPasswordModule(new SettingPasswordModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void renderView(){
        mProfile = new SessionManager(this).getUserAccount().getUserProfile();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        etOldpass       = (EditText) findViewById(R.id.et_setting_current_password);
        etOldpass.setOnTouchListener(this);
        etOldpass.setOnEditorActionListener(this);
        etNewpass       = (EditText) findViewById(R.id.et_setting_new_password);
        etNewpass.setOnTouchListener(this);
        etNewpass.setOnEditorActionListener(this);
        etConpass       = (EditText) findViewById(R.id.et_setting_confirm_password);
        etConpass.setOnTouchListener(this);
        etConpass.setOnEditorActionListener(this);

        btnSavePassword = (Button) findViewById(R.id.btn_setting_save_password);
        btnSavePassword.setOnClickListener(this);

        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
        mDialogLoader.setCancelable(false);
    }

    @Override
    public void onSuccessSavePassword(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorSavePassword(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnSavePassword.getId()){
            String oldpass = etOldpass.getText().toString();
            String newpass = etNewpass.getText().toString();
            String conpass = etConpass.getText().toString();
            if(newpass.equals(conpass)){
                mDialogLoader.show();
                mProfile.setOldPassword(oldpass);
                mProfile.setPassword(newpass);
                mProfile.setConfirmPassword(conpass);
                presenter.savePassword(mProfile);
            } else {
                Toast.makeText(this,"Konfirmasi password tidak sama.",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }
}
