package sti_car.com.sticar.ui.forgetpassword;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 08/02/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules=ForgetPasswordModule.class)
public interface ForgetPasswordComponent {
    void inject(ForgetPasswordActivity activity);
}

