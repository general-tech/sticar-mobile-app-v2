package sti_car.com.sticar.ui.main;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.urbanmobility.Line;
import com.sti_car.rschsticar10.R;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.Notification;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.Report.Lifetime;
import sti_car.com.sticar.data.models.Report.Periodic;
import sti_car.com.sticar.data.models.TodayTrip;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserData;
import sti_car.com.sticar.services.TrackService;
import sti_car.com.sticar.ui.campaigntypes.CampaigntypesActivity;
import sti_car.com.sticar.ui.feedback.FeedbackActivity;
import sti_car.com.sticar.ui.login.LoginActivity;
import sti_car.com.sticar.ui.notification.NotificationActivity;
import sti_car.com.sticar.ui.profilemain.ProfileMainActivity;
import sti_car.com.sticar.ui.reportdashboard.ReportDashboardActivity;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.data.models.Report.Today;
import sti_car.com.sticar.data.models.StatusCampaign;
import sti_car.com.sticar.data.models.UserHome;
import sti_car.com.sticar.data.models.UserParam;
import sti_car.com.sticar.ui.campaigns.CampaignsActivity;
import sti_car.com.sticar.ui.main.DaggerMainComponent;
import sti_car.com.sticar.utils.AppConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements
            NavigationView.OnNavigationItemSelectedListener
            ,MainContract.View
            ,MainContract.LocView
            ,LocationListener
            ,MapGesture.OnGestureListener
            ,View.OnClickListener{

    // Declare activity class
    @Inject
    MainPresenter presenter;

    @Inject
    MainLocPresenter locPresenter;

    private MainComponent actvComponent;

    private Intent intentTrackService;

    //--- Declare constant and variable
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    // * map fragment embedded in this activity
    private Location mLastLocation  = null;
    private Map map                 = null;
    private MapFragment mapFragment = null;
    private float mAzimuth          = 0;
    private Bitmap dwblCarMarker, bmpMarker;
    private MapMarker mMapMarker;
    private Image imgMarker;
    private GeoCoordinate mCoordinate;
    private RelativeLayout mOverlayMaps;

    private boolean allPermisionsGranted = false;

    private double totalDistanceToday, totalCreditToday;
    private UserAccount mUserAccount;

    // Declare view component
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private Button btnNavCloseDrawer;
    private TextView sideNavTvName, sideNavTvBalance, tvNavNotJoined, tvNavJoined;
    private CircleImageView sideNavImgProfile;
    private ImageView sidenavImgCampaignType;

    private android.support.design.widget.FloatingActionButton fabMenu, fabLocation, fabNotif;;
    private com.getbase.floatingactionbutton.FloatingActionButton fabNav, fabCurrLoc, fabZoomIn, fabZoomOut;


    private TextView tvCampaignType, tvCampaignTypeCurrent, tvDailyLimitNotif,tvCurrentCampaignName , tvLabelNoCampaign;

    private TextView tvTripTodayDistance, tvTripTodayBenefit, tvTripCurrentDistance, tvTripCurrentBenefit;
    private Button btnStartTrip, btnStopTrip, btnJoinCampaign;

    private LinearLayout llNoCampaign, llDataTripToday, llDataTripCurrent;
    private android.support.v7.widget.CardView carviewJoinedCampaign;

    private SweetAlertDialog mTripDialogLoader;



    //---------------------------------------------- Implementation Activity ----------------------------------------------//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        injectComponent();
        renderView();

        intentTrackService = new Intent(MainActivity.this, TrackService.class);
        checkPermission();
        presenter.initLocalUserAccountData();
        presenter.initHomeData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();
        locPresenter.setLocationListener();
        IntentFilter i = new IntentFilter(AppConstant.INTENT_FILTER_KEY_GPS_EVENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,i);
    }

    @Override
    protected void onStop() {
        super.onStop();
        locPresenter.unsetLocationListener();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(AppConstant.INTENT_FILTER_KEY_GPS_EVENT));
        if(mMapMarker != null){
            setMarker();
        }
        syncTrackService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        locPresenter.unsetLocationListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                allPermisionsGranted = true;
                break;
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------------//




    private void injectComponent(){
        actvComponent = DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
    }


    private void renderView(){

        // Binding Side Nav Header
        drawer                  = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView          = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header             = navigationView.getHeaderView(0);
        sideNavTvName           = (TextView) header.findViewById(R.id.tvName);
        sideNavTvBalance        = (TextView) header.findViewById(R.id.tvBalance);
        sideNavImgProfile       = (CircleImageView) header.findViewById(R.id.profile_image);
        sideNavImgProfile.setOnClickListener(this);
        btnNavCloseDrawer       = (Button) header.findViewById(R.id.btn_nav_close_drawer);
        btnNavCloseDrawer.setOnClickListener(this);
        tvNavNotJoined          = (TextView) header.findViewById(R.id.tv_nav_current_campaign_notjoined);
        tvNavNotJoined.setOnClickListener(this);
        tvNavJoined             = (TextView) header.findViewById(R.id.tv_nav_current_campaign_joined);
        sidenavImgCampaignType  = (ImageView) header.findViewById(R.id.img_drawer_campign_type);

        tvCurrentCampaignName       = (TextView) findViewById(R.id.tv_current_campaign);
        tvCampaignType              = (TextView) findViewById(R.id.tv_campaign_type_value);
        tvCampaignTypeCurrent       = (TextView) findViewById(R.id.tv_campaign_type_current_trip_value);
        tvTripTodayDistance         = (TextView) findViewById(R.id.tv_today_km_value);
        tvTripTodayBenefit          = (TextView) findViewById(R.id.tv_today_benefit_value);
        tvTripCurrentDistance       = (TextView) findViewById(R.id.tv_current_trip_km_value);
        tvTripCurrentBenefit        = (TextView) findViewById(R.id.tv_current_trip_benefit_value);


        btnStartTrip                = (Button) findViewById(R.id.btn_start_trip);
        btnStartTrip.setOnClickListener(this);
        btnStopTrip                 = (Button) findViewById(R.id.btn_stop_trip);
        btnStopTrip.setOnClickListener(this);
        btnJoinCampaign             = (Button) findViewById(R.id.btn_join_campaign);
        btnJoinCampaign.setOnClickListener(this);

        carviewJoinedCampaign       = (CardView) findViewById(R.id.cardview_joined_campaign);
        llNoCampaign                = (LinearLayout) findViewById(R.id.ll_has_no_campaign);
        tvLabelNoCampaign           = (TextView) findViewById(R.id.tv_label_no_campaign);

        llDataTripToday             = (LinearLayout) findViewById(R.id.ll_data_trip_today);
        llDataTripCurrent           = (LinearLayout) findViewById(R.id.ll_data_trip_current);

        tvDailyLimitNotif           = (TextView) findViewById(R.id.tv_daily_limit);

        fabMenu         = (android.support.design.widget.FloatingActionButton) findViewById(R.id.fabMenu);
        fabMenu.setOnClickListener(this);
        fabLocation     = (android.support.design.widget.FloatingActionButton)findViewById(R.id.fabLocation);
        fabLocation.setOnClickListener(this);
        fabNotif        = (android.support.design.widget.FloatingActionButton) findViewById(R.id.fabNotif);
        fabNotif.setOnClickListener(this);

        //fabNav, fabZoomIn, fabZoomOut
        fabCurrLoc      = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.fab_my_location);
        fabCurrLoc.setOnClickListener(this);
        fabZoomIn       = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.fab_zoom_in);
        fabZoomIn.setOnClickListener(this);
        fabZoomOut      = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.fab_zoom_out);
        fabZoomOut.setOnClickListener(this);

        mTripDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mTripDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mTripDialogLoader.setTitleText("Loading");

        // HERE Maps Components
        mOverlayMaps    = (RelativeLayout) findViewById(R.id.overlay_map_loader);
        Matrix matrix   = new Matrix();
        matrix.postRotate(mAzimuth);
        dwblCarMarker   = BitmapFactory.decodeResource(getResources(), R.drawable.ic_car);
        bmpMarker       = Bitmap.createBitmap(dwblCarMarker, 0, 0, dwblCarMarker.getWidth(), dwblCarMarker.getHeight(), matrix, false);
        mapFragment     = (MapFragment)getFragmentManager().findFragmentById(R.id.mapHome);
        //----------------------------------------------------------------------------------------//
    }

    private void renderSideNavheader(){
        if(mUserAccount != null){
            Driver mDriver = mUserAccount.getData().getDriver();
            sideNavTvName.setText(mDriver.getName());
            sideNavTvBalance.setText(AppUtil.formatCurrencyIDR(mDriver.getCredit()));
            sideNavImgProfile.setImageURI(Uri.parse(mDriver.getPhoto_url()));
            AppUtil.showImage(getApplicationContext(), mDriver.getPhoto_url(), sideNavImgProfile);
        }
    }


    private void checkPermission(){
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : AppConstant.REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }

        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[AppConstant.REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, AppConstant.REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    private void mapsInit() {
        if(map == null) {
            mapFragment.init(this,new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                    if (error == OnEngineInitListener.Error.NONE) {
                        // retrieve a reference of the map from the map fragment
                        map     = mapFragment.getMap();
                        mapFragment.getMapGesture().addOnGestureListener((MapGesture.OnGestureListener) MainActivity.this);
                        map.setTrafficInfoVisible(true);
                        //Set Mode Maps Day/Night
                        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                        if (hour < 18){
                            map.setMapScheme(Map.Scheme.CARNAV_DAY);
                        } else {
                            map.setMapScheme(Map.Scheme.CARNAV_NIGHT);
                        }
                        initMarker();
                    } else {
                        Log.d(AppConstant.APP_TAG,"ERROR: Cannot initialize Map Fragment: " + error.toString());
                        Log.d(AppConstant.APP_TAG,"ERROR: Cannot initialize Map Fragment: " + error.getDetails());
                    }
                }
            });
        }
    }

    public void initMarker(){
        if(mMapMarker == null){
            Matrix matrix       = new Matrix();
            matrix.postRotate(mAzimuth);
            imgMarker           = new Image();
            imgMarker.setBitmap(bmpMarker);
            mCoordinate         = new GeoCoordinate(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            mMapMarker          = new MapMarker(mCoordinate, imgMarker);
            setMarker();

            new CountDownTimer(1000,1000){
                @Override
                public void onTick(long millisUntilFinished) {}

                @Override
                public void onFinish() {
                    mOverlayMaps.setVisibility(View.GONE);
                }
            }.start();
        }

    }

    public void setMarker() {
        if(map != null && mCoordinate != null){
            mMapMarker.setCoordinate(mCoordinate);
            map.setFadingAnimations(true);
            map.addMapObject(mMapMarker);
            map.setCenter(mCoordinate, Map.Animation.BOW);
            map.setZoomLevel(AppConstant.HERE_MAPS_DEFAULT_ZOOM_LEVEL);
        }
    }
    //---------------------------------------------- Implementation Navigation View ----------------------------------------------//
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.nav_logout){
            presenter.logout();
        } else if(id == R.id.nav_notif){
            Intent i = new Intent(MainActivity.this, NotificationActivity.class);
            startActivity(i);
        } else if(id == R.id.nav_join){
            Intent i = new Intent(MainActivity.this, CampaigntypesActivity.class);
            startActivity(i);
        } else if(id == R.id.nav_feedback){
            Intent i = new Intent(MainActivity.this, FeedbackActivity.class);
            startActivity(i);
        } else if(id == R.id.nav_report){
            Intent i = new Intent(MainActivity.this, ReportDashboardActivity.class);
            startActivity(i);
        }
        drawer.closeDrawer(navigationView);
        return false;
    }

    //-----------------------------------------------------------------------------------------------------------------------------//


    //---------------------------------------------- Implementation MainContract.View ----------------------------------------------//
    @Override
    public void userInitialized(UserAccount mUser) {
        mUserAccount = mUser;
        if(mUserAccount != null){
            renderSideNavheader();
        }
    }

    @Override
    public void updateReport(Response<Periodic> report) {

    }

    @Override
    public void onTotalTodayUpdated(double mTotalDistance, double mTotalCredit) {
        totalDistanceToday  = mTotalDistance;
        totalCreditToday    = mTotalCredit;
    }

    @Override
    public void onTripStarted() {
        btnStartTrip.setText("Start Trip");
        mTripDialogLoader.dismiss();
        llDataTripCurrent.setVisibility(View.VISIBLE);
        llDataTripToday.setVisibility(View.GONE);
        if (!AppUtil.isServiceRunning(MainActivity.this, TrackService.class)) {
            startService(intentTrackService);
        }
    }

    @Override
    public void startingTripFailed(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        mTripDialogLoader.dismiss();
        btnStartTrip.setText("Start Trip");
        onTripStopped();
    }

    @Override
    public void onTripStopped() {
        if (AppUtil.isServiceRunning(MainActivity.this, TrackService.class)) {
            stopService(intentTrackService);
        }
        llDataTripCurrent.setVisibility(View.GONE);
        llDataTripToday.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUserAccountLoggedOut() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    @Override
    public void onSuccessIntiLocalUserAccountData(UserAccount mUserAcc) {
        mUserAccount = mUserAcc;
        renderSideNavheader();
    }

    @Override
    public void onErrorIntiLocalUserAccountData() {
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    @Override
    public void onSuccessInitHomeData(UserAccount mUserAcc) {
        mUserAccount = mUserAcc;
        Campaign mCampaign = mUserAcc.getCampaign();
        if(mCampaign != null){
            // Sidenav Label Campaign
            tvNavJoined.setVisibility(View.VISIBLE);
            tvNavNotJoined.setVisibility(View.GONE);
            tvNavJoined.setText(Html.fromHtml("You have joined <b>"+AppUtil.strLimit(mCampaign.getmCampaignName(),18)+"</b>"));
            if(mCampaign.getType().getName().equalsIgnoreCase("car")){
                sidenavImgCampaignType.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                        R.drawable.ic_vehicle_type_car));
            } else if(mCampaign.getType().getName().equalsIgnoreCase("motor")){
                sidenavImgCampaignType.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                        R.drawable.ic_vehicle_type_motor));
            } else {
                sidenavImgCampaignType.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                        R.drawable.ic_vehicle_no_type));
            }

            if(mCampaign.getStatus() == 1){
                carviewJoinedCampaign.setVisibility(View.VISIBLE);
                llNoCampaign.setVisibility(View.GONE);
                tvCurrentCampaignName.setText(AppUtil.strLimit(mCampaign.getName(), 40));
                tvCampaignType.setText(mCampaign.getType().getName());
                tvCampaignTypeCurrent.setText(mCampaign.getType().getName());
                tvTripTodayBenefit.setText(AppUtil.formatCurrencyIDR(mUserAcc.getData().getTodayTrip().getCredit()));
                tvTripTodayDistance.setText(mUserAcc.getData().getTodayTrip().getDistance()+" KM");
            } else {
                tvLabelNoCampaign.setText(Html.fromHtml(mCampaign.getMessage()));
                carviewJoinedCampaign.setVisibility(View.GONE);
                llNoCampaign.setVisibility(View.VISIBLE);
                btnJoinCampaign.setVisibility(View.GONE);
            }

        } else {
            tvNavNotJoined.setVisibility(View.VISIBLE);
            tvNavJoined.setVisibility(View.GONE);

            carviewJoinedCampaign.setVisibility(View.GONE);
            llNoCampaign.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onErrorInitHomeData(String msg) {
        Toast.makeText(this,msg, Toast.LENGTH_SHORT).show();
    }

    //--------------------------------------------------------------------------------------------------------------------------------//

    //------------------------------------------ Implementation MapGesture.OnGestureListener -----------------------------------------//
    @Override
    public void onPanStart() {}

    @Override
    public void onPanEnd() {}

    @Override
    public void onMultiFingerManipulationStart() {}

    @Override
    public void onMultiFingerManipulationEnd() {}

    @Override
    public boolean onMapObjectsSelected(List<ViewObject> list) {
        return false;
    }

    @Override
    public boolean onTapEvent(PointF pointF) {
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(PointF pointF) {
        return false;
    }

    @Override
    public void onPinchLocked() {}

    @Override
    public boolean onPinchZoomEvent(float v, PointF pointF) {
        return false;
    }

    @Override
    public void onRotateLocked() {}

    @Override
    public boolean onRotateEvent(float v) {
        return false;
    }

    @Override
    public boolean onTiltEvent(float v) {
        return false;
    }

    @Override
    public boolean onLongPressEvent(PointF pointF) {
        return false;
    }

    @Override
    public void onLongPressRelease() {}

    @Override
    public boolean onTwoFingerTapEvent(PointF pointF) {
        double level = map.getMinZoomLevel() + map.getMaxZoomLevel() / 2;
        map.setCenter(mCoordinate,Map.Animation.BOW);
        map.setZoomLevel(level);
        return true;
    }

    //--------------------------------------------------------------------------------------------------------------------------------//

    //---------------------------------------------- Implementation LocationListener ---------------------------------------------//
    @Override
    public void onLocationChanged(Location location) {
        if(location != null && mCoordinate != null){
            mLastLocation = location;
            mCoordinate.setLatitude(mLastLocation.getLatitude());
            mCoordinate.setLongitude(mLastLocation.getLongitude());
            setMarker();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}
    //--------------------------------------------------------------------------------------------------------------------------------//


    //---------------------------------------------- Implementation MainContract.locView ---------------------------------------------//
    @Override
    public void onNoNetworkProvider() {}

    @Override
    public void onLocationListenerSet(Location l) {
        mLastLocation = l;
        mapsInit();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == fabMenu.getId()){
            if (!drawer.isDrawerOpen(navigationView)) {
                drawer.openDrawer(navigationView);
            } else {
                drawer.closeDrawer(navigationView);
            }
        } else if(v.getId() == fabNotif.getId()){
            Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
            startActivity(intent);
        } else if(v.getId() == fabCurrLoc.getId()){
            setMarker();
        } else if(v.getId() == fabZoomIn.getId()){
            if (map != null) {
                map.setZoomLevel(map.getZoomLevel() + 1, com.here.android.mpa.mapping.Map.Animation.LINEAR);
            }
        } else if(v.getId() == fabZoomOut.getId()){
            if (map != null) {
                map.setZoomLevel(map.getZoomLevel() - 1, com.here.android.mpa.mapping.Map.Animation.LINEAR);
            }
        } else if(v.getId() == btnStartTrip.getId()){
            btnStartTrip.setText("Starting Trip");
            mTripDialogLoader.show();
            presenter.startTrip();

        } else if(v.getId() == btnStopTrip.getId()){
            onTripStopped();
        } else if(v.getId() == btnNavCloseDrawer.getId()){
            drawer.closeDrawer(navigationView);
        } else if(v.getId() == tvNavNotJoined.getId()){
            Intent intent = new Intent(MainActivity.this,CampaigntypesActivity.class);
            startActivity(intent);
            finish();
        } else if(v.getId() == btnJoinCampaign.getId()){
            Intent intent = new Intent(MainActivity.this,CampaigntypesActivity.class);
            startActivity(intent);
        } else if(v.getId() == sideNavImgProfile.getId()) {
            Intent intent = new Intent(MainActivity.this,ProfileMainActivity.class);
            startActivity(intent);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------//


    private void syncTrackService(){
        if (!AppUtil.isServiceRunning(MainActivity.this, TrackService.class)) {
            onTripStopped();
        } else {
            onTripStarted();
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            // Get extra data included in the Intent
            String type = intent.getStringExtra("type");
            String value = intent.getStringExtra("value");

            if(type.equalsIgnoreCase(AppConstant.TRACK_SERIVCE_MSG_KEY_UPDATE_TOTAL)){
                TrackServiceUpdateTotalCurrent(value);
            } else if(type.equalsIgnoreCase(AppConstant.TRACK_SERIVCE_MSG_KEY_UPDATE_TOTAL_TODAY)){
                TrackServiceUpdateTotalToday(value);
            } else if(type.equalsIgnoreCase(AppConstant.TRACK_SERIVCE_MSG_KEY_UPDATE_LIMIT)){
                tvDailyLimitNotif.setText(value);
                tvDailyLimitNotif.setVisibility(View.VISIBLE);
            }
        }
    };

    private void TrackServiceUpdateTotalCurrent(String extra){
        try {
            JSONObject obj = new JSONObject(extra);
            String fromServiceCurrentDistance   = obj.getString("distance");
            String fromServiceCurrentBenefit    = obj.getString("benefit");
            tvTripCurrentDistance.setText(fromServiceCurrentDistance);
            tvTripCurrentBenefit.setText(fromServiceCurrentBenefit);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void TrackServiceUpdateTotalToday(String extra){
        try {
            JSONObject obj = new JSONObject(extra);
            String fromServiceCurrentDistance   = obj.getString("distance");
            String fromServiceCurrentBenefit    = obj.getString("benefit");
            tvTripTodayDistance.setText(fromServiceCurrentDistance);
            tvTripTodayBenefit.setText(fromServiceCurrentBenefit);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
