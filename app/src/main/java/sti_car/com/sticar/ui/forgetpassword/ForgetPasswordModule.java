package sti_car.com.sticar.ui.forgetpassword;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 08/02/2018.
 */

@Module
public class ForgetPasswordModule {

    private final ForgetPasswordContract.View view;

    public ForgetPasswordModule(ForgetPasswordContract.View view) {
        this.view = view;
    }

    @Provides
    ForgetPasswordContract.View provideView(){
        return view;
    }
}
