package sti_car.com.sticar.ui.reportdashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;
import com.wang.avi.AVLoadingIndicatorView;

import javax.inject.Inject;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Report.Lifetime;
import sti_car.com.sticar.ui.redeem.RedeemActivity;
import sti_car.com.sticar.ui.reportcalculation.ReportCalculationActivity;
import sti_car.com.sticar.ui.reporthistory.ReportHistoryActivity;
import sti_car.com.sticar.utils.AppUtil;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReportDashboardActivity extends AppCompatActivity
    implements ReportDashboardContract.View
    , View.OnClickListener{

    @Inject
    ReportDashboardPresenter presenter;

    ReportDashboardComponent actvComponent;

    private TextView tvTotalCredit;

    private AVLoadingIndicatorView avloaderTotalCredit;

    private CardView cvCalculation, cvRedeem, cvHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_dashboard);

        actvComponent = DaggerReportDashboardComponent.builder()
                .reportDashboardModule(new ReportDashboardModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
        presenter.requestReportTotal();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void renderView(){
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }


        avloaderTotalCredit     = (AVLoadingIndicatorView) findViewById(R.id.avloader_total_credit);
        tvTotalCredit           = (TextView) findViewById(R.id.tv_repdash_total_credit);
        cvCalculation           = (CardView) findViewById(R.id.cardview_repdash_calculation);
        cvCalculation.setOnClickListener(this);
        cvRedeem                = (CardView) findViewById(R.id.cardview_repdash_redeem);
        cvRedeem.setOnClickListener(this);
        cvHistory               = (CardView) findViewById(R.id.cardview_repdash_history);
        cvHistory.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == cvCalculation.getId()){
            Intent i = new Intent(ReportDashboardActivity.this, ReportCalculationActivity.class);
            startActivity(i);
        } else if(v.getId() == cvRedeem.getId()) {
            Intent i = new Intent(ReportDashboardActivity.this, RedeemActivity.class);
            startActivity(i);
        } else if(v.getId() == cvHistory.getId()) {
            Intent i = new Intent(ReportDashboardActivity.this, ReportHistoryActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void onSuccessRequestReport(Lifetime report) {
        tvTotalCredit.setText(AppUtil.formatCurrencyIDR(report.getCredit()));
        avloaderTotalCredit.setVisibility(View.GONE);
        tvTotalCredit.setVisibility(View.VISIBLE);
    }

    @Override
    public void onErrorRequestReport(String msg) {
        Toast.makeText(this, msg,Toast.LENGTH_SHORT).show();
        avloaderTotalCredit.setVisibility(View.GONE);
        tvTotalCredit.setVisibility(View.VISIBLE);
        tvTotalCredit.setText("-");
    }
}
