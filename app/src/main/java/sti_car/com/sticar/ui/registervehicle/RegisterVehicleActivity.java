package sti_car.com.sticar.ui.registervehicle;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import java.util.ArrayList;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.CarBrand;
import sti_car.com.sticar.data.models.CarModel;
import sti_car.com.sticar.data.models.MotorBrand;
import sti_car.com.sticar.data.models.MotorModel;
import sti_car.com.sticar.data.models.Register;
import sti_car.com.sticar.data.models.VehicleColor;
import sti_car.com.sticar.ui.registerconfirm.RegisterConfirmActivity;
import sti_car.com.sticar.utils.AppConstant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterVehicleActivity extends AppCompatActivity implements
        RegisterVehicleContract.View
        ,AdapterView.OnItemSelectedListener
        ,View.OnClickListener{

    @Inject
    RegisterVehiclePresenter presenter;

    RegisterVehicleComponent actvComponent;

    private Register mRegister;

    private String VEHICLE_TYPE;

    private ArrayList<CarBrand> carBrands               = new ArrayList<CarBrand>();

    private ArrayList<MotorBrand> motorBrands           = new ArrayList<MotorBrand>();

    private ArrayList<CarModel> carModels               = new ArrayList<CarModel>();

    private ArrayList<MotorModel> motorModels           = new ArrayList<MotorModel>();

    private ArrayList<VehicleColor> vehicleColors       = new ArrayList<VehicleColor>();

    private LinearLayout llVehicleCar, llVehicleMotor;

    private Spinner spCarBrand, spCarModel, spCarColor, spMotorBrand, spMotorModel, spMotorColor;

    private Button btnNext;

    private SweetAlertDialog mDialogLoader, mDialogLoaderCarModel, mDialogLoaderMotorModel;

    private int carBrandId, carModelId, carColorId, motorBrandId, motorModelId, motorColorId;

    private String carBrandName, carModelName, carColorName, motorBrandName, motorModelName, motorColorName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setContentView(R.layout.activity_register_vehicle);

        actvComponent = DaggerRegisterVehicleComponent.builder()
                .registerVehicleModule(new RegisterVehicleModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);

        Bundle extras = getIntent().getExtras();
        VEHICLE_TYPE = AppConstant.EXTRA_VEHICLE_TYPE_BOTH;
        if (extras != null) {
            mRegister = (Register) extras.getSerializable(AppConstant.EXTRA_REGISTER_KEY);
            VEHICLE_TYPE = extras.getString(AppConstant.EXTRA_KEY_VEHICLE_TYPE,AppConstant.EXTRA_VEHICLE_TYPE_BOTH);
            Log.v(AppConstant.APP_TAG, "VEHICLE_TYPE : "+VEHICLE_TYPE);
        }
        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void renderView(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        llVehicleCar        = (LinearLayout) findViewById(R.id.ll_wrap_form_car);
        llVehicleMotor      = (LinearLayout) findViewById(R.id.ll_wrap_form_motor);

        spCarBrand          = (Spinner) findViewById(R.id.sp_car_brand);
        spCarBrand.setOnItemSelectedListener(this);
        spCarModel          = (Spinner) findViewById(R.id.sp_car_model);
        spCarModel.setOnItemSelectedListener(this);
        spCarColor          = (Spinner) findViewById(R.id.sp_car_color);
        spCarColor.setOnItemSelectedListener(this);

        spMotorBrand        = (Spinner) findViewById(R.id.sp_motor_brand);
        spMotorBrand.setOnItemSelectedListener(this);
        spMotorModel        = (Spinner) findViewById(R.id.sp_motor_model);
        spMotorModel.setOnItemSelectedListener(this);
        spMotorColor        = (Spinner) findViewById(R.id.sp_motor_color);
        spMotorColor.setOnItemSelectedListener(this);

        btnNext             = (Button) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);

        if(VEHICLE_TYPE.equalsIgnoreCase(AppConstant.EXTRA_VEHICLE_TYPE_CAR)){
            llVehicleCar.setVisibility(View.VISIBLE);
            llVehicleMotor.setVisibility(View.GONE);
            presenter.requestCarBrands();
        } else if(VEHICLE_TYPE.equalsIgnoreCase(AppConstant.EXTRA_VEHICLE_TYPE_MOTOR)){
            llVehicleCar.setVisibility(View.GONE);
            llVehicleMotor.setVisibility(View.VISIBLE);
            presenter.requestMotorBrands();
        } else {
            llVehicleCar.setVisibility(View.VISIBLE);
            llVehicleMotor.setVisibility(View.VISIBLE);
            presenter.requestCarBrands();
            presenter.requestMotorBrands();
        }
        presenter.requestVehicleColor();
        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
        mDialogLoader.setCancelable(false);
        mDialogLoaderCarModel       = mDialogLoader;
        mDialogLoaderMotorModel     = mDialogLoader;

        mDialogLoader.show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnNext.getId()){
            mRegister.setCarBrandId(carBrandId);
            mRegister.setCarBrandName(carBrandName);
            mRegister.setCarModelId(carModelId);
            mRegister.setCarModelName(carModelName);
            mRegister.setCarColorId(carColorId);
            mRegister.setCarColorName(carColorName);

            mRegister.setMotorcycleBrandId(motorBrandId);
            mRegister.setMotorcycleBrandName(motorBrandName);
            mRegister.setMotorcycleModelId(motorModelId);
            mRegister.setMotorcycleModelName(motorModelName);
            mRegister.setMotorcycleColorId(motorColorId);
            mRegister.setMotorcycleColorName(motorColorName);

            Log.d(AppConstant.APP_TAG, "carBrandId :"+carBrandId+" - carBrandName :"+carBrandName);
            Log.d(AppConstant.APP_TAG, "carModelId :"+carModelId+" - carModelName : "+carModelName);
            Log.d(AppConstant.APP_TAG, "carColorId :"+carColorId+" - carColorName : "+carColorName);

            Log.d(AppConstant.APP_TAG, "motorBrandId :"+motorBrandId+" - motorBrandName :"+motorBrandName);
            Log.d(AppConstant.APP_TAG, "motorModelId :"+motorModelId+" - motorModelName : "+motorModelName);
            Log.d(AppConstant.APP_TAG, "motorColorId :"+motorColorId+" - motorColorName : "+motorColorName);

            Intent i = new Intent(RegisterVehicleActivity.this, RegisterConfirmActivity.class);
            i.putExtra(AppConstant.EXTRA_REGISTER_KEY,mRegister);
            startActivity(i);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(parent.getId() == spCarBrand.getId()){
            CarBrand mCarBrand = carBrands.get(position);
            carBrandId      = mCarBrand.getId();
            carBrandName    = mCarBrand.getName();
            presenter.requestCarModel(carBrandId);
            mDialogLoaderCarModel.show();
        } else if(parent.getId() == spCarModel.getId()){
            CarModel mCarModel = carModels.get(position);
            carModelId      = mCarModel.getId();
            carModelName    = mCarModel.getName();
        } else if(parent.getId() == spCarColor.getId()){
            VehicleColor vehicleColor = vehicleColors.get(position);
            carColorId = vehicleColor.getId();
            carColorName = vehicleColor.getName();
        } else if(parent.getId() == spMotorBrand.getId()){
            MotorBrand mMotorBrand = motorBrands.get(position);
            motorBrandId = mMotorBrand.getId();
            motorBrandName = mMotorBrand.getName();
            presenter.requestMotorModel(motorBrandId);
            mDialogLoaderMotorModel.show();
        } else if(parent.getId() == spMotorModel.getId()){
            MotorModel mMotorModel = motorModels.get(position);
            motorModelId      = mMotorModel.getId();
            motorModelName    = mMotorModel.getName();
        } else if(parent.getId() == spMotorColor.getId()){
            VehicleColor vehicleColor = vehicleColors.get(position);
            motorColorId = vehicleColor.getId();
            motorColorName = vehicleColor.getName();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onSuccessRequestCarBrands(ArrayList<CarBrand> brands) {
        carBrands = brands;
        ArrayList<String> carBrandNames = new ArrayList<String>();
        for (int i = 0; i < brands.size(); i++){
            CarBrand mCarBrand = brands.get(i);
            carBrandNames.add(mCarBrand.getName());
        }

        ArrayAdapter<String> carBrandAdapter = new ArrayAdapter<String>(RegisterVehicleActivity.this,
                R.layout.spinner_layout_custom, carBrandNames);
        carBrandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCarBrand.setAdapter(carBrandAdapter);
    }

    @Override
    public void onErrorRequestCarBrands(String msg) {
        Toast.makeText(RegisterVehicleActivity.this, msg, Toast.LENGTH_SHORT);
    }

    @Override
    public void onSuccessRequestMotorBrands(ArrayList<MotorBrand> brands) {
        motorBrands = brands;
        ArrayList<String> motorBrandNames = new ArrayList<String>();
        for (int i = 0; i < brands.size(); i++){
            MotorBrand mMotorBrand = brands.get(i);
            motorBrandNames.add(mMotorBrand.getName());
        }

        ArrayAdapter<String> carBrandAdapter = new ArrayAdapter<String>(RegisterVehicleActivity.this,
                R.layout.spinner_layout_custom, motorBrandNames);
        carBrandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMotorBrand.setAdapter(carBrandAdapter);
    }

    @Override
    public void onErrorRequestMotorBrands(String msg) {
        Toast.makeText(RegisterVehicleActivity.this, msg, Toast.LENGTH_SHORT);
    }

    @Override
    public void onSuccessRequestCarModels(ArrayList<CarModel> models) {
        carModels = models;
        ArrayList<String> carModelNames = new ArrayList<String>();
        for (int i = 0; i < models.size(); i++){
            CarModel mCarModel = models.get(i);
            carModelNames.add(mCarModel.getName());
        }

        ArrayAdapter<String> carModelAdapter = new ArrayAdapter<String>(RegisterVehicleActivity.this,
                R.layout.spinner_layout_custom, carModelNames);
        carModelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCarModel.setAdapter(carModelAdapter);
        mDialogLoaderCarModel.dismiss();
    }

    @Override
    public void onErrorRequestCarModels(String msg) {
        mDialogLoaderCarModel.dismiss();
        Toast.makeText(RegisterVehicleActivity.this, msg, Toast.LENGTH_SHORT);
    }

    @Override
    public void onSuccessRequestMotorModels(ArrayList<MotorModel> models) {
        motorModels = models;
        ArrayList<String> motortModelNames = new ArrayList<String>();
        for (int i = 0; i < models.size(); i++){
            MotorModel mMotorModel = models.get(i);
            motortModelNames.add(mMotorModel.getName());
        }

        ArrayAdapter<String> motorModelAdapter = new ArrayAdapter<String>(RegisterVehicleActivity.this,
                R.layout.spinner_layout_custom, motortModelNames);
        motorModelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMotorModel.setAdapter(motorModelAdapter);
        mDialogLoaderMotorModel.dismiss();
    }

    @Override
    public void onErrorRequestMotorModels(String msg) {
        mDialogLoaderMotorModel.dismiss();
        Toast.makeText(RegisterVehicleActivity.this, msg, Toast.LENGTH_SHORT);
    }

    @Override
    public void onSuccessRequestVehicleColors(ArrayList<VehicleColor> colors) {
        vehicleColors = colors;
        ArrayList<String> vehicleColors = new ArrayList<String>();
        for (int i = 0; i < colors.size(); i++){
            VehicleColor vehicleColor = colors.get(i);
            vehicleColors.add(vehicleColor.getName());
        }

        ArrayAdapter<String> vehicleColorAdapter = new ArrayAdapter<String>(RegisterVehicleActivity.this,
                R.layout.spinner_layout_custom, vehicleColors);
        vehicleColorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCarColor.setAdapter(vehicleColorAdapter);
        spMotorColor.setAdapter(vehicleColorAdapter);
    }

    @Override
    public void onErrorRequestVehicleColors(String msg) {
        Toast.makeText(RegisterVehicleActivity.this, msg, Toast.LENGTH_SHORT);
        mDialogLoader.dismiss();
    }
}
