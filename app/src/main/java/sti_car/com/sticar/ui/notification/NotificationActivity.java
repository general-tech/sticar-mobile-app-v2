package sti_car.com.sticar.ui.notification;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import javax.inject.Inject;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Notification;
import sti_car.com.sticar.ui.login.DaggerLoginComponent;
import sti_car.com.sticar.ui.login.LoginModule;
import sti_car.com.sticar.utils.AppConstant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NotificationActivity extends AppCompatActivity
    implements NotificationContract.View,
        SwipeRefreshLayout.OnRefreshListener{

    @Inject
    NotificationPresenter presenter;

    NotificationComponent actvComponent;

    private NotificationAdapter mNotifAdapter;

    private ArrayList<Notification> mNotifs = new ArrayList<Notification>();
    private SwipeRefreshLayout swipeContainer;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private AVLoadingIndicatorView avLoadingIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        actvComponent = DaggerNotificationComponent.builder()
                .notificationModule(new NotificationModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    private void renderView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        swipeContainer                  = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeResources(R.color.green_light);
        swipeContainer.setOnRefreshListener(this);
        avLoadingIndicator              = (AVLoadingIndicatorView) findViewById(R.id.avi_loader);

        mLayoutManager                  =  new LinearLayoutManager(this);
        mRecyclerView                   = (RecyclerView) findViewById(R.id.rv_notif);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mNotifAdapter                   = new NotificationAdapter(this,mNotifs);
        mRecyclerView.setAdapter(mNotifAdapter);
        mNotifAdapter.notifyDataSetChanged();
        presenter.requestNotif();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onSuccessRequestNotif(ArrayList<Notification> notifs) {
        swipeContainer.setRefreshing(false);
        avLoadingIndicator.hide();
        mNotifAdapter.setData(notifs);
        mNotifAdapter.notifyDataSetChanged();
    }

    @Override
    public void onErrorRequestNotif(String msg) {
        avLoadingIndicator.hide();
        Toast.makeText(this, msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        presenter.requestNotif();
    }
}
