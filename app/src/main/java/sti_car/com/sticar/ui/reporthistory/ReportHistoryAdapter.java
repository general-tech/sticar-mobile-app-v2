package sti_car.com.sticar.ui.reporthistory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sti_car.rschsticar10.R;

import java.util.List;

import sti_car.com.sticar.data.models.TransactionRedeem;
import sti_car.com.sticar.utils.AppUtil;

/**
 * Created by femmy on 14/02/2018.
 */

public class ReportHistoryAdapter extends RecyclerView.Adapter {

    private List<TransactionRedeem> data;

    private Context context;


    public class ReportHistoryViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDate, tvTotal;

        public ReportHistoryViewHolder(View itemView) {
            super(itemView);
            tvDate      = (TextView) itemView.findViewById(R.id.tv_report_history_date);
            tvTotal     = (TextView) itemView.findViewById(R.id.tv_report_history_total);
        }
    }

    public ReportHistoryAdapter(List<TransactionRedeem> mData, Context mCtx){
        data        = mData;
        context     = mCtx;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_report_history, parent, false);
        return new ReportHistoryViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TransactionRedeem mTransRedeem = data.get(position);
        ReportHistoryViewHolder repHistoryHolder     = (ReportHistoryViewHolder) holder;
        repHistoryHolder.tvDate.setText(AppUtil.datetimeToDateString(mTransRedeem.getDatetime()));
        repHistoryHolder.tvTotal.setText(AppUtil.formatCurrencyIDR(mTransRedeem.getTotal()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<TransactionRedeem> e){
        data = e;
        notifyDataSetChanged();
    }
}
