package sti_car.com.sticar.ui.feedback;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 22/01/2018.
 */

@Module
public class FeedbackModule {

    private final FeedbackContract.View view;

    public FeedbackModule(FeedbackContract.View v){
        view = v;
    }

    @Provides
    FeedbackContract.View provideView(){
        return view;
    }
}
