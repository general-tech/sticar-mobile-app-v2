package sti_car.com.sticar.ui.settingbank;

import android.content.Context;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Bank;
import sti_car.com.sticar.data.models.BankAccount;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.Reponses.BankAccountResponse;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 14/02/2018.
 */

public class SettingBankPresenter extends BasePresenter<SettingBankContract.View>
    implements SettingBankContract.Presenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public SettingBankPresenter(SettingBankContract.View view) {
        super(view);
    }

    @Override
    public void requestBankList() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getListBank()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<ArrayList<Bank>>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onRequestBankListFailed("Error request bank list");
                    }

                    @Override
                    public void onNext(Response<ArrayList<Bank>> response) {
                        if(response.getCode() == 200){
                            view.onRequestBankListSuccess(response.getData());
                        } else {
                            view.onRequestBankListFailed("Error request bank list");
                        }
                    }
                })
        );
    }

    @Override
    public void checkBankAccountHolder(BankAccount mbankAccount) {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().checkBankAccount(mbankAccount)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<BankAccountResponse>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onBankAccountInvalid("Error checking bank account, try again later.");
                    }

                    @Override
                    public void onNext(Response<BankAccountResponse> response) {
                        if(response.getCode() == 200){
                            view.onBankAccountValid(response.getData());
                        } else {
                            view.onBankAccountInvalid(response.getMessage());
                        }
                    }
                })
        );
    }


    @Override
    public void saveDateBank(UserProfile profile) {
                retrofitSubscription.add(mNetworkUtil.getRetrofitV2().editDataBank(profile)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<UserProfile>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorSavingDataBank(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<UserProfile> response) {
                        if(response.getCode() == 200){
                            UserAccount mUserAccount = mSessionManager.getUserAccount();
                            mUserAccount.setProfile(response.getData());
                            mSessionManager.setUserAccount(mUserAccount);
                            view.onSavingDataBankSuccess();
                        } else {
                            view.onErrorSavingDataBank("Error save data bank");
                        }
                    }
                })
        );
    }
}
