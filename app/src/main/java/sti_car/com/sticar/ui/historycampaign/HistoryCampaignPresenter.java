package sti_car.com.sticar.ui.historycampaign;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import javax.inject.Inject;

import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.ui.base.BasePresenter;

/**
 * Created by femmy on 14/02/2018.
 */

public class HistoryCampaignPresenter extends BasePresenter<HistoryCampaignContract.View>
    implements HistoryCampaignContract.Presenter{

    @Inject
    public HistoryCampaignPresenter(HistoryCampaignContract.View view) {
        super(view);
    }

    @Override
    public void requestHistoryCampaign() {
        String json = "{'status':true,'code':200,'data':[{'id':1,'uuid':'dd3c3ee1-0bc4-11e8-93e7-06a3d53a0c0e','name':'TEST | Sticar Car 1.0 TEST | Sticar Car 1.0 TEST | Sticar Car 1.0','advertiser_name':'Advertiser Sticar','start_at':'2018-02-17 00:00','wrapping_type':'Rear Window','image_url':'http://res.cloudinary.com/sticar/image/upload/v1517826275/full_premium_rgkc9s.jpg','area':'KOTA JAKARTA SELATAN','type':{'id':1,'name':'CAR'},'status':1,'message':'Active'}],'message':'Successfully show all Campaigns','language':'en'}";
        Gson gson = new Gson();
        // new TypeToken<List<Staff>>(){}.getType()
        Response<ArrayList<Campaign>> response = gson.fromJson(json,new TypeToken<Response<ArrayList<Campaign>>>(){}.getType());
        view.onSuccessRequestHistoryCampaign(response.getData());

    }
}
