package sti_car.com.sticar.ui.registervehicle;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 23/01/2018.
 */

@Module
public class RegisterVehicleModule {
    private final RegisterVehicleContract.View view;

    public RegisterVehicleModule(RegisterVehicleContract.View v){
        view = v;
    }

    @Provides
    RegisterVehicleContract.View provideView(){
        return view;
    }
}
