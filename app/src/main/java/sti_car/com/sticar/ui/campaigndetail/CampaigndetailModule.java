package sti_car.com.sticar.ui.campaigndetail;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 09/01/2018.
 */
@Module
public class CampaigndetailModule {

    private final CampaigndetailContract.View view;

    public CampaigndetailModule(CampaigndetailContract.View v){
        view = v;
    }

    @Provides
    CampaigndetailContract.View provideView(){
        return view;
    }
}
