package sti_car.com.sticar.ui.campaigntypes;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.sti_car.rschsticar10.R;

import java.util.List;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.CampaignType;
import sti_car.com.sticar.ui.main.MainActivity;
import sti_car.com.sticar.ui.campaigns.CampaignsActivity;
import sti_car.com.sticar.ui.campaigntypes.DaggerCampaigntypesComponent;
import sti_car.com.sticar.utils.AppConstant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import javax.inject.Inject;

public class CampaigntypesActivity extends AppCompatActivity
        implements CampaigntypesContract.View, View.OnClickListener {

    @Inject
    CampaigntypesPresenter presenter;

    CampaigntypesComponent actvComponent;

    private Button btnCampignTypeCar, btnCampignTypeMotor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaigntype);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        actvComponent = DaggerCampaigntypesComponent.builder()
                .campaigntypesModule(new CampaigntypesModule(this))
                .appComponent(App.get(this).getComponent()).build();

        actvComponent.inject(this);
        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        btnCampignTypeCar   = (Button) findViewById(R.id.btn_campaign_type_car);
        btnCampignTypeCar.setOnClickListener(this);
        btnCampignTypeMotor = (Button) findViewById(R.id.btn_campaign_type_motor);
        btnCampignTypeMotor.setOnClickListener(this);
    }

//    private void backToMain(){
//        Intent i = new Intent(CampaigntypesActivity.this, MainActivity.class);
//        startActivity(i);
//        finish();
//    }

    @Override
    public void onBackPressed(){
        ActivityManager mngr = (ActivityManager) getSystemService( ACTIVITY_SERVICE );

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if(taskList.get(0).numActivities == 1 && taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            Intent i = new Intent(CampaigntypesActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnCampignTypeCar.getId()){
            Intent i = new Intent(CampaigntypesActivity.this, CampaignsActivity.class);
            i.putExtra(AppConstant.EXTRA_CAMPAIGN_TYPE_KEY, CampaignType.CAMPAIGN_TYPE_CAR);
            startActivity(i);
        } else if(v.getId() == btnCampignTypeMotor.getId()) {
            Intent i = new Intent(CampaigntypesActivity.this, CampaignsActivity.class);
            i.putExtra(AppConstant.EXTRA_CAMPAIGN_TYPE_KEY, CampaignType.CAMPAIGN_TYPE_MOTOR);
            startActivity(i);
        }
    }
}
