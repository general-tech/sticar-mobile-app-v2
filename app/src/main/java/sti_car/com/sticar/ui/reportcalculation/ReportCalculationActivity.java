package sti_car.com.sticar.ui.reportcalculation;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.here.android.mpa.urbanmobility.Line;
import com.sti_car.rschsticar10.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Reponses.ReportPeriodic;
import sti_car.com.sticar.data.models.Report.Chart;
import sti_car.com.sticar.data.models.Report.Daily;
import sti_car.com.sticar.data.models.Report.Monthly;
import sti_car.com.sticar.data.models.Report.Today;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReportCalculationActivity extends AppCompatActivity implements
    ReportCalculationContract.View{

    @Inject
    ReportCalculationPresenter presenter;

    ReportCalculationComponent actvComponent;

    private Toolbar toolbar;

    private TextView tvTodayDistance, tvTodayBenefit, tvLegendDaily, tvLegendMonthly;

    private LineChart lcDaily, lcMonthly;

    private RelativeLayout popupLoaderGeneral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_calculation);

        actvComponent = DaggerReportCalculationComponent.builder()
                .reportCalculationModule(new ReportCalculationModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
        presenter.requestReport();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
//        getSupportActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        lcDaily             = (LineChart) findViewById(R.id.linechart_daily);
        setupChartDaily();
        lcMonthly           = (LineChart) findViewById(R.id.linechart_monthly);
        setupMonthly();
        tvTodayDistance     = (TextView) findViewById(R.id.tv_repcalc_today_distance_value);
        tvTodayBenefit      = (TextView) findViewById(R.id.tv_repcalc_today_benefit_value);
        tvLegendDaily       = (TextView) findViewById(R.id.tv_legend_value_daily);
        tvLegendMonthly     = (TextView) findViewById(R.id.tv_legend_value_monthly);
        popupLoaderGeneral  = (RelativeLayout) findViewById(R.id.rl_popup_loader_general);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable upArrow = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_arrow_left);
        upArrow.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green_dark), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Back
                onBackPressed();
            }
        });

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar_header_calc);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    getSupportActionBar().setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_grd_radial_green_blue));
                    collapsingToolbarLayout.setTitle("Report Calculation");
                    Drawable upArrow = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_arrow_left);
                    upArrow.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.white), PorterDuff.Mode.SRC_ATOP);
                    getSupportActionBar().setHomeAsUpIndicator(upArrow);
                    isShow = true;
                } else if(isShow) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    Drawable upArrow = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_arrow_left);
                    upArrow.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green_dark), PorterDuff.Mode.SRC_ATOP);
                    getSupportActionBar().setHomeAsUpIndicator(upArrow);
                    isShow = false;
                }
            }
        });
    }

    private void setupChartDaily() {
        Description desc = new Description();
        desc.setText("");
        lcDaily.setDescription(desc);
        lcDaily.setTouchEnabled(true);
        lcDaily.setDragEnabled(true);
        lcDaily.setScaleEnabled(true);
        lcDaily.setPinchZoom(false);
        lcDaily.setDoubleTapToZoomEnabled(false);
        lcDaily.setDrawGridBackground(false);
        lcDaily.getAxisLeft().setDrawGridLines(false);
        lcDaily.getAxisLeft().setDrawLabels(false);
        lcDaily.getAxisRight().setEnabled(false);
        lcDaily.getXAxis().setDrawGridLines(false);
        lcDaily.getXAxis().setDrawAxisLine(true);
        lcDaily.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lcDaily.getXAxis().setTextColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.green_light));
        lcDaily.getXAxis().setAvoidFirstLastClipping(true);

    }

    private void setupMonthly() {
        Description desc = new Description();
        desc.setText("");
        lcMonthly.setDescription(desc);
        lcMonthly.setTouchEnabled(true);
        lcMonthly.setDragEnabled(true);
        lcMonthly.setScaleEnabled(true);
        lcMonthly.setPinchZoom(false);
        lcMonthly.setDoubleTapToZoomEnabled(false);
        lcMonthly.setDrawGridBackground(false);

        lcMonthly.getAxisLeft().setDrawGridLines(false);
        lcMonthly.getAxisLeft().setDrawLabels(false);

        lcMonthly.getAxisRight().setEnabled(false);

        lcMonthly.getXAxis().setDrawGridLines(false);
        lcMonthly.getXAxis().setDrawAxisLine(true);
        lcMonthly.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lcMonthly.getXAxis().setTextColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.green_light));
        lcMonthly.getXAxis().setAvoidFirstLastClipping(true);
    }

    @Override
    public void onSuccessRequestReport(ReportPeriodic report) {
        popupLoaderGeneral.setVisibility(View.GONE);
        Today mToday = report.getToday();
        tvTodayDistance.setText(mToday.getDistance()+" KM");
        tvTodayBenefit.setText(AppUtil.formatCurrencyIDR(mToday.getCredit()));

        final ArrayList<Daily> dailyData = report.getChartDaily();
        mapDataDaily(dailyData);
        lcDaily.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                int idx = (int) value;
                return dailyData.get(idx).getDay();
            }
        });

        final ArrayList<Monthly> monthlyData = report.getChartMonthly();
        mapDataMonthly(monthlyData);
        lcMonthly.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                int idx = (int) value;
                return monthlyData.get(idx).getMonth();
            }
        });
    }

    private void mapDataDaily(ArrayList<Daily> dailyData){
        List<Entry> entries = new ArrayList<Entry>();
        for (int i =0; i < dailyData.size(); i++) {
            Daily mDaily = dailyData.get(i);
            float x = i;
            float y = mDaily.getDistance().floatValue();
            Entry mEntry = new Entry(x,y);
            entries.add(mEntry);
        }

        LineDataSet dataSetDaily = new LineDataSet(entries, "Distance Per Day");
        dataSetDaily.setHighlightEnabled(false);
        dataSetDaily.setLineWidth(4f);
        dataSetDaily.setCircleRadius(4.5f);
        dataSetDaily.setDrawValues(true);
        dataSetDaily.setColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.yellow));
        dataSetDaily.setCircleColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.green_light));
        dataSetDaily.setValueTextSize(10);

        LineData data = new LineData(dataSetDaily);
        lcDaily.setData(data);
        lcDaily.animateXY(1000, 1000);
        lcDaily.invalidate();
    }

    private void mapDataMonthly(ArrayList<Monthly> monthlyData){
        List<Entry> entries = new ArrayList<Entry>();
        for (int i =0; i < monthlyData.size(); i++) {
            Monthly mMonthly = monthlyData.get(i);
            float x = i;
            float y = mMonthly.getDistance().floatValue();
            Entry mEntry = new Entry(x,y);
            entries.add(mEntry);
        }

        LineDataSet dataSetMonthly = new LineDataSet(entries, "Distance Per Month");
        dataSetMonthly.setHighlightEnabled(false);
        dataSetMonthly.setLineWidth(4f);
        dataSetMonthly.setCircleRadius(4.5f);
        dataSetMonthly.setDrawValues(true);
        dataSetMonthly.setColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.yellow));
        dataSetMonthly.setCircleColor(ContextCompat.getColor(ReportCalculationActivity.this, R.color.green_light));
        dataSetMonthly.setValueTextSize(10);
        LineData data = new LineData(dataSetMonthly);
        lcMonthly.setData(data);
        lcMonthly.animateXY(1000, 1000);
        lcMonthly.invalidate();
    }

    @Override
    public void onErrorrequestReport(String msg) {

    }
}
