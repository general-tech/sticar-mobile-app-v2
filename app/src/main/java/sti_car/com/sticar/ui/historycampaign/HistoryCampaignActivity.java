package sti_car.com.sticar.ui.historycampaign;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import javax.inject.Inject;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Campaign;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HistoryCampaignActivity extends AppCompatActivity implements
    HistoryCampaignContract.View
    ,SwipeRefreshLayout.OnRefreshListener{

    @Inject
    HistoryCampaignPresenter presenter;

    HistoryCampaignComponent actvComponent;

    private HistoryCampaignAdapater mCampaignAdapter;

    private ArrayList<Campaign> mCampaigns = new ArrayList<Campaign>();
    private SwipeRefreshLayout swipeContainer;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private AVLoadingIndicatorView avLoadingIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_campaign);

        actvComponent = DaggerHistoryCampaignComponent.builder()
                .historyCampaignModule(new HistoryCampaignModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    private void renderView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        swipeContainer                  = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeContainer.setColorSchemeResources(R.color.green_light);
        swipeContainer.setOnRefreshListener(this);
        avLoadingIndicator              = (AVLoadingIndicatorView) findViewById(R.id.avi_loader);

        mLayoutManager                  =  new LinearLayoutManager(this);
        mRecyclerView                   = (RecyclerView) findViewById(R.id.rv_history);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mCampaignAdapter                = new HistoryCampaignAdapater(this,mCampaigns);
        mRecyclerView.setAdapter(mCampaignAdapter);
        mCampaignAdapter.notifyDataSetChanged();
        presenter.requestHistoryCampaign();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        swipeContainer.setRefreshing(true);
        presenter.requestHistoryCampaign();
    }

    @Override
    public void onSuccessRequestHistoryCampaign(ArrayList<Campaign> campaigns) {
        swipeContainer.setRefreshing(false);
        avLoadingIndicator.hide();
        mCampaignAdapter.setData(campaigns);
        mCampaignAdapter.notifyDataSetChanged();
    }

    @Override
    public void onErrorRequestHistoryCampaign(String msg) {
        swipeContainer.setRefreshing(true);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }
}
