package sti_car.com.sticar.ui.campaigns;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Advertiser;
import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.CampaignItem;
import sti_car.com.sticar.data.models.StatusCampaign;
import sti_car.com.sticar.data.models.UserAccount;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.sti_car.rschsticar10.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.iwgang.countdownview.CountdownView;
import sti_car.com.sticar.ui.campaigns.DaggerCampaignsComponent;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CampaignsActivity extends AppCompatActivity implements
        CampaignsContract.View,
        View.OnClickListener,
//        CampaignsAdapter.OnLoadMoreListener,
        SwipeRefreshLayout.OnRefreshListener{

    @Inject
    CampaignsPresenter presenter;

    CampaignsComponent actvComponent;

    private LinearLayoutManager mLayoutManager;
    private RecyclerViewHeader mRVHeader;
    private RecyclerView mRecyclerView;
    private CountdownView countdownView;
    private SwipeRefreshLayout swipeContainer;
    private Toolbar toolbar;
//    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout appBarLayout;
    private AVLoadingIndicatorView avLoadingIndicatorView;
    private LinearLayout ll_container_header_campaign;
    private TextView tvTitleCampaign,tvTitleAdvertiser,tvEmpty,tvSdate,tvEdate, tvDaysLeftVal, tvDaysLeftLabel;

    private ArrayList<Campaign> mCampaignList = new ArrayList<Campaign>();
    private CampaignsAdapter mCampaignAdapter;

    private int limit = 5;
    private boolean isShow = false;
    private int scrollRange = -1;

    UserAccount mUserAccount;

    private int CAMPAIGN_TYPE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaigns);

        actvComponent = DaggerCampaignsComponent.builder()
                .campaignsModule(new CampaignsModule(this))
                .appComponent(App.get(this).getComponent()).build();

        actvComponent.inject(this);
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            CAMPAIGN_TYPE = extras.getInt(AppConstant.EXTRA_CAMPAIGN_TYPE_KEY);
        }
        renderView();



        presenter.initUser();
        avLoadingIndicatorView.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        mLayoutManager                  =  new LinearLayoutManager(this);
        mRecyclerView                   = (RecyclerView) findViewById(R.id.rv_campaign);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRVHeader                       =  (RecyclerViewHeader) findViewById(R.id.rv_campaign_header);

        countdownView                   = (CountdownView) findViewById(R.id.cv_countdownView);
        swipeContainer                  = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeResources(R.color.green_light);
        swipeContainer.setOnRefreshListener(this);
        avLoadingIndicatorView          = (AVLoadingIndicatorView) findViewById(R.id.avi);
        ll_container_header_campaign    = (LinearLayout) findViewById(R.id.ll_container_header_campaign);
        tvEmpty                         = (TextView) findViewById(R.id.tvEmpty);

        tvTitleCampaign                 = (TextView) findViewById(R.id.tv_campaign_title);
        tvTitleAdvertiser               = (TextView) findViewById(R.id.tv_advertiser_name);
        tvSdate                         = (TextView) findViewById(R.id.tv_date_start_value);
        tvEdate                         = (TextView) findViewById(R.id.tv_date_end_value);
        tvDaysLeftVal                   = (TextView) findViewById(R.id.tv_days_left_val);
        tvDaysLeftLabel                 = (TextView) findViewById(R.id.tv_days_left_lbl);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mCampaignAdapter = new CampaignsAdapter(this,mCampaignList);
        mRecyclerView.setAdapter(mCampaignAdapter);
        mCampaignAdapter.notifyDataSetChanged();
        presenter.loadCampaignList(CAMPAIGN_TYPE);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == toolbar.getId()){
            onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void campaignListLoaded(ArrayList<Campaign> campaignsList) {
        avLoadingIndicatorView.hide();
        swipeContainer.setRefreshing(false);
        if (campaignsList.size() > 0) {
            mCampaignAdapter.setData(campaignsList);
        }

    }

    @Override
    public void campaignListErrorLoaded() {
        Toast.makeText(this, "Error loading campaign list", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserInitialized(UserAccount mUserAcc) {
        mUserAccount = mUserAcc;
        if(mUserAccount != null){
            Campaign mCampaign = mUserAccount.getCampaign();
            if(mCampaign != null) {
                Log.d(AppConstant.APP_TAG,"mCampaign = "+mCampaign.getmCampaignName());
                mRVHeader.attachTo(mRecyclerView);
                tvTitleAdvertiser.setText(mCampaign.getAdvertiserName());
                tvTitleCampaign.setText(AppUtil.strLimit(mCampaign.getmCampaignName(),35));
                tvEdate.setText(mCampaign.getEndDateFormat());
                tvSdate.setText(mCampaign.getStartDateFormat());
                tvDaysLeftLabel.setText(mCampaign.getElapsedTimesLabel()+" left");
                tvDaysLeftVal.setText(mCampaign.getElapsedTimesValue());

                if(mCampaign.getElapsedTimesLabel() == Campaign.DAYS ){
                    if(Integer.valueOf(mCampaign.getElapsedTimesValue()) > 7)
                        ll_container_header_campaign.setBackgroundResource(R.color.green_dark);
                    else if(Integer.valueOf(mCampaign.getElapsedTimesValue()) > 2){
                        ll_container_header_campaign.setBackgroundResource(R.color.yellow_dark);
                    } else {
                        ll_container_header_campaign.setBackgroundResource(R.color.red);
                    }
                } else {
                    ll_container_header_campaign.setBackgroundResource(R.color.red);
                }
            } else {
                ll_container_header_campaign.setVisibility(View.GONE);
            }
        } else {
            ll_container_header_campaign.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                limit += 10;
                mCampaignList.clear();
                mCampaignAdapter.notifyDataSetChanged();
                mRecyclerView.setAdapter(mCampaignAdapter);
                presenter.loadCampaignList(CAMPAIGN_TYPE);
                avLoadingIndicatorView.show();
            }
        }, 1000);
    }
}
