package sti_car.com.sticar.ui.notification;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import javax.inject.Inject;

import sti_car.com.sticar.data.models.Notification;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppConstant;

/**
 * Created by femmy on 10/02/2018.
 */

public class NotificationPresenter extends BasePresenter<NotificationContract.View>
        implements NotificationContract.Presenter {

    @Inject
    public NotificationPresenter(NotificationContract.View view) {
        super(view);
    }

    @Override
    public void requestNotif() {
        String json = "{'status':true,'code':200,'data':[{'notification_uuid':'','title':'campaign','excerpt':'Anda telah bergabung campaign bla bla bla','image_url':'http://res.cloudinary.com/sticar/image/upload/v1517826275/full_premium_rgkc9s.jpg','content':'','status':0,'created_at':'2018-03-07 05:07:58'},{'notification_uuid':'','title':'campaign','excerpt':'Anda telah bergabung campaign bla bla bla','image_url':'http://res.cloudinary.com/sticar/image/upload/v1517826275/full_premium_rgkc9s.jpg','content':'','status':0,'created_at':'2018-03-07 05:07:58'},{'notification_uuid':'','title':'campaign','excerpt':'Anda telah bergabung campaign bla bla bla','image_url':'http://res.cloudinary.com/sticar/image/upload/v1517826275/full_premium_rgkc9s.jpg','content':'','status':0,'created_at':'2018-03-07 05:07:58'},{'notification_uuid':'','title':'campaign','excerpt':'Anda telah bergabung campaign bla bla bla','image_url':'http://res.cloudinary.com/sticar/image/upload/v1517826275/full_premium_rgkc9s.jpg','content':'','status':0,'created_at':'2018-03-07 05:07:58'},{'notification_uuid':'','title':'campaign','excerpt':'Anda telah bergabung campaign bla bla bla','image_url':'http://res.cloudinary.com/sticar/image/upload/v1517826275/full_premium_rgkc9s.jpg','content':'','status':0,'created_at':'2018-03-07 05:07:58'},{'notification_uuid':'','title':'campaign','excerpt':'Anda telah bergabung campaign bla bla bla','image_url':'http://res.cloudinary.com/sticar/image/upload/v1517826275/full_premium_rgkc9s.jpg','content':'','status':0,'created_at':'2018-03-07 05:07:58'}],'message':'Successfully show all status in home','language':'en'}";


        Gson gson = new Gson();
        // new TypeToken<List<Staff>>(){}.getType()
        Response<ArrayList<Notification>> response = gson.fromJson(json,new TypeToken<Response<ArrayList<Notification>>>(){}.getType());
        Log.d(AppConstant.APP_TAG, "-----------------dsadsad-----------------"+response.getData().size());
        view.onSuccessRequestNotif(response.getData());

    }
}
