package sti_car.com.sticar.ui.profilemain;

/**
 * Created by femmy on 14/02/2018.
 */

public class ProfileMainContract {
    interface View {
        void onSuccessUploadProfilePic(String url);

        void onErrorUploadProfilePic(String msg);
    }

    interface Presenter {
        void uploadProfilePic(String imgBlob);
    }
}
