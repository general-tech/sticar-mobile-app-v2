package sti_car.com.sticar.ui.registerconfirm;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Register;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserData;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 23/01/2018.
 */

public class RegisterConfirmPresenter extends BasePresenter<RegisterConfirmContract.View>
    implements RegisterConfirmContract.Presenter{

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public RegisterConfirmPresenter(RegisterConfirmContract.View view) {
        super(view);
    }

    @Override
    public void confirmRegister(Register mRegister) {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().register(mRegister)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<UserAccount>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onRegisterError("Server error. Error Messsage : " + e.getMessage());
                    }

                    @Override
                    public void onNext(UserAccount response) {
                        if(response.getmCode() == 200){
                            response.setSessionAsLoggedIn();
                            mSessionManager.setUserAccount(response);
                            view.onRegisterSuccess(response.getData().getmToken());
                        } else {
                            view.onRegisterError(response.getmMessage());
                        }
                    }
                })
        );
    }
}
