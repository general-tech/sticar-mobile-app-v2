package sti_car.com.sticar.ui.registerconfirm;

import sti_car.com.sticar.data.models.Register;

/**
 * Created by femmy on 23/01/2018.
 */

public class RegisterConfirmContract {

    interface View {
        void onRegisterError(String msg);

        void onRegisterSuccess(String Token);
    }

    interface Presenter {
        void confirmRegister(Register mRegister);
    }
}
