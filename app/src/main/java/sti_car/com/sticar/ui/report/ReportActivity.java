package sti_car.com.sticar.ui.report;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Report.Report;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.ui.report.DaggerReportComponent;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import javax.inject.Inject;

public class ReportActivity extends AppCompatActivity implements
        ReportContract.View
        , View.OnClickListener{

    @Inject
    ReportPresenter presenter;

    ReportComponent actvComponent;

    private CardView btnCalculation,btnHistory,btnRedeem;
    private TextView tvCredit;

    private Double mCredit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        actvComponent = DaggerReportComponent.builder()
                .reportModule(new ReportModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
        presenter.requestReport();
    }

    private void renderView(){
        btnCalculation      = (CardView) findViewById(R.id.containerCalculation);
        btnCalculation.setOnClickListener(this);
        btnHistory          = (CardView) findViewById(R.id.containerHistory);
        btnHistory.setOnClickListener(this);
        btnRedeem           = (CardView) findViewById(R.id.containerRedeem);
        btnRedeem.setOnClickListener(this);
        tvCredit            = (TextView) findViewById(R.id.tvDetailrp);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnCalculation.getId()){
            Intent intent = new Intent(ReportActivity.this,ReportCalculationActivity.class);
            startActivity(intent);
        } else if (v.getId() == btnHistory.getId()) {
//            Intent intent = new Intent(ReportActivity.this,RedeemActivity.class);
//            startActivity(intent);
        } else if (v.getId() == btnRedeem.getId()) {
//            Intent intent = new Intent(ReportActivity.this,HistoryActivity.class);
//            startActivity(intent);
        }
    }

    @Override
    public void successRequestReport(Report mReport) {
        mCredit = mReport.getLifetime().getCredit();
        tvCredit.setText("Rp. " + AppUtil.formatCurrency(mCredit));
    }

    @Override
    public void errorRequestReport(String msg) {
        Toast.makeText(ReportActivity.this, msg, Toast.LENGTH_SHORT).show();
    }
}
