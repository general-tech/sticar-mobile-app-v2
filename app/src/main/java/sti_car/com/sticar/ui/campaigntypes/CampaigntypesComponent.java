package sti_car.com.sticar.ui.campaigntypes;

import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

import dagger.Component;

/**
 * Created by femmy on 09/01/2018.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules={CampaigntypesModule.class})
public interface CampaigntypesComponent {
    void inject(CampaigntypesActivity activity);
}
