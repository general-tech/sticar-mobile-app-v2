package sti_car.com.sticar.ui.tnc;

import sti_car.com.sticar.data.models.Reponses.TncPolicy;

/**
 * Created by femmy on 23/01/2018.
 */

public class TncContract {

    interface View {
        void successRequestTnc(TncPolicy mTncPolicy);

        void errorRequestTnc(String msg);
    }

    interface Presenter {
        void getTnc();
    }
}
