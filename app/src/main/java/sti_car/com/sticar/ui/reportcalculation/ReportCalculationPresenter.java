package sti_car.com.sticar.ui.reportcalculation;

import android.os.CountDownTimer;

import com.google.gson.Gson;

import javax.inject.Inject;

import sti_car.com.sticar.data.models.Reponses.ReportPeriodic;
import sti_car.com.sticar.ui.base.BasePresenter;

/**
 * Created by femmy on 06/02/2018.
 */

public class ReportCalculationPresenter extends BasePresenter<ReportCalculationContract.View>
    implements ReportCalculationContract.Presenter{

    @Inject
    public ReportCalculationPresenter(ReportCalculationContract.View view) {
        super(view);
    }

    @Override
    public void requestReport() {
        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {}
            public void onFinish() {
                String rawJsonDaily = "{'today':{'credit':50000,'distance':40},'chart_daily':[{'day':'wed','date':'2018-02-07','credit':50000,'distance':40},{'day':'tue','date':'2018-02-06','credit':50000,'distance':35},{'day':'mon','date':'2018-02-05','credit':50000,'distance':42},{'day':'sun','date':'2018-02-04','credit':50000,'distance':35},{'day':'sat','date':'2018-02-03','credit':50000,'distance':50},{'day':'fri','date':'2018-02-02','credit':50000,'distance':50},{'day':'thu','date':'2018-02-01','credit':50000,'distance':40}],'chart_monthly':[{'month':'FEBRUARY 18','credit':50000,'distance':50},{'month':'JANUARY 18','credit':50000,'distance':50},{'month':'DECEMBER 17','credit':50000,'distance':50},{'month':'NOVEMBER 17','credit':50000,'distance':50},{'month':'OCTOBER 17','credit':50000,'distance':50},{'month':'SEPTEMBER 17','credit':50000,'distance':50},{'month':'AUGUST 17','credit':50000,'distance':50},{'month':'JULY 17','credit':50000,'distance':50},{'month':'JUNE 17','credit':50000,'distance':50},{'month':'MAY 17','credit':50000,'distance':50},{'month':'APRIL 17','credit':50000,'distance':50},{'month':'MARCH 17','credit':50000,'distance':50}]}";
                Gson gson = new Gson();
                ReportPeriodic report = gson.fromJson(rawJsonDaily, ReportPeriodic.class);
                view.onSuccessRequestReport(report);
            }
        }.start();
    }
}
