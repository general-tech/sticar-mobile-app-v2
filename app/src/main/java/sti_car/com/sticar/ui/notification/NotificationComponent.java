package sti_car.com.sticar.ui.notification;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 10/02/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules=NotificationModule.class)
public interface NotificationComponent {
    void inject(NotificationActivity activity);
}

