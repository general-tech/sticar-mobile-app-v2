package sti_car.com.sticar.ui.registerconfirm;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Register;
import sti_car.com.sticar.ui.registeraccount.RegisteraccountActivity;
import sti_car.com.sticar.ui.registersuccess.RegisterSuccessActivity;
import sti_car.com.sticar.utils.AppConstant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterConfirmActivity extends AppCompatActivity
    implements RegisterConfirmContract.View,
        View.OnClickListener{

    @Inject
    RegisterConfirmPresenter presenter;

    RegisterConfirmComponent actvComponent;

    Register mRegister;

    private TextView tvFullname, tvEmail, tvPhone, tvAccname, tvAccno, tvBankName, tvCarSpecLabel, tvCarSpec, tvMotorSpeclabel, tvMotorSpec;
    private Button btnConfirmSubmit;
    private SweetAlertDialog mDialogLoader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_confirm);

        actvComponent = DaggerRegisterConfirmComponent.builder()
                .registerConfirmModule(new RegisterConfirmModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mRegister = (Register) extras.getSerializable(AppConstant.EXTRA_REGISTER_KEY);
            String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),Settings.Secure.ANDROID_ID);
            mRegister.setDeviceId(android_id);
            setTextViewValue();
        } else {
            Intent i = new Intent(RegisterConfirmActivity.this, RegisteraccountActivity.class);
            startActivity(i);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        tvFullname              = (TextView) findViewById(R.id.tv_confirm_fullname);
        tvEmail                 = (TextView) findViewById(R.id.tv_confirm_email);
        tvPhone                 = (TextView) findViewById(R.id.tv_confirm_phone);
        tvAccname               = (TextView) findViewById(R.id.tv_confirm_accname);
        tvAccno                 = (TextView) findViewById(R.id.tv_confirm_bankname);
        tvBankName              = (TextView) findViewById(R.id.tv_confirm_accno);
        tvCarSpec               = (TextView) findViewById(R.id.tv_confirm_car_value);
        tvCarSpecLabel          = (TextView) findViewById(R.id.tv_confirm_car_label);
        tvMotorSpec             = (TextView) findViewById(R.id.tv_confirm_motor_value);
        tvMotorSpeclabel        = (TextView) findViewById(R.id.tv_confirm_motor_label);

        btnConfirmSubmit = (Button) findViewById(R.id.btn_confirm_register);
        btnConfirmSubmit.setOnClickListener(this);
        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTextViewValue(){
        tvFullname.setText(mRegister.getName());
        tvEmail.setText(mRegister.getEmail());
        tvPhone.setText(mRegister.getPhoneNumber());
        if(mRegister.isBankAccountVerified()){
            tvAccname.setText(mRegister.getBankAccountName());
            tvAccno.setText(mRegister.getBankAccountNumber());
        } else {
            tvAccname.setText(" Not Verified ");
            tvAccno.setText(" - ");
        }

        if(mRegister.getCarBrandName() != null){
            tvCarSpecLabel.setVisibility(View.VISIBLE);
            tvCarSpec.setVisibility(View.VISIBLE);
            tvCarSpec.setText(mRegister.getCarBrandName() +" / "+mRegister.getCarModelName()+" / "+mRegister.getCarColorName());
        } else {
            tvCarSpecLabel.setVisibility(View.GONE);
            tvCarSpec.setVisibility(View.GONE);
        }

        if(mRegister.getMotorcycleBrandName() != null){
            tvMotorSpeclabel.setVisibility(View.VISIBLE);
            tvMotorSpec.setVisibility(View.VISIBLE);
            tvMotorSpec.setText(mRegister.getMotorcycleBrandName() +" / "+mRegister.getMotorcycleModelName()+" / "+mRegister.getMotorcycleColorName());
        } else {
            tvMotorSpeclabel.setVisibility(View.GONE);
            tvMotorSpec.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnConfirmSubmit.getId()){
            mDialogLoader.show();
            btnConfirmSubmit.setText("Registering...");
            presenter.confirmRegister(mRegister);
        }
    }

    @Override
    public void onRegisterError(String msg) {
        mDialogLoader.dismiss();
        btnConfirmSubmit.setText("Register");
        Toast.makeText(RegisterConfirmActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRegisterSuccess(String Token) {
        mDialogLoader.dismiss();
        btnConfirmSubmit.setText("Register");
        Intent i = new Intent(RegisterConfirmActivity.this, RegisterSuccessActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
