package sti_car.com.sticar.ui.report;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.sti_car.rschsticar10.R;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Reponses.ResponseList;
import sti_car.com.sticar.data.models.Report.History;
import sti_car.com.sticar.ui.report.DaggerReportComponent;

import javax.inject.Inject;

public class ReportHistoryActivity extends AppCompatActivity implements ReportContract.HistoryView {

    @Inject
    ReportHistoryPresenter presenter;

    ReportComponent actvComponent;

    private SwipeRefreshLayout swipeContainer;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_history);

        actvComponent = DaggerReportComponent.builder()
                .reportHistoryModule(new ReportHistoryModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    private void renderView() {

    }

    @Override
    public void successRequest(ResponseList<History> response) {

    }

    @Override
    public void errorRequest(String msg) {

    }
}
