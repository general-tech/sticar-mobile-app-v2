package sti_car.com.sticar.ui.report;

import android.content.Context;

import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.Report.Report;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 11/01/2018.
 */

public class ReportCalculationPresenter extends BasePresenter<ReportContract.CalculationView>
        implements ReportContract.CalculationPresenter {

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public ReportCalculationPresenter(ReportContract.CalculationView view) {
        super(view);
    }

    @Override
    public void getCalculationReport() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getReport()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<Response<Report>>(){
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {
                    view.errorRequestReport("Error requesting data report.");
                }

                @Override
                public void onNext(Response<Report> response){
                    if(response.getData() != null ){
                        view.successRequestReport(response.getData());
                    } else {
                        view.errorRequestReport("No data available");
                    }
                }
            })
        );

    }
}
