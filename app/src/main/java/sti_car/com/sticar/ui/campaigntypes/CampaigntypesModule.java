package sti_car.com.sticar.ui.campaigntypes;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 09/01/2018.
 */
@Module
public class CampaigntypesModule {

    private final CampaigntypesContract.View view;

    public CampaigntypesModule(CampaigntypesContract.View v){
        view = v;
    }

    @Provides
    CampaigntypesContract.View provideView(){
        return view;
    }
}
