package sti_car.com.sticar.ui.settingaccount;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 14/02/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={SettingAccountModule.class})
public interface SettingAccountComponent {
    void inject(SettingAccountActivity activity);
}