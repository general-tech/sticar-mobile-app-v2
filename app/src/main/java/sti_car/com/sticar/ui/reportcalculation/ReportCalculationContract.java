package sti_car.com.sticar.ui.reportcalculation;

import sti_car.com.sticar.data.models.Reponses.ReportPeriodic;

/**
 * Created by femmy on 06/02/2018.
 */

public class ReportCalculationContract {

    interface View {
        void onSuccessRequestReport(ReportPeriodic report);

        void onErrorrequestReport(String msg);
    }

    interface Presenter {
        void requestReport();
    }
}
