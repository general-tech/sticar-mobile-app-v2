package sti_car.com.sticar.ui.profilemain;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 14/02/2018.
 */

@Module
public class ProfileMainModule {

    private final ProfileMainContract.View view;

    public ProfileMainModule(ProfileMainContract.View view) {
        this.view = view;
    }

    @Provides
    ProfileMainContract.View provideView(){
        return view;
    }
}

