package sti_car.com.sticar.ui.report;

import android.content.Context;

import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.data.models.Reponses.ResponseList;
import sti_car.com.sticar.data.models.Report.History;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 11/01/2018.
 */

public class ReportHistoryPresenter extends BasePresenter<ReportContract.HistoryView>
        implements ReportContract.HistoryPresenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;


    @Inject
    public ReportHistoryPresenter(ReportContract.HistoryView view) {
        super(view);
    }

    @Override
    public void requestReportHistory() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getReportHistory()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<ResponseList<History>>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                    view.errorRequest("Error request history.");
                }

                @Override
                public void onNext(ResponseList<History> response) {
                    view.successRequest(response);
                }
            })
        );
    }
}
