package sti_car.com.sticar.ui.historycampaign;

import java.util.ArrayList;

import sti_car.com.sticar.data.models.Campaign;

/**
 * Created by femmy on 14/02/2018.
 */

public class HistoryCampaignContract {

    interface View {
        void onSuccessRequestHistoryCampaign(ArrayList<Campaign> campaigns);

        void onErrorRequestHistoryCampaign(String msg);
    }

    interface Presenter {
        void requestHistoryCampaign();
    }

}
