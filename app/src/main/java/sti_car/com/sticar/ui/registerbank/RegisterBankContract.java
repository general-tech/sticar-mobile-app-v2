package sti_car.com.sticar.ui.registerbank;

import java.util.List;

import sti_car.com.sticar.data.models.Bank;
import sti_car.com.sticar.data.models.BankAccount;
import sti_car.com.sticar.data.models.BankBranch;
import sti_car.com.sticar.data.models.Register;
import sti_car.com.sticar.data.models.Reponses.BankAccountResponse;

/**
 * Created by femmy on 14/01/2018.
 */

public class RegisterBankContract {

    interface View {
        void onRequestBankListSuccess(List<Bank> banks);

        void onRequestBankListFailed(String msg);


        void onBankAccountValid(BankAccountResponse bankAccountResponse);

        void onBankAccountInvalid(String msg);

    }

    interface Presenter {
        void requestBankList();

        void checkBankAccountHolder(BankAccount mbankAccount);
    }

}
