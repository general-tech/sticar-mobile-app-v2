package sti_car.com.sticar.ui.campaigns;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sti_car.rschsticar10.R;

import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.CampaignItem;
import sti_car.com.sticar.ui.campaigndetail.CampaigndetailActivity;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by femmy on 09/01/2018.
 */

public class CampaignsAdapter extends RecyclerView.Adapter{

    private List<Campaign> campaigns;

    private Context context;

    public CampaignsAdapter(Context mContext, List<Campaign> mCampaigns) {
        context     = mContext;
        campaigns   = mCampaigns;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_campaign, parent, false);
        return new CampaignViewHolder(itemView);
    }

    public void setData(List<Campaign> MCampaigns){
        campaigns = MCampaigns;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Campaign mCampaign = campaigns.get(position);
        CampaignViewHolder mholder = (CampaignViewHolder) holder;
        mholder.tvAdvertiser.setText(AppUtil.strLimit(mCampaign.getName(),40));
        mholder.tvCampaignArea.setText(mCampaign.getArea() +" / "+mCampaign.getWrappingType());
        Glide.with(context).load(mCampaign.getImageUrl())
                .placeholder(R.drawable.default_shortlogo).dontAnimate().into(mholder.imgHolder);
        if(mCampaign.getStatus() == Campaign.CAMPAIGN_STATUS_ACTIVE){
            mholder.tvIndicator.setText("Available");
            mholder.tvIndicator.setTextColor(context.getResources().getColor(R.color.green_dark));
            mholder.vLine.setBackgroundColor(context.getResources().getColor(R.color.green_dark));
        } else {
            mholder.tvIndicator.setText("Not Available");
            mholder.tvIndicator.setTextColor(context.getResources().getColor(R.color.red));
            mholder.vLine.setBackgroundColor(context.getResources().getColor(R.color.red));
        }

        mholder.btnDetailCampaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CampaigndetailActivity.class);
                intent.putExtra(AppConstant.EXTRA_ADS_KEY, mCampaign.getUuid());
                context.startActivity(intent);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return campaigns.size();
    }

    public class CampaignViewHolder extends RecyclerView.ViewHolder {

        public TextView tvAdvertiser,tvIndicator,tvDetailCampaign, tvCampaignArea;
        public CardView cardView;
        public ImageView imgHolder;
        public View vLine;
        public Button btnDetailCampaign;

        public CampaignViewHolder(View itemView) {
            super(itemView);
            vLine = (View) itemView.findViewById(R.id.viewLine);
            tvAdvertiser = (TextView) itemView.findViewById(R.id.tvAdvertiser);
            tvIndicator = (TextView) itemView.findViewById(R.id.tvNotificationStatus);
            tvDetailCampaign = (TextView) itemView.findViewById(R.id.tvDetailCampaign);
            tvCampaignArea = (TextView) itemView.findViewById(R.id.tvCampaignArea);

            cardView = (CardView) itemView.findViewById(R.id.cardViewCampaign);
            imgHolder = (ImageView) itemView.findViewById(R.id.imgCampaignAdv);
            btnDetailCampaign = (Button) itemView.findViewById(R.id.btn_detail_campaign);
        }
    }
}
