package sti_car.com.sticar.ui.registeraccount;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 13/01/2018.
 */
@Module
public class RegisteraccountModule {
    private final RegisteraccountContract.View view;

    public RegisteraccountModule(RegisteraccountContract.View v){
        view = v;
    }

    @Provides
    RegisteraccountContract.View provideView(){
        return view;
    }
}
