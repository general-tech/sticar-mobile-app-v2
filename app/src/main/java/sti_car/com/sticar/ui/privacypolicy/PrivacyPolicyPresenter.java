package sti_car.com.sticar.ui.privacypolicy;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Reponses.TncPolicy;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;

/**
 * Created by femmy on 23/01/2018.
 */

public class PrivacyPolicyPresenter extends BasePresenter<PrivacyPolicyContract.View>
        implements PrivacyPolicyContract.Presenter {

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public PrivacyPolicyPresenter(PrivacyPolicyContract.View view) {
        super(view);
    }

    @Override
    public void getPolicy() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getPrivacyPolicy()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<TncPolicy>() {
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {
                    view.errorRequestPolicy("Error request privacy Policy, Try again later.");
                }

                @Override
                public void onNext(TncPolicy tncPolicy) {
                    view.successRequestPolicy(tncPolicy);
                }
            })
        );
    }
}
