package sti_car.com.sticar.ui.settingaccount;

import android.content.Context;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserData;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppHelper;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 14/02/2018.
 */

public class SettingAccountPresenter extends BasePresenter<SettingAccountContract.View>
        implements SettingAccountContract.Presenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public SettingAccountPresenter(SettingAccountContract.View view) {
        super(view);
    }

    @Override
    public void saveAccount(UserProfile profile) {

        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().editAccount(profile)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<Response<UserProfile>>() {
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {
                    view.onErrorSaveAccount(e.getMessage());
                }

                @Override
                public void onNext(Response<UserProfile> response) {
                    if (response.getCode() == 200) {
                        UserAccount mUserAccount = mSessionManager.getUserAccount();
                        mUserAccount.setProfile(response.getData());
                        mSessionManager.setUserAccount(mUserAccount);
                        view.onSuccessSaveAccount(response.getMessage());
                    } else {
                        view.onErrorSaveAccount(response.getMessage());
                    }
                }
            })
        );
    }
}
