package sti_car.com.sticar.ui.report;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 11/01/2018.
 */
@Module
public class ReportHistoryModule {

    private final ReportContract.HistoryView view;

    public ReportHistoryModule(ReportContract.HistoryView v){
        view = v;
    }

    @Provides
    ReportContract.HistoryView provideCalculationView(){
        return view;
    }
}
