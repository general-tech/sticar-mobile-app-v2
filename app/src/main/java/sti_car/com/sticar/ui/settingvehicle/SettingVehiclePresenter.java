package sti_car.com.sticar.ui.settingvehicle;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.CarBrand;
import sti_car.com.sticar.data.models.CarModel;
import sti_car.com.sticar.data.models.MotorBrand;
import sti_car.com.sticar.data.models.MotorModel;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.data.models.VehicleColor;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 14/02/2018.
 */

public class SettingVehiclePresenter extends BasePresenter<SettingVehicleContract.View>
    implements SettingVehicleContract.Presenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;


    @Inject
    public SettingVehiclePresenter(SettingVehicleContract.View view) {
        super(view);
    }

    @Override
    public void requestCarBrands() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getCarBrands()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<ArrayList<CarBrand>>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorRequestCarBrands("Error request car brands");
                    }

                    @Override
                    public void onNext(Response<ArrayList<CarBrand>> response) {
                        if(response.getCode() == 200){
                            view.onSuccessRequestCarBrands(response.getData());
                        } else {
                            view.onErrorRequestCarBrands(response.getMessage());
                        }
                    }
                })
        );
    }

    @Override
    public void requestMotorBrands() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getMotorBrands()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<ArrayList<MotorBrand>>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorRequestCarBrands("Error request car brands");
                    }

                    @Override
                    public void onNext(Response<ArrayList<MotorBrand>> response) {
                        if(response.getCode() == 200){
                            view.onSuccessRequestMotorBrands(response.getData());
                        } else {
                            view.onErrorRequestMotorBrands(response.getMessage());
                        }
                    }
                })
        );
    }

    @Override
    public void requestCarModel(int id) {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getCarModels(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<ArrayList<CarModel>>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorRequestCarModels(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<ArrayList<CarModel>> response) {
                        if(response.getCode() == 200){
                            view.onSuccessRequestCarModels(response.getData());
                        } else {
                            view.onErrorRequestCarModels(response.getMessage());
                        }
                    }
                })
        );
    }

    @Override
    public void requestMotorModel(int id) {
        Log.d(AppConstant.APP_TAG, "requestMotorModel : "+id);
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getMotorModels(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<ArrayList<MotorModel>>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorRequestMotorModels(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<ArrayList<MotorModel>> response) {
                        if(response.getCode() == 200){
                            view.onSuccessRequestMotorModels(response.getData());
                        } else {
                            view.onErrorRequestMotorModels(response.getMessage());
                        }
                    }
                })
        );
    }

    @Override
    public void requestVehicleColor() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getVehicleColors()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<ArrayList<VehicleColor>>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorRequestVehicleColors(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<ArrayList<VehicleColor>> response) {
                        if(response.getCode() == 200){
                            view.onSuccessRequestVehicleColors(response.getData());
                        } else {
                            view.onErrorRequestVehicleColors(response.getMessage());
                        }
                    }
                })
        );
    }

    @Override
    public void saveDataVehicle(UserProfile profile) {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().editDataVehicle(profile)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<UserProfile>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorEditVehicle(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<UserProfile> response) {
                        if(response.getCode() == 200){
                            UserAccount mUserAccount = mSessionManager.getUserAccount();
                            mUserAccount.setProfile(response.getData());
                            mSessionManager.setUserAccount(mUserAccount);
                            view.onSuccessEdiVehicle();
                        } else {
                            view.onErrorEditVehicle(response.getMessage());
                        }
                    }
                })
        );
    }
}
