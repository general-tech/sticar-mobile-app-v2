package sti_car.com.sticar.ui.registersuccess;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.CampaignType;
import sti_car.com.sticar.ui.campaigntypes.CampaigntypesActivity;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterSuccessActivity extends AppCompatActivity
    implements RegisterSuccessContract.View,
        View.OnClickListener{

    @Inject
    RegisterSuccessPresenter presenter;

    RegisterSuccessComponent actvComponent;

    private Button btnJoinCampaign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_success);

        actvComponent = DaggerRegisterSuccessComponent.builder()
                .registerSuccessModule(new RegisterSuccessModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);

        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        btnJoinCampaign = findViewById(R.id.btn_join_campaign);
        btnJoinCampaign.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnJoinCampaign.getId()){
            Intent i = new Intent(RegisterSuccessActivity.this, CampaigntypesActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        }
    }
}
