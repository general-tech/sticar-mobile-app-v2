package sti_car.com.sticar.ui.settingaccount;

import sti_car.com.sticar.data.models.UserProfile;

/**
 * Created by femmy on 14/02/2018.
 */

public class SettingAccountContract {

    interface View {
        void onSuccessSaveAccount(String msg);

        void onErrorSaveAccount(String msg);
    }

    interface Presenter {
        void saveAccount(UserProfile profile);
    }
}
