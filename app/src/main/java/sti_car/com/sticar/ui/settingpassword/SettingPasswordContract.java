package sti_car.com.sticar.ui.settingpassword;

import sti_car.com.sticar.data.models.UserProfile;

/**
 * Created by femmy on 14/02/2018.
 */

public class SettingPasswordContract {

    interface View {
        void onSuccessSavePassword(String msg);

        void onErrorSavePassword(String msg);
    }

    interface Presenter {
        void savePassword(UserProfile profile);
    }
}
