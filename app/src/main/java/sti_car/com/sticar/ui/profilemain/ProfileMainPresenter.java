package sti_car.com.sticar.ui.profilemain;

import android.content.Context;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Bank;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserData;
import sti_car.com.sticar.data.models.UserProfileImage;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 14/02/2018.
 */

public class ProfileMainPresenter extends BasePresenter<ProfileMainContract.View>
        implements ProfileMainContract.Presenter {

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public ProfileMainPresenter(ProfileMainContract.View view) {
        super(view);
    }

    @Override
    public void uploadProfilePic(String imgBlob) {
        UserProfileImage mProfileImage = new UserProfileImage();
        mProfileImage.setImgBlob(imgBlob);
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().uploadImageProfile(mProfileImage)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<String>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorUploadProfilePic(e.getMessage());
                    }

                    @Override
                    public void onNext(Response<String> response) {
                        if(response.getCode() == 200){
                            UserAccount mUserAccount = mSessionManager.getUserAccount();
                            UserData mData = mUserAccount.getData();
                            Driver mDriver = mData.getDriver();
                            mDriver.setPhoto_url(response.getData());
                            mData.setDriver(mDriver);
                            mUserAccount.setData(mData);
                            mSessionManager.setUserAccount(mUserAccount);
                            view.onSuccessUploadProfilePic(response.getMessage());

                        } else {
                            view.onErrorUploadProfilePic("Error upload profile image");
                        }
                    }
                })
        );
    }
}
