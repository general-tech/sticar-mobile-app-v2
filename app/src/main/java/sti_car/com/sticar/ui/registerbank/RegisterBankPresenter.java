package sti_car.com.sticar.ui.registerbank;

import android.content.Context;

import java.util.ArrayList;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Bank;
import sti_car.com.sticar.data.models.BankAccount;
import sti_car.com.sticar.data.models.BankBranch;
import sti_car.com.sticar.data.models.Register;
import sti_car.com.sticar.data.models.Reponses.BankAccountResponse;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by femmy on 14/01/2018.
 */

public class RegisterBankPresenter extends BasePresenter<RegisterBankContract.View>
        implements RegisterBankContract.Presenter {

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public RegisterBankPresenter(RegisterBankContract.View view) {
        super(view);
    }

    @Override
    public void requestBankList() {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getListBank()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<ArrayList<Bank>>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onRequestBankListFailed("Error request bank list");
                    }

                    @Override
                    public void onNext(Response<ArrayList<Bank>> response) {
                        if(response.getCode() == 200){
                            view.onRequestBankListSuccess(response.getData());
                        } else {
                            view.onRequestBankListFailed("Error request bank list");
                        }
                    }
                })
        );
    }


    @Override
    public void checkBankAccountHolder(BankAccount mbankAccount) {
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().checkBankAccount(mbankAccount)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<BankAccountResponse>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onBankAccountInvalid("Error checking bank account, try again later.");
                    }

                    @Override
                    public void onNext(Response<BankAccountResponse> response) {
                        if(response.getCode() == 200){
                            view.onBankAccountValid(response.getData());
                        } else {
                            view.onBankAccountInvalid(response.getMessage());
                        }
                    }
                })
        );
    }
}
