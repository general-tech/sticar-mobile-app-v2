package sti_car.com.sticar.ui.settingpassword;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Register;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.ui.settingbank.SettingBankContract;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 14/02/2018.
 */

public class SettingPasswordPresenter extends BasePresenter<SettingPasswordContract.View>
        implements SettingPasswordContract.Presenter {

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public SettingPasswordPresenter(SettingPasswordContract.View view) {
        super(view);
    }

    @Override
    public void savePassword(UserProfile profile) {

        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().editPassword(profile)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<Response<Boolean>>() {
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {
                    view.onErrorSavePassword(e.getMessage());
                }

                @Override
                public void onNext(Response<Boolean> response) {
                    if (response.getCode() == 200) {
                        if(response.getData()){
                            view.onSuccessSavePassword(response.getMessage());
                        } else {
                            view.onErrorSavePassword(response.getMessage());
                        }

                    } else {
                        view.onErrorSavePassword(response.getMessage());
                    }
                }
            })
        );
    }
}
