package sti_car.com.sticar.ui.base;

/**
 * Created by femmy on 08/12/2017.
 */

public class BasePresenter<T> {

    protected T view;

//    @Named("AppContext")
//    @Inject
//    Context context;
//
//    @Inject
//    protected SessionManager mSessionManger;
//
//    @Inject
//    protected NetworkUtil mNetworkUtil;

    public BasePresenter(T view){
        this.view = view;
    }

//    protected boolean isConnectedToInternet(){
//        ConnectivityManager conn = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
//        if (conn.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
//                conn.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
//                conn.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
//                conn.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
//            return true;
//        } else if (
//                conn.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
//                        conn.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
//            return false;
//        } else {
//            return false;
//        }
//    }
//
//    protected boolean isLoggedIn(){
//        UserAccount mUserAcc = mSessionManger.getUserAccount();
//        if (mUserAcc != null){
//            if (mUserAcc.getData().getSessionType().equalsIgnoreCase(AppConstant.SESSION_TYPE_DRIVER)) {
//                AppConstant.IS_DRIVER = true;
//                return true;
//            }
//        }
//        return false;
//    }

}
