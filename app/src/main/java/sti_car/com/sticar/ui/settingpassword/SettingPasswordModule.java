package sti_car.com.sticar.ui.settingpassword;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 14/02/2018.
 */

@Module
public class SettingPasswordModule {

    private final SettingPasswordContract.View view;

    public SettingPasswordModule(SettingPasswordContract.View v){
        view = v;
    }

    @Provides
    SettingPasswordContract.View provideView(){
        return view;
    }
}
