package sti_car.com.sticar.ui.main;

import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

import dagger.Component;

/**
 * Created by femmy on 12/12/2017.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={MainModule.class})
public interface MainComponent {
    void inject(MainActivity activity);
}
