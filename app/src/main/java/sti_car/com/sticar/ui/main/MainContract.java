package sti_car.com.sticar.ui.main;

import android.location.Location;

import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.Report.Periodic;

/**
 * Created by femmy on 12/12/2017.
 */

public class MainContract {

    public interface View {
        void userInitialized(UserAccount mUser);

        void updateReport(Response<Periodic> mReportPeriodic);

        void onTotalTodayUpdated(double totalDistance, double totalCredit);

        void onTripStarted();

        void startingTripFailed(String msg);

        void onTripStopped();

        void onUserAccountLoggedOut();

        void onSuccessIntiLocalUserAccountData(UserAccount mUserAccount);

        void onErrorIntiLocalUserAccountData();

        void onSuccessInitHomeData(UserAccount mUserAccount);

        void onErrorInitHomeData(String msg);

    }



    public interface Presenter {

        void checkAndPostUnpostedTrip();

        void startTrip();

        void logout();

        void initLocalUserAccountData();

        void initHomeData();

    }

    public interface LocView {
        void onNoNetworkProvider();

        void onLocationListenerSet(Location l);
    }

    public interface LocPresenter {
        void setLocationListener();

        void unsetLocationListener();
    }

}
