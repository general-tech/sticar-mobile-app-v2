package sti_car.com.sticar.ui.splash;

import sti_car.com.sticar.data.models.UserAccount;

/**
 * Created by femmy on 08/12/2017.
 */

public class SplashContract {

    interface View {
        void throwToLogin();

        void gotoMain();

        void onDataUpdated(UserAccount userAccount);

        void onErrorProcess(String msg);

        void showToastMsg(String msg);

        void onUnavailableGPS();
    }

    interface Presenter {
        void init();
    }
}
