package sti_car.com.sticar.ui.campaigndetail;

import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.CampaignDetail;

/**
 * Created by femmy on 09/01/2018.
 */

public class CampaigndetailContract {

    interface View {
        void onCampaignInitialized(Campaign campaign);

        void errorIniatializedCampaign();

        void errorJoinCampaign(String msg);

        void successJoinCampaign();

    }

    interface Presenter {
        void initCampaign(String idWrap);

        void joinCampaign(String wrappingUUID, String campaignUUID);
    }
}
