package sti_car.com.sticar.ui.login;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 11/12/2017.
 */
@Module
public class LoginModule {

    private final LoginContract.View view;

    public LoginModule(LoginContract.View view) {
        this.view = view;
    }

    @Provides
    LoginContract.View provideView(){
        return view;
    }
}
