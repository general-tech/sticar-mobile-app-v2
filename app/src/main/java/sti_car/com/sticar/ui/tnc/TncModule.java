package sti_car.com.sticar.ui.tnc;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 23/01/2018.
 */

@Module
public class TncModule {

    private final TncContract.View view;

    public TncModule(TncContract.View v){
        view = v;
    }

    @Provides
    TncContract.View provideView(){
        return view;
    }
}
