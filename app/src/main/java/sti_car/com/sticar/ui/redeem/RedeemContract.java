package sti_car.com.sticar.ui.redeem;

import sti_car.com.sticar.data.models.Transaction;

/**
 * Created by femmy on 06/02/2018.
 */

public class RedeemContract {

    interface View {
        void onRequestRedeemSuccess();

        void onRequestRedeemError(String msg);

        void onCheckCurrentRedeemSuccess(Transaction transaction);

        void onCheckCurrentRedeemError(String msg);
    }

    interface Presenter {
        void checkCurrentRedeem();

        void requestRedeem(Transaction transaction);
    }
}
