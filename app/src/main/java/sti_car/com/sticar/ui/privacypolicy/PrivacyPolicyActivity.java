package sti_car.com.sticar.ui.privacypolicy;

import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.Reponses.TncPolicy;
import sti_car.com.sticar.ui.feedback.DaggerFeedbackComponent;
import sti_car.com.sticar.ui.feedback.FeedbackModule;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PrivacyPolicyActivity extends AppCompatActivity implements PrivacyPolicyContract.View {

    @Inject
    PrivacyPolicyPresenter presenter;

    PrivacyPolicyComponent actvComponent;

    private TextView tvContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);

        actvComponent = DaggerPrivacyPolicyComponent.builder()
                .privacyPolicyModule(new PrivacyPolicyModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(PrivacyPolicyActivity.this);
        setupWindowAnimations();
        renderView();
        presenter.getPolicy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void renderView() {
        tvContent = (TextView) findViewById(R.id.tv_content_policy);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupWindowAnimations() {
        Slide fade = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            fade = (Slide) TransitionInflater.from(this).inflateTransition(R.transition.activity_slide);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(fade);
        }

    }

    @Override
    public void successRequestPolicy(TncPolicy mtncPolicy) {
        tvContent.setText(Html.fromHtml(mtncPolicy.getData()));
    }

    @Override
    public void errorRequestPolicy(String msg) {
        Toast.makeText(PrivacyPolicyActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
