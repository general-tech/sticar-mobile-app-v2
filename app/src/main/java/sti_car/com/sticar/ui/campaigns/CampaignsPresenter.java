package sti_car.com.sticar.ui.campaigns;

import android.content.Context;
import android.util.Log;

import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.CampaignItem;
import sti_car.com.sticar.data.models.Reponses.CampaignListV2;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.SessionManager;
import sti_car.com.sticar.utils.NetworkUtil;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 09/01/2018.
 */

public class CampaignsPresenter extends BasePresenter<CampaignsContract.View> implements CampaignsContract.Presenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public CampaignsPresenter(CampaignsContract.View view) {
        super(view);
    }

    @Override
    public void loadCampaignList(int type) {
        Log.d(AppConstant.APP_TAG, "type : "+type);
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getListCampaign(type)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<Response<ArrayList<Campaign>>>() {
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {
                    view.campaignListErrorLoaded();
                }

                @Override
                public void onNext(Response<ArrayList<Campaign>> campaignListModel) {
                    if (campaignListModel.getData() != null) {
                        view.campaignListLoaded(campaignListModel.getData());
                    } else {
                        view.campaignListErrorLoaded();
                    }
                }
            })
        );
    }

    @Override
    public void initUser() {
        view.onUserInitialized(mSessionManager.getUserAccount());
    }
}
