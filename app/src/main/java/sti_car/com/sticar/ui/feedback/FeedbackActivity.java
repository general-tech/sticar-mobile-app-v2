package sti_car.com.sticar.ui.feedback;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.ui.privacypolicy.PrivacyPolicyActivity;
import sti_car.com.sticar.ui.tnc.TncActivity;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FeedbackActivity extends AppCompatActivity implements
         FeedbackContract.View
        ,View.OnClickListener
        ,View.OnTouchListener{

    @Inject
    FeedbackPresenter presenter;

    FeedbackComponent actvComponent;

    private TextView tvPolicy, tvTos;

    private Button btnSubmit;

    private EditText etSubject, etContent;

    private SweetAlertDialog mDialogLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        actvComponent = DaggerFeedbackComponent.builder()
                .feedbackModule(new FeedbackModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    private void renderView(){
        etSubject       =  (EditText) findViewById(R.id.et_feedback_subject);
        etSubject.setOnTouchListener(this);
        etContent       =  (EditText) findViewById(R.id.et_feedback_content);
        etContent.setOnTouchListener(this);
        btnSubmit       =  (Button) findViewById(R.id.btn_feedback_submit);
        btnSubmit.setOnClickListener(this);
        tvPolicy        = (TextView) findViewById(R.id.tv_feedback_policy);
        tvPolicy.setOnClickListener(this);
        tvTos           = (TextView) findViewById(R.id.tv_feedback_tos);
        tvTos.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnSubmit.getId()){
            String subject = etSubject.getText().toString();
            String content = etContent.getText().toString();
            mDialogLoader.show();
            presenter.submitFeedback(subject,content);
            btnSubmit.setText("Submitting...");
        } else if(v.getId() == tvPolicy.getId()){
            Intent i = new Intent(FeedbackActivity.this, PrivacyPolicyActivity.class);
            startActivity(i);
        } else if(v.getId() == tvTos.getId()){
            Intent i = new Intent(FeedbackActivity.this, TncActivity.class);
            startActivity(i);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void successSubmitFeeback() {
        etContent.getText().clear();
        etSubject.getText().clear();
        btnSubmit.setText("Submit");
        mDialogLoader.dismiss();
    }

    @Override
    public void errorSubmitSubject(String msg) {
        mDialogLoader.dismiss();
        btnSubmit.setText("Submit");
        Toast.makeText(FeedbackActivity.this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        v.setFocusable(true);
        v.setFocusableInTouchMode(true);
        return false;
    }
}
