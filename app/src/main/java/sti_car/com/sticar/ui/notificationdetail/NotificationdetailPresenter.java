package sti_car.com.sticar.ui.notificationdetail;

import sti_car.com.sticar.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by femmy on 10/01/2018.
 */

public class NotificationdetailPresenter extends BasePresenter<NotificationdetailContract.View>
        implements NotificationdetailContract.Presenter {

    @Inject
    public NotificationdetailPresenter(NotificationdetailContract.View view) {
        super(view);
    }
}
