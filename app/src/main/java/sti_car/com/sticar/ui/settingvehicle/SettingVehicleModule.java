package sti_car.com.sticar.ui.settingvehicle;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 14/02/2018.
 */

@Module
public class SettingVehicleModule {

    private final SettingVehicleContract.View view;

    public SettingVehicleModule(SettingVehicleContract.View v){
        view = v;
    }

    @Provides
    SettingVehicleContract.View provideView(){
        return view;
    }
}
