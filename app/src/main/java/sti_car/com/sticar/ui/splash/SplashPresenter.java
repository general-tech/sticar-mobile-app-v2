package sti_car.com.sticar.ui.splash;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;

import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppHelper;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.utils.SessionManager;
import sti_car.com.sticar.utils.NetworkUtil;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 08/12/2017.
 */

public class SplashPresenter extends BasePresenter<SplashContract.View>
        implements SplashContract.Presenter {

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Named("AppHelper")
    @Inject
    AppHelper mAppHelper;

    private UserAccount mUserAccount;


    @Inject
    public SplashPresenter(SplashContract.View view) {
        super(view);
    }

    @Override
    public void init() {
        mUserAccount = mSessionManager.getUserAccount();
        if(AppUtil.isConnectedToInternet(context)){
            new CountDownTimer(2000, 1000) {
                public void onTick(long m) {}
                public void onFinish() {
                    if(mSessionManager.isLoggedIn()){
                        view.gotoMain();
                    } else {
                        view.throwToLogin();
                    }
                }
            }.start();
        } else {
            view.onErrorProcess(AppConstant.ERROR_MSG_INTENET_NOT_CONNECTED);
            view.throwToLogin();
        }
    }


    private boolean isLoggedIn(){
        UserAccount mUserAcc = mSessionManager.getUserAccount();
        if (mUserAcc != null){
            if (mUserAcc.isLoggedIn()) {
                AppConstant.IS_DRIVER = true;
                return true;
            }
        }
        return false;
    }

    private void updateServerUserAccData(){
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().getUserAccountData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<UserAccount>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        //String code = ((HttpException) e).code();
                        Log.d(AppConstant.APP_TAG, " Code : "+((HttpException) e).code());
                        view.onErrorProcess(e.getMessage());
                    }

                    @Override
                    public void onNext(UserAccount userAcco) {
                        if(userAcco.getData() != null){
                            mSessionManager.setUserAccount(userAcco);
                            view.onDataUpdated(userAcco);
                            view.gotoMain();
                        } else {
                            view.throwToLogin();
                        }
                    }
                })
        );
    }
}
