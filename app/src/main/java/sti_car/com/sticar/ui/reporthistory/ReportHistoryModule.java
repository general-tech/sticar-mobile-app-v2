package sti_car.com.sticar.ui.reporthistory;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 06/02/2018.
 */

@Module
public class ReportHistoryModule {
    private final ReportHistoryContract.View view;

    public ReportHistoryModule(ReportHistoryContract.View v){
        view = v;
    }

    @Provides
    ReportHistoryContract.View provideView(){
        return view;
    }
}

