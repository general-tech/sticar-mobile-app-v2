package sti_car.com.sticar.ui.reporthistory;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.sti_car.rschsticar10.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import javax.inject.Inject;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.TransactionRedeem;
import sti_car.com.sticar.utils.AppConstant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReportHistoryActivity extends AppCompatActivity implements
    ReportHistoryContract.View,
    SwipeRefreshLayout.OnRefreshListener{

    @Inject
    ReportHistoryPresenter presenter;

    ReportHistoryComponent actvComponent;

    private ReportHistoryAdapter mRepHisAdapter;
    private ArrayList<TransactionRedeem> data = new ArrayList<TransactionRedeem>();
    private SwipeRefreshLayout swipeContainer;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private AVLoadingIndicatorView avLoadingIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_history);

        actvComponent = DaggerReportHistoryComponent.builder()
                .reportHistoryModule(new ReportHistoryModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void renderView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        swipeContainer                  = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeContainer.setColorSchemeResources(R.color.green_light);
        swipeContainer.setOnRefreshListener(this);
        avLoadingIndicator              = (AVLoadingIndicatorView) findViewById(R.id.avi_loader);

        mLayoutManager                  =  new LinearLayoutManager(this);
        mRecyclerView                   = (RecyclerView) findViewById(R.id.rv_history);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRepHisAdapter                  = new ReportHistoryAdapter(data,this);
        mRecyclerView.setAdapter(mRepHisAdapter);
        mRepHisAdapter.notifyDataSetChanged();
        presenter.requestRedeemHistory();
    }



    @Override
    public void onSuccessRequestHistory(ArrayList<TransactionRedeem> e) {
        data = e;
        Log.d(AppConstant.APP_TAG, "data size : "+data.size());
        avLoadingIndicator.hide();
        mRepHisAdapter.setData(data);
        mRepHisAdapter.notifyDataSetChanged();
    }


    @Override
    public void onFailedReuqestHistory(String msg) {
        avLoadingIndicator.hide();
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        avLoadingIndicator.show();
        presenter.requestRedeemHistory();
    }
}
