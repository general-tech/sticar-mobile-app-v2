package sti_car.com.sticar.ui.settingpassword;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 14/02/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={SettingPasswordModule.class})
public interface SettingPasswordComponent {
    void inject(SettingPasswordActivity activity);
}

