package sti_car.com.sticar.ui.settingvehicle;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.here.android.mpa.urbanmobility.Line;
import com.sti_car.rschsticar10.R;

import java.util.ArrayList;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;
import sti_car.com.sticar.data.models.CarBrand;
import sti_car.com.sticar.data.models.CarModel;
import sti_car.com.sticar.data.models.MotorBrand;
import sti_car.com.sticar.data.models.MotorModel;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.data.models.VehicleColor;
import sti_car.com.sticar.ui.registervehicle.RegisterVehicleActivity;
import sti_car.com.sticar.utils.SessionManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingVehicleActivity extends AppCompatActivity implements
        SettingVehicleContract.View
        ,AdapterView.OnItemSelectedListener
        ,View.OnClickListener{

    @Inject
    SettingVehiclePresenter presenter;

    SettingVehicleComponent actvComponent;

    private UserProfile mProfile;

    private LinearLayout llWrapCar, llWrapMotor;

    private Spinner spCarBrand, spCarModel, spCarColor, spMotorBrand, spMotorModel, spMotorColor;

    private Button btnSaveVehicle;

    private ArrayList<CarBrand> carBrands               = new ArrayList<CarBrand>();

    private ArrayList<MotorBrand> motorBrands           = new ArrayList<MotorBrand>();

    private ArrayList<CarModel> carModels               = new ArrayList<CarModel>();

    private ArrayList<MotorModel> motorModels           = new ArrayList<MotorModel>();

    private ArrayList<VehicleColor> vehicleColors       = new ArrayList<VehicleColor>();

    private SweetAlertDialog mDialogLoader, mDialogLoaderCarModel, mDialogLoaderMotorModel;

    private int carBrandId, carModelId, carColorId, motorBrandId, motorModelId, motorColorId;

    private String carBrandName, carModelName, carColorName, motorBrandName, motorModelName, motorColorName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_vehicle);

        actvComponent = DaggerSettingVehicleComponent.builder()
                .settingVehicleModule(new SettingVehicleModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);
        renderView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void renderView(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        llWrapCar   = (LinearLayout) findViewById(R.id.ll_vehicle_car);
        llWrapMotor = (LinearLayout) findViewById(R.id.ll_vehicle_motor);

        spCarBrand      = (Spinner) findViewById(R.id.sp_car_brand);
        spCarModel      = (Spinner) findViewById(R.id.sp_car_model);
        spCarColor      = (Spinner) findViewById(R.id.sp_car_color);

        spMotorBrand    = (Spinner) findViewById(R.id.sp_motor_brand);
        spMotorModel    = (Spinner) findViewById(R.id.sp_motor_model);
        spMotorColor    = (Spinner) findViewById(R.id.sp_motor_color);

        btnSaveVehicle = (Button) findViewById(R.id.btn_setting_save_vehicle);
        btnSaveVehicle.setOnClickListener(this);

        mProfile = new SessionManager(this).getUserAccount().getUserProfile();
        if(mProfile.hasCar()){
            llWrapCar.setVisibility(View.VISIBLE);
            presenter.requestCarBrands();
            presenter.requestCarModel(mProfile.getCarBrandId());
        } else {
            llWrapCar.setVisibility(View.GONE);
        }

        if(mProfile.hasMotorCycle()){
            llWrapMotor.setVisibility(View.VISIBLE);
            presenter.requestMotorBrands();
            presenter.requestMotorModel(mProfile.getMotorcycleBrandId());
        } else {
            llWrapMotor.setVisibility(View.GONE);
        }
        presenter.requestVehicleColor();
        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
        mDialogLoader.setCancelable(false);
        mDialogLoaderCarModel       = mDialogLoader;
        mDialogLoaderMotorModel     = mDialogLoader;
        mDialogLoader.show();
    }

    @Override
    public void onSuccessRequestCarBrands(ArrayList<CarBrand> brands) {
        carBrands = brands;
        ArrayList<String> carBrandNames = new ArrayList<String>();
        for (int i = 0; i < brands.size(); i++){
            CarBrand mCarBrand = brands.get(i);
            carBrandNames.add(mCarBrand.getName());
        }

        ArrayAdapter<String> carBrandAdapter = new ArrayAdapter<String>(SettingVehicleActivity.this,
                R.layout.spinner_layout_custom_green, carBrandNames);
        carBrandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCarBrand.setAdapter(carBrandAdapter);
        spCarBrand.setSelection(carBrandAdapter.getPosition(mProfile.getCarBrandName()));
        presenter.requestCarModel(mProfile.getCarBrandId());
    }

    @Override
    public void onErrorRequestCarBrands(String msg) {
        Toast.makeText(SettingVehicleActivity.this, msg, Toast.LENGTH_SHORT);
    }

    @Override
    public void onSuccessRequestMotorBrands(ArrayList<MotorBrand> brands) {
        motorBrands = brands;
        ArrayList<String> motorBrandNames = new ArrayList<String>();
        for (int i = 0; i < brands.size(); i++){
            MotorBrand mMotorBrand = brands.get(i);
            motorBrandNames.add(mMotorBrand.getName());
        }

        ArrayAdapter<String> motorandAdapter = new ArrayAdapter<String>(SettingVehicleActivity.this,
                R.layout.spinner_layout_custom_green, motorBrandNames);
        motorandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMotorBrand.setAdapter(motorandAdapter);
        spMotorBrand.setSelection(motorandAdapter.getPosition(mProfile.getMotorcycleBrandName()));


    }

    @Override
    public void onErrorRequestMotorBrands(String msg) {
        Toast.makeText(SettingVehicleActivity.this, msg, Toast.LENGTH_SHORT);
    }

    @Override
    public void onSuccessRequestCarModels(ArrayList<CarModel> models) {
        carModels = models;
        ArrayList<String> carModelNames = new ArrayList<String>();
        for (int i = 0; i < models.size(); i++){
            CarModel mCarModel = models.get(i);
            carModelNames.add(mCarModel.getName());
        }

        ArrayAdapter<String> carModelAdapter = new ArrayAdapter<String>(SettingVehicleActivity.this,
                R.layout.spinner_layout_custom_green, carModelNames);
        carModelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCarModel.setAdapter(carModelAdapter);
        spCarModel.setSelection(carModelAdapter.getPosition(mProfile.getCarModelName()));
        mDialogLoaderCarModel.dismiss();
    }

    @Override
    public void onErrorRequestCarModels(String msg) {
        mDialogLoaderCarModel.dismiss();
        Toast.makeText(SettingVehicleActivity.this, msg, Toast.LENGTH_SHORT);
    }

    @Override
    public void onSuccessRequestMotorModels(ArrayList<MotorModel> models) {
        motorModels = models;
        ArrayList<String> motortModelNames = new ArrayList<String>();
        for (int i = 0; i < models.size(); i++){
            MotorModel mMotorModel = models.get(i);
            motortModelNames.add(mMotorModel.getName());
        }

        ArrayAdapter<String> motorModelAdapter = new ArrayAdapter<String>(SettingVehicleActivity.this,
                R.layout.spinner_layout_custom_green, motortModelNames);
        motorModelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMotorModel.setAdapter(motorModelAdapter);
        spMotorModel.setSelection(motorModelAdapter.getPosition(mProfile.getMotorcycleModelName()));
        mDialogLoaderMotorModel.dismiss();
    }

    @Override
    public void onErrorRequestMotorModels(String msg) {
        mDialogLoaderMotorModel.dismiss();
        Toast.makeText(SettingVehicleActivity.this, msg, Toast.LENGTH_SHORT);
    }

    @Override
    public void onSuccessRequestVehicleColors(ArrayList<VehicleColor> colors) {
        vehicleColors = colors;
        ArrayList<String> vehicleColors = new ArrayList<String>();
        for (int i = 0; i < colors.size(); i++){
            VehicleColor vehicleColor = colors.get(i);
            vehicleColors.add(vehicleColor.getName());
        }

        ArrayAdapter<String> vehicleColorAdapter = new ArrayAdapter<String>(SettingVehicleActivity.this,
                R.layout.spinner_layout_custom_green, vehicleColors);
        vehicleColorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCarColor.setAdapter(vehicleColorAdapter);
        spCarColor.setSelection(vehicleColorAdapter.getPosition(mProfile.getCarColorName()));
        spMotorColor.setAdapter(vehicleColorAdapter);
        spMotorColor.setSelection(vehicleColorAdapter.getPosition(mProfile.getMotorcycleColorName()));
    }

    @Override
    public void onErrorRequestVehicleColors(String msg) {
        Toast.makeText(SettingVehicleActivity.this, msg, Toast.LENGTH_SHORT);
        mDialogLoader.dismiss();
    }

    @Override
    public void onSuccessEdiVehicle() {
        mDialogLoader.dismiss();
        new SessionManager(this).getUserAccount().setProfile(mProfile);
    }

    @Override
    public void onErrorEditVehicle(String msg) {
        mDialogLoader.dismiss();
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnSaveVehicle.getId()){
            mDialogLoader.show();
            presenter.saveDataVehicle(mProfile);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(parent.getId() == spCarBrand.getId()){
            CarBrand mCarBrand = carBrands.get(position);
            mProfile.setCarBrandId(mCarBrand.getId());
            mProfile.setCarBrandName(mCarBrand.getName());
            presenter.requestCarModel(mCarBrand.getId());
            mDialogLoaderCarModel.show();
        } else if(parent.getId() == spCarModel.getId()){
            CarModel mCarModel = carModels.get(position);
            mProfile.setCarBrandId(mCarModel.getId());
            mProfile.setCarBrandName(mCarModel.getName());
        } else if(parent.getId() == spCarColor.getId()){
            VehicleColor vehicleColor = vehicleColors.get(position);
            mProfile.setCarColorId(vehicleColor.getId());
            mProfile.setCarColorName(vehicleColor.getName());
        } else if(parent.getId() == spMotorBrand.getId()){
            MotorBrand mMotorBrand = motorBrands.get(position);
            mProfile.setMotorcycleBrandId(mMotorBrand.getId());
            mProfile.setMotorcycleBrandName(mMotorBrand.getName());
            presenter.requestMotorModel(mMotorBrand.getId());
            mDialogLoaderMotorModel.show();
        } else if(parent.getId() == spMotorModel.getId()){
            MotorModel mMotorModel = motorModels.get(position);
            mProfile.setMotorcycleModelId(mMotorModel.getId());
            mProfile.setMotorcycleModelName(mMotorModel.getName());
            motorModelName    = mMotorModel.getName();
        } else if(parent.getId() == spMotorColor.getId()){
            VehicleColor vehicleColor = vehicleColors.get(position);
            mProfile.setMotorcycleColorId(vehicleColor.getId());
            mProfile.setMotorcycleColorName(vehicleColor.getName());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
