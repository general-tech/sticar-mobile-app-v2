package sti_car.com.sticar.ui.registersuccess;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 25/01/2018.
 */

@Module
public class RegisterSuccessModule {
    private final RegisterSuccessContract.View view;

    public RegisterSuccessModule(RegisterSuccessContract.View v){
        view = v;
    }

    @Provides
    RegisterSuccessContract.View provideView(){
        return view;
    }
}
