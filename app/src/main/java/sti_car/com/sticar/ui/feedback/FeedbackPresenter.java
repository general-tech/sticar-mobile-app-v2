package sti_car.com.sticar.ui.feedback;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import sti_car.com.sticar.data.models.Feedback;
import sti_car.com.sticar.data.models.Reponses.FeedbackResponse;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

/**
 * Created by femmy on 22/01/2018.
 */

public class FeedbackPresenter extends BasePresenter<FeedbackContract.View> implements FeedbackContract.Presenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Inject
    public FeedbackPresenter(FeedbackContract.View view) {
        super(view);
    }

    @Override
    public void submitFeedback(String subject, String content) {
        if(!subject.equalsIgnoreCase("") && !content.equalsIgnoreCase("")){
            Feedback mFeedback = new Feedback();
            mFeedback.setSubject(subject);
            mFeedback.setNote(content);
            retrofitSubscription.add(mNetworkUtil.getRetrofitV2().feedbackDriver(mFeedback)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<Response<String>>() {
                        @Override
                        public void onCompleted() {}

                        @Override
                        public void onError(Throwable e) {
                            view.errorSubmitSubject("Error submit feedback, try again later.");
                        }

                        @Override
                        public void onNext(Response<String> response) {
                            if(response.getCode() == 200){
                                view.successSubmitFeeback();
                            } else {
                                view.errorSubmitSubject("Error submit feedback, try again later.");
                            }
                        }
                    })
            );
        } else {
            view.errorSubmitSubject("Please fill all the fields.");
        }
    }
}
