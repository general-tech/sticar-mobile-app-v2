package sti_car.com.sticar.ui.reportcalculation;

import dagger.Component;
import sti_car.com.sticar.AppComponent;
import sti_car.com.sticar.utils.annot.PerActivity;

/**
 * Created by femmy on 06/02/2018.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules={ReportCalculationModule.class})
public interface ReportCalculationComponent {
    void inject(ReportCalculationActivity activity);
}
