package sti_car.com.sticar.ui.reportdashboard;

import sti_car.com.sticar.data.models.Report.Lifetime;

/**
 * Created by femmy on 06/02/2018.
 */

public class ReportDashboardContract {

    interface View {
        void onSuccessRequestReport(Lifetime report);

        void onErrorRequestReport(String msg);
    }

    interface Presenter {
        void requestReportTotal();
    }
}
