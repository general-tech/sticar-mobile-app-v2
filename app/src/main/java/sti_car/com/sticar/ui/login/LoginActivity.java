package sti_car.com.sticar.ui.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sti_car.com.sticar.App;

import sti_car.com.sticar.ui.forgetpassword.ForgetPasswordActivity;
import sti_car.com.sticar.ui.login.DaggerLoginComponent;
import sti_car.com.sticar.ui.main.MainActivity;
import sti_car.com.sticar.ui.registeraccount.RegisteraccountActivity;
import sti_car.com.sticar.utils.AppConstant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import com.sti_car.rschsticar10.R;
import com.vistrav.ask.Ask;

import javax.inject.Inject;

public class LoginActivity extends AppCompatActivity implements
        LoginContract.View, View.OnClickListener {

    @Inject
    LoginPresenter presenter;

    LoginComponent actvComponent;
    Button btnLogin;
    TextView tvLinkSignup,tvLinkForgotPassword;
    EditText etPassword,etEmail;
    private SweetAlertDialog mDialogLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        setContentView(R.layout.activity_login);

        adjustLayoutBasedOnOSVersion();

        actvComponent = DaggerLoginComponent.builder()
                .loginModule(new LoginModule(this))
                .appComponent(App.get(this).getComponent()).build();
        actvComponent.inject(this);

        initView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void adjustLayoutBasedOnOSVersion(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    private void initView(){
        tvLinkSignup            = (TextView) findViewById(R.id.tv_register);
        tvLinkSignup.setText(Html.fromHtml("Belum memiliki akun? <b>Daftar</b>"));
        tvLinkSignup.setOnClickListener(this);
        btnLogin                = (Button) findViewById(R.id.btn_login);
        tvLinkForgotPassword    = (TextView) findViewById(R.id.tv_forget_password);
        tvLinkForgotPassword.setOnClickListener(this);
        etEmail                 = (EditText) findViewById(R.id.et_email);
        etPassword              = (EditText) findViewById(R.id.et_password);
        btnLogin.setOnClickListener(this);

        mDialogLoader = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mDialogLoader.getProgressHelper().setBarColor(getResources().getColor(R.color.v2_green));
        mDialogLoader.setTitleText("Loading");
    }

    @Override
    public void onLoginFailed(String msg) {
        mDialogLoader.dismiss();
        Log.e(AppConstant.APP_TAG, msg);
        Toast.makeText(this, msg,Toast.LENGTH_SHORT).show();
        btnLogin.setText("Login");
    }

    @Override
    public void onLoginSuccess() {
        btnLogin.setText("Login");
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();

    }

    @Override
    public void onPermissionDenied() {
        Ask.on(this).forPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE).go();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnLogin.getId()){
            String email    = etEmail.getText().toString();
            String pass     = etPassword.getText().toString();
            btnLogin.setText("Please wait...");
            mDialogLoader.show();
            presenter.doLogin(email,pass);
        } else if(v.getId() == tvLinkSignup.getId()){
            Intent i = new Intent(LoginActivity.this, RegisteraccountActivity.class);
            startActivity(i);
            finish();
        } else if(v.getId() == tvLinkForgotPassword.getId()){
            Intent i = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
            startActivity(i);
            finish();
        }
    }
}
