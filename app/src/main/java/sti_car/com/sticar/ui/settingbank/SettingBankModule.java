package sti_car.com.sticar.ui.settingbank;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 14/02/2018.
 */
@Module
public class SettingBankModule {

    private final SettingBankContract.View view;

    public SettingBankModule(SettingBankContract.View v){
        view = v;
    }

    @Provides
    SettingBankContract.View provideView(){
        return view;
    }
}
