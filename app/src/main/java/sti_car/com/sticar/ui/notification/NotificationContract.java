package sti_car.com.sticar.ui.notification;

import java.util.ArrayList;

import sti_car.com.sticar.data.models.Notification;

/**
 * Created by femmy on 10/02/2018.
 */

public class NotificationContract  {

    interface View {
        void onSuccessRequestNotif(ArrayList<Notification> notifs);

        void onErrorRequestNotif(String msg);
    }

    interface Presenter {
        void requestNotif();
    }
}
