package sti_car.com.sticar.ui.registeraccount;

import android.content.Context;
import android.util.Log;

import sti_car.com.sticar.data.models.Email;
import sti_car.com.sticar.data.models.Reponses.CheckEmail;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.ui.base.BasePresenter;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppHelper;
import sti_car.com.sticar.utils.SessionManager;
import sti_car.com.sticar.utils.NetworkUtil;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 13/01/2018.
 */

public class RegisteraccountPresenter extends BasePresenter<RegisteraccountContract.View>
    implements RegisteraccountContract.Presenter{

    @Named("AppContext")
    @Inject
    Context context;

    @Inject
    SessionManager mSessionManager;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper SPHelper;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription retrofitSubscription;

    @Named("AppHelper")
    @Inject
    AppHelper mAppHelper;

    @Inject
    public RegisteraccountPresenter(RegisteraccountContract.View view) {
        super(view);
    }

    @Override
    public void checkEmail(String email) {
        Email mEmail = new Email();
        mEmail.setEmail(email);
        retrofitSubscription.add(mNetworkUtil.getRetrofitV2().checkEmail(mEmail)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Response<CheckEmail>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        view.onEmailInvalid("Error request, Try again later.");
                    }

                    @Override
                    public void onNext(Response<CheckEmail> response) {
                        if(response.getCode() == 200){
                            view.onEmailValid();
                        } else {
                            view.onEmailInvalid(response.getMessage());
                        }
                    }
                })
        );
    }
}
