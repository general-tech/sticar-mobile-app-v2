package sti_car.com.sticar.ui.registerbank;

import dagger.Module;
import dagger.Provides;

/**
 * Created by femmy on 14/01/2018.
 */
@Module
public class RegisterBankModule {
    private final RegisterBankContract.View view;

    public RegisterBankModule(RegisterBankContract.View v){
        view = v;
    }

    @Provides
    RegisterBankContract.View provideView(){
        return view;
    }
}
