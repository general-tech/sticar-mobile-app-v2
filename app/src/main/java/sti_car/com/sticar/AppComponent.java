package sti_car.com.sticar;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import sti_car.com.sticar.data.source.realm.RealmHelper;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.utils.AppHelper;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import io.realm.Realm;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 08/12/2017.
 */
@SuppressWarnings("unused")
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(App app);

    void inject(AppCompatActivity activity);

    @Named("AppContext")
    Context getContext();

    @Named("RetrofitSubscription")
    CompositeSubscription getCompositeSubscription();

    @Named("SharedPrefHelper")
    SharedPrefHelper getSharedPrefHelper();

    @Named("AppHelper")
    AppHelper getAppHelper();

    @Named("Realm")
    Realm getRealm();

    @Named("RealmHelper")
    RealmHelper getRealmHelper();

}
