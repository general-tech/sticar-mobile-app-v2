package sti_car.com.sticar.data.models;

/**
 * Created by femmy on 14/12/2017.
 */

public class Track {

    private Double lat;
    private Double lng;
    private Double lastTotalDistance;
    private int lastTotalCredit;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLastTotalDistance() {
        return lastTotalDistance;
    }

    public void setLastTotalDistance(Double lastTotalDistance) {
        this.lastTotalDistance = lastTotalDistance;
    }

    public int getLastTotalCredit() {
        return lastTotalCredit;
    }

    public void setLastTotalCredit(int lastTotalCredit) {
        this.lastTotalCredit = lastTotalCredit;
    }
}
