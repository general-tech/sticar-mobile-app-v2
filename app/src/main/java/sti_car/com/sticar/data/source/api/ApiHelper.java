package sti_car.com.sticar.data.source.api;

import sti_car.com.sticar.utils.AppConstant;

/**
 * Created by femmy on 11/12/2017.
 */

public class ApiHelper {

    public static String getUrlV2() {
        if (AppConstant.APP_IS_PRODUCTION) {
            return ApiConstant.URL_STAGING+ ApiConstant.V2;
        }
        return ApiConstant.URL_STAGING+ ApiConstant.V2;
        //return ApiConstant.URL_LOCAL+ ApiConstant.V2;
    }
}
