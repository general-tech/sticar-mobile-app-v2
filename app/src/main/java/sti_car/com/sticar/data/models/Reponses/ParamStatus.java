package sti_car.com.sticar.data.models.Reponses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 19/12/2017.
 */

public class ParamStatus {

    @SerializedName("contractId")
    String contractId;
    @SerializedName("contractUUID")
    String contractUUID;
    @SerializedName("benefitToday")
    String benefitToday;
    @SerializedName("distanceToday")
    String distanceToday;
    @SerializedName("dailyKm")
    String dailyKm;
    @SerializedName("status")
    String status;


    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractUUID(String contractUUID) {
        this.contractUUID = contractUUID;
    }

    public String getContractUUID() {
        return contractUUID;
    }

    public void setBenefitToday(String benefitToday) {
        this.benefitToday = benefitToday;
    }

    public String getBenefitToday() {
        return benefitToday;
    }

    public void setDistanceToday(String distanceToday) {
        this.distanceToday = distanceToday;
    }

    public String getDistanceToday() {
        return distanceToday;
    }


    public void setDailyKm(String dailyKm) {
        this.dailyKm = dailyKm;
    }

    public String getDailyKm() {
        return dailyKm;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
