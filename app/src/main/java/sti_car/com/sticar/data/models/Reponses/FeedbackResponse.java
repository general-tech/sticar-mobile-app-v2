package sti_car.com.sticar.data.models.Reponses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 22/01/2018.
 */

public class FeedbackResponse {

    @SerializedName("code")
    private Long code;

    @SerializedName("data")
    private Object data;

    @SerializedName("language")
    private String language;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Boolean status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
