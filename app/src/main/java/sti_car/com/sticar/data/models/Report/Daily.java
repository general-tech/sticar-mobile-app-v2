package sti_car.com.sticar.data.models.Report;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by femmy on 17/12/2017.
 */

public class Daily implements Serializable{

    @SerializedName("date")
    private String date;

    @SerializedName("day")
    private String day;

    @SerializedName("credit")
    private Double credit;

    @SerializedName("distance")
    private Double distance;

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double mcredit) {
        credit = mcredit;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double mdistance) {
        distance = mdistance;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
