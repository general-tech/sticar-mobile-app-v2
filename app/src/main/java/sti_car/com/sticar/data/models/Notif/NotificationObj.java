package sti_car.com.sticar.data.models.Notif;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 10/01/2018.
 */

public class NotificationObj {

    @SerializedName("code")
    private Long code;

    @SerializedName("data")
    private NotificationData data;

    @SerializedName("language")
    private String language;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Boolean status;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NotificationData getData() {
        return data;
    }

    public void setData(NotificationData data) {
        this.data = data;
    }
}
