package sti_car.com.sticar.data.source.api;

import java.util.ArrayList;

import sti_car.com.sticar.data.models.Bank;
import sti_car.com.sticar.data.models.BankAccount;
import sti_car.com.sticar.data.models.BankBranch;
import sti_car.com.sticar.data.models.Campaign;
import sti_car.com.sticar.data.models.CarBrand;
import sti_car.com.sticar.data.models.CarModel;
import sti_car.com.sticar.data.models.ContractUUIDBody;
import sti_car.com.sticar.data.models.Driver;
import sti_car.com.sticar.data.models.Email;
import sti_car.com.sticar.data.models.Feedback;
import sti_car.com.sticar.data.models.JoinAdsParam;
import sti_car.com.sticar.data.models.MotorBrand;
import sti_car.com.sticar.data.models.MotorModel;
import sti_car.com.sticar.data.models.Notification;
import sti_car.com.sticar.data.models.Register;
import sti_car.com.sticar.data.models.Reponses.BankAccountResponse;
import sti_car.com.sticar.data.models.Reponses.CheckEmail;
import sti_car.com.sticar.data.models.Reponses.FeedbackResponse;
import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.Reponses.StartTrip;
import sti_car.com.sticar.data.models.Reponses.TncPolicy;
import sti_car.com.sticar.data.models.Reponses.TrackFeed;
import sti_car.com.sticar.data.models.Report.History;
import sti_car.com.sticar.data.models.Report.Report;
import sti_car.com.sticar.data.models.SendDataTracker;
import sti_car.com.sticar.data.models.TodayTrip;
import sti_car.com.sticar.data.models.Transaction;
import sti_car.com.sticar.data.models.TripUUIDBody;
import sti_car.com.sticar.data.models.User;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.Reponses.EndTrip;
import sti_car.com.sticar.data.models.Reponses.ResponseList;
import sti_car.com.sticar.data.models.Report.Periodic;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;
import sti_car.com.sticar.data.models.UserProfile;
import sti_car.com.sticar.data.models.UserProfileImage;
import sti_car.com.sticar.data.models.VehicleColor;

/**
 * Created by femmy on 11/12/2017.
 */

public interface RestApi {

    @Headers("Content-Type: application/json")
    @POST("auth/get-session")
    Observable<UserAccount> getUserAccountData();

    // AUTH
    @Headers("Content-Type: application/json")
    @POST("auth/login")
    Observable<UserAccount> login(@Body User user);

//    @Headers("Content-Type: application/json")
//    @POST("driver/trip/start")
//    Observable<StartTrip> startTrip(@Body ContractUUIDBody contractUUIDBody);

    @Headers("Content-Type: application/json")
    @GET("driver/trip/start")
    Observable<Response<TodayTrip>> startTrip();

    //New Trcker
    @Headers("Content-Type: application/json")
    @POST("driver/trip/tracker-feed")
    Observable<TrackFeed> sendTrackerFeed (@Body SendDataTracker sendDataTracker);

    @Headers("Content-Type: application/json")
    @POST("driver/trip/stop")
    Observable<EndTrip> stopTrip(@Body TripUUIDBody tripUUIDBody);

    @Headers("Content-Type: application/json")
    @GET("driver/report")
    Observable<Response<Periodic>> getReportData();

//    @Headers("Content-Type: application/json")
//    @GET("driver/home")
//    Observable<ResponseAllStatus> getStatus();

    @Headers("Content-Type: application/json")
    @GET("driver/home")
    Observable<UserAccount> getHomeData();

//    @Headers("Content-Type: application/json")
//    @GET("driver/campaign")
//    Observable<Response<CampaignListV2>> getListCampaign(@Query("type") int type, @Query("limit") int limit);

    @Headers("Content-Type: application/json")
    @GET("driver/campaigns")
    Observable<Response<ArrayList<Campaign>>> getListCampaign(@Query("type") int type);

//    @Headers("Content-Type: application/json")
//    @GET("driver/campaign/review/{uuid}")
//    Observable<Response<CampaignDetail>> getDetailCampaign(@Path("uuid") String uuid);

    @Headers("Content-Type: application/json")
    @GET("driver/campaigns/detail")
    Observable<Response<Campaign>> getDetailCampaign(@Query("uuid") String uuid);

    @Headers("Content-Type: application/json")
    @POST("driver/campaigns/join")
    Observable<Response<Campaign>> joinCampaign(@Body JoinAdsParam joinAds);

//    @Headers("Content-Type: application/json")
//    @GET("driver/campaigns/join")
//    Observable<Response<Campaign>> joinCampaign(@Query("wrapping_uuid") String wrappingUUID, @Query("campaign_uuid") String campaignUUID);

//    @Headers("Content-Type: application/json")
//    @GET("driver/notification")
//    Observable<Notification.NotificationObj> getNotifications();

    @Headers("Content-Type: application/json")
    @GET("driver/notification")
    Observable<Response<ArrayList<Notification>>> getNotifications();

    @Headers("Content-Type: application/json")
    @GET("driver/report")
    Observable<Response<Report>> getReport();

//    @Headers("Content-Type: application/json")
//    @GET("driver/report")
//    Observable<Response<Report>> getReport();


    @Headers("Content-Type: application/json")
    @GET("driver/transaction")
    Observable<ResponseList<History>> getReportHistory();

    @Headers("Content-Type: application/json")
    @POST("auth/check-email")
    Observable<Response<CheckEmail>> checkEmail(@Body Email email);

    @Headers("Content-Type: application/json")
    @GET("public/forgot-password")
    Observable<Response<Boolean>> submitForgetPass(@Query("email") String email);

    @Headers("Content-Type: application/json")
    @GET("public/banks")
    Observable<Response<ArrayList<Bank>>> getListBank();

    @Headers("Content-Type: application/json")
    @GET("public/domicile-banks")
    Observable<Response<ArrayList<BankBranch>>> getListBankBranch();

    @Headers("Content-Type: application/json")
    @POST("driver/feedback/create")
    Observable<Response<String>> feedbackDriver(@Body Feedback feedback);

    @Headers("Content-Type: application/json")
    @GET("public/terms-and-conditions")
    Observable<TncPolicy> getTNC();

    @Headers("Content-Type: application/json")
    @GET("public/privacy-policy")
    Observable<TncPolicy> getPrivacyPolicy();

    @Headers("Content-Type: application/json")
    @POST("public/bank-account-inquiry")
    Observable<Response<BankAccountResponse>> checkBankAccount(@Body BankAccount bankAccount);

    @Headers("Content-Type: application/json")
    @POST("auth/register")
    Observable<UserAccount> register(@Body Register register);

    @Headers("Content-Type: application/json")
    @GET("public/car-brands")
    Observable<Response<ArrayList<CarBrand>>> getCarBrands();

    @Headers("Content-Type: application/json")
    @GET("public/car-models")
    Observable<Response<ArrayList<CarModel>>> getCarModels(@Query("brand_id") int brandID);

    @Headers("Content-Type: application/json")
    @GET("public/motorcycle-brands")
    Observable<Response<ArrayList<MotorBrand>>> getMotorBrands();

    @Headers("Content-Type: application/json")
    @GET("public/motorcycle-models")
    Observable<Response<ArrayList<MotorModel>>> getMotorModels(@Query("brand_id") int brandID);

    @Headers("Content-Type: application/json")
    @GET("public/vehicle-colors")
    Observable<Response<ArrayList<VehicleColor>>> getVehicleColors();

    @Headers("Content-Type: application/json")
    @POST("driver/profile-img")
    Observable<Response<String>> uploadImageProfile(@Body UserProfileImage profileImage);

    @Headers("Content-Type: application/json")
    @POST("driver/edit-account")
    Observable<Response<UserProfile>> editAccount(@Body UserProfile profile);

    @Headers("Content-Type: application/json")
    @POST("driver/edit-password")
    Observable<Response<Boolean>> editPassword(@Body UserProfile profile);

    @Headers("Content-Type: application/json")
    @POST("driver/edit-bank")
    Observable<Response<UserProfile>> editDataBank(@Body UserProfile profile);

    @Headers("Content-Type: application/json")
    @POST("driver/edit-vehicle")
    Observable<Response<UserProfile>> editDataVehicle(@Body UserProfile profile);

    @Headers("Content-Type: application/json")
    @GET("driver/notification")
    Observable<Response<ArrayList<Campaign>>> getCampaignHistory();

    @Headers("Content-Type: application/json")
    @POST("driver/redeem")
    Observable<Response<Transaction>> requestRedeem(Transaction transaction);

    @Headers("Content-Type: application/json")
    @POST("driver/check-redeem")
    Observable<Response<Transaction>> checkCurrentRedeem();

}
