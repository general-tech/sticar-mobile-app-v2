package sti_car.com.sticar.data.models.Reponses;

import java.util.List;

/**
 * Created by femmy on 11/01/2018.
 */

public class ResponseList<T> extends Response<List<T>> {

    public ResponseList(boolean status, int code, String language, String message, List<T> data) {
        super(status,code,language,message, data);
    }
}
