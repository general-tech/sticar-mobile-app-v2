package sti_car.com.sticar.data.models.Report;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 11/01/2018.
 */

public class History {

    @SerializedName("contract")
    private Object contract;

    @SerializedName("contractId")
    private Object contractId;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("driverId")
    private Double driverId;

    @SerializedName("imgUrl")
    private String imgUrl;

    @SerializedName("status")
    private String status;

    @SerializedName("total")
    private String total;

    @SerializedName("transactionId")
    private Double transactionId;

    @SerializedName("transactionTypeId")
    private Double transactionTypeId;

    @SerializedName("type")
    private HistoryType type;

    @SerializedName("updatedAt")
    private String updatedAt;

    public History() {
    }

    public Object getContract() {
        return contract;
    }

    public void setContract(Object contract) {
        this.contract = contract;
    }

    public Object getContractId() {
        return contractId;
    }

    public void setContractId(Object contractId) {
        this.contractId = contractId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Double getDriverId() {
        return driverId;
    }

    public void setDriverId(Double driverId) {
        this.driverId = driverId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Double getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Double transactionId) {
        this.transactionId = transactionId;
    }

    public Double getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(Double transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public HistoryType getType() {
        return type;
    }

    public void setType(HistoryType type) {
        this.type = type;
    }
}
