package sti_car.com.sticar.data.models;

/**
 * Created by femmy on 27/01/2018.
 */

public class Email {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
