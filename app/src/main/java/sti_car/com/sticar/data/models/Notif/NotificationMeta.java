package sti_car.com.sticar.data.models.Notif;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 10/01/2018.
 */

public class NotificationMeta {

    @SerializedName("currentPage")
    private Long currentPage;
    @SerializedName("limit")
    private Long limit;
    @SerializedName("totalPage")
    private Long totalPage;
    @SerializedName("totalRow")
    private Long totalRow;

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public Long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(Long totalRow) {
        this.totalRow = totalRow;
    }
}
