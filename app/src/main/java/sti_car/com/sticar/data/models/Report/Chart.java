package sti_car.com.sticar.data.models.Report;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by femmy on 17/12/2017.
 */

public class Chart implements Serializable {

    @SerializedName("date")
    private String date;
    @SerializedName("day")
    private String day;
    @SerializedName("distance")
    private Double distance;
    @SerializedName("month")
    private String month;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
