package sti_car.com.sticar.data.models.Reponses;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

/**
 * Created by femmy on 17/12/2017.
 */

public class Response<T> {
    private JsonPrimitive status;
    private JsonElement code;
    private JsonElement language;
    private JsonElement message;

    private T data;

    public Response(){

    }

    public Response(boolean status, int code, String language, String message, T data) {
        this.status = new JsonPrimitive(status);
        this.code = new JsonPrimitive(code);
        this.language = new JsonPrimitive(language);
        this.message = new JsonPrimitive(message);
        this.data = data;
    }

    public static <R> R read(Response<R> response)  {
        return response.getData();
    }

    public boolean isError() {
        return !status.isBoolean() || !status.getAsBoolean();
    }

    public T getData() {
        return data;
    }

    public String getMessage() {
        if (message.isJsonPrimitive())
            return message.getAsJsonPrimitive().getAsString();
        else
            return message.toString();
    }

    public String getLanguage() {
        if (language.isJsonPrimitive())
            return language.getAsJsonPrimitive().getAsString();
        else
            return language.toString();
    }

    public int getCode() {
        if (code.isJsonPrimitive())
            return code.getAsJsonPrimitive().getAsInt();
        else
            return code.getAsInt();
    }
}
