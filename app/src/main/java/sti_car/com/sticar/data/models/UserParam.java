package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 11/12/2017.
 */

public class UserParam {

    @SerializedName("tripId")
    private String contractId;

    @SerializedName("tripUUID")
    private String contractUUID;

    @SerializedName("credit")
    private String credit;

    @SerializedName("benefitToday")
    private String benefitToday;

    @SerializedName("distanceToday")
    private String distanceToday;

    @SerializedName("dailyKm")
    private String dailyKM;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("longitude")
    private String longitude;

    @SerializedName("radiusKm")
    private String radiusKm;

    @SerializedName("areaDescription")
    private String areaDescription;

    @SerializedName("status")
    private String status;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractUUID() {
        return contractUUID;
    }

    public void setContractUUID(String contractUUID) {
        this.contractUUID = contractUUID;
    }

    public String getBenefitToday() {
        return benefitToday;
    }

    public void setBenefitToday(String benefitToday) {
        this.benefitToday = benefitToday;
    }

    public String getDistanceToday() {
        return distanceToday;
    }

    public void setDistanceToday(String distanceToday) {
        this.distanceToday = distanceToday;
    }

    public String getDailyKM() {
        return dailyKM;
    }

    public void setDailyKM(String dailyKM) {
        this.dailyKM = dailyKM;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getRadiusKm() {
        return radiusKm;
    }

    public void setRadiusKm(String radiusKm) {
        this.radiusKm = radiusKm;
    }

    public String getAreaDescription() {
        return areaDescription;
    }

    public void setAreaDescription(String areaDescription) {
        this.areaDescription = areaDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }
}
