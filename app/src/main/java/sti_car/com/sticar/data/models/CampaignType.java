package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 29/01/2018.
 */

public class CampaignType {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    public static final int CAMPAIGN_TYPE_CAR   = 1;

    public static final int CAMPAIGN_TYPE_MOTOR = 2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
