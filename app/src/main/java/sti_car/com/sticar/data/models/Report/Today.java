package sti_car.com.sticar.data.models.Report;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by femmy on 17/12/2017.
 */

public class Today implements Serializable {

    @SerializedName("credit")
    private Double credit;

    @SerializedName("distance")
    private Double distance;

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
