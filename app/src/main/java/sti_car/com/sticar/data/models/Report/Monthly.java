package sti_car.com.sticar.data.models.Report;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by femmy on 17/12/2017.
 */

public class Monthly implements Serializable{

    @SerializedName("month")
    private String month;

    @SerializedName("credit")
    private Double credit;

    @SerializedName("distance")
    private Double distance;

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double mCredit) {
        credit = mCredit;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double mDistance) {
        distance = mDistance;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
