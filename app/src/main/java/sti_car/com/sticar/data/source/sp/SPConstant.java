package sti_car.com.sticar.data.source.sp;

/**
 * Created by femmy on 11/12/2017.
 */

public class SPConstant {

    public static final String PREF_KEY_ACCESS_TOKEN = "access-token";

    public static final String SP_EMPTY_STRING = "";

    public static final String SP_USER_ACCOUNT = "user_account";

    public static final String SP_TOKEN = "token";

    public static final String SP_TRACKER_SERVICE = "TrackerService";

    public static final String SP_KEY_LAST_LATITUDE = "app_last_lat";

    public static final String SP_KEY_LAST_LONGITUDE = "app_last_lng";

    public static final String SP_KEY_TRIP_TODAY = "trip_today";

    public static final String SP_KEY_TRIP_ARRAY = "TripArray";

    public static final String SP_KEY_UNPOSTED_TRIP_TRIPUUID = "unposted_trip_tripuuid";

    public static final String SP_KEY_UNPOSTED_TRIP_ARRAYTRACK = "unposted_trip_arraytrack";
}
