package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 11/12/2017.
 */

public class Wrapping {

    @SerializedName("wrappingId")
    private String wrappingId;

    @SerializedName("wrappingUUID")
    private String wrappingUUID;

    @SerializedName("typeId")
    private String typeId;

    @SerializedName("currency")
    private String currency;

    @SerializedName("credit")
    private String credit;

    @SerializedName("imageUrl")
    private String imageUrl;

    @SerializedName("campaignId")
    private String campaignId;

    @SerializedName("status")
    private String status;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("updatedAt")
    private String updatedAt;

    public String getWrappingId() {
        return wrappingId;
    }

    public void setWrappingId(String wrappingId) {
        this.wrappingId = wrappingId;
    }

    public String getWrappingUUID() {
        return wrappingUUID;
    }

    public void setWrappingUUID(String wrappingUUID) {
        this.wrappingUUID = wrappingUUID;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
