package sti_car.com.sticar.data.models.Reponses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import sti_car.com.sticar.data.models.Report.Daily;
import sti_car.com.sticar.data.models.Report.Monthly;
import sti_car.com.sticar.data.models.Report.Today;

/**
 * Created by femmy on 07/02/2018.
 */

public class ReportPeriodic {

    @SerializedName("today")
    private Today today;

    @SerializedName("chart_daily")
    private ArrayList<Daily> chartDaily;

    @SerializedName("chart_monthly")
    private ArrayList<Monthly> chartMonthly;

    public Today getToday() {
        return today;
    }

    public void setToday(Today today) {
        this.today = today;
    }

    public ArrayList<Daily> getChartDaily() {
        return chartDaily;
    }

    public void setChartDaily(ArrayList<Daily> chartDaily) {
        this.chartDaily = chartDaily;
    }

    public ArrayList<Monthly> getChartMonthly() {
        return chartMonthly;
    }

    public void setChartMonthly(ArrayList<Monthly> chartMonthly) {
        this.chartMonthly = chartMonthly;
    }
}
