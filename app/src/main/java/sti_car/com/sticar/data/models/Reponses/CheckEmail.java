package sti_car.com.sticar.data.models.Reponses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 13/01/2018.
 */

public class CheckEmail {

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Boolean status;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
