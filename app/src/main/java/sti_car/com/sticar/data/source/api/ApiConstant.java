package sti_car.com.sticar.data.source.api;

/**
 * Created by femmy on 11/12/2017.
 */

public class ApiConstant {


//    public static final String URL_STAGING = "http://sandboxapi.sti-car.com/";

    public static final String URL_STAGING = "http://13.229.211.90:8080/";

    public static final  String URL_STAGING_2 = "http://stagingapi.sti-car.com/";

    public static final  String URL_LOCAL = "http://localapi.sti-car.com/";

    public static final String URL_HERE = "https://route.cit.api.here.com/";

    public static final String V1 = "1.0/";

    public static final String V2 = "2.0/";

}
