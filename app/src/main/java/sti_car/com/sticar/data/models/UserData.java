package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 11/12/2017.
 */

public class UserData {

    @SerializedName("token")
    private String token;

    @SerializedName("driver")
    private Driver driver;

    @SerializedName("campaign")
    private Campaign campaign;

    @SerializedName("trip")
    private TodayTrip todayTrip;

    @SerializedName("vehicles")
    private UserVehicles vehicles;

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public TodayTrip getTodayTrip() {
        return todayTrip;
    }

    public void setTodayTrip(TodayTrip todayTrip) {
        this.todayTrip = todayTrip;
    }

    public UserVehicles getVehicles() {
        return vehicles;
    }

    public void setVehicles(UserVehicles vehicles) {
        this.vehicles = vehicles;
    }
}
