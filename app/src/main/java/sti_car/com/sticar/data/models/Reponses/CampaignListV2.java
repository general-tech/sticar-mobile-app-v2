package sti_car.com.sticar.data.models.Reponses;

import com.google.gson.annotations.SerializedName;

import sti_car.com.sticar.data.models.CampaignItem;
import sti_car.com.sticar.data.models.StatusCampaign;

import java.util.List;

/**
 * Created by femmy on 09/01/2018.
 */

public class CampaignListV2 {
    @SerializedName("campaign_list")
    private List<CampaignItem> mCampaignList;
    @SerializedName("status_campaign")
    private StatusCampaign mStatusCampaign;

    public List<CampaignItem> getCampaignList() {
        return mCampaignList;
    }

    public void setCampaignList(List<CampaignItem> campaignList) {
        mCampaignList = campaignList;
    }

    public StatusCampaign getStatusCampaign() {
        return mStatusCampaign;
    }

    public void setStatusCampaign(StatusCampaign statusCampaign) {
        mStatusCampaign = statusCampaign;
    }
}
