package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by femmy on 13/01/2018.
 */

public class Register implements Serializable{

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("password")
    private String password;

    @SerializedName("confirm_password")
    private String confirmPassword;

    @SerializedName("old_password")
    private String oldPassword;

    @SerializedName("car_brand_id")
    private int carBrandId;

    @SerializedName("car_brand_name")
    private String carBrandName;

    @SerializedName("car_model_id")
    private int carModelId;

    @SerializedName("car_model_name")
    private String carModelName;

    @SerializedName("car_color_id")
    private int carColorId;

    @SerializedName("car_color_name")
    private String carColorName;

    @SerializedName("motorcycle_brand_id")
    private int motorcycleBrandId;

    @SerializedName("motorcycle_brand_name")
    private String motorcycleBrandName;

    @SerializedName("motorcycle_model_id")
    private int motorcycleModelId;

    @SerializedName("motorcycle_model_name")
    private String motorcycleModelName;

    @SerializedName("motorcycle_color_id")
    private int motorcycleColorId;

    @SerializedName("motorcycle_color_name")
    private String motorcycleColorName;

    @SerializedName("bank_code")
    private String bankCode;

    @SerializedName("bank_account_name")
    private String bankAccountName;

    @SerializedName("bank_account_number")
    private String bankAccountNumber;

    @SerializedName("bank_account_verified")
    private boolean bankAccountVerified;

    @SerializedName("device_id")
    private String deviceId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public int getCarBrandId() {
        return carBrandId;
    }

    public void setCarBrandId(int carBrandId) {
        this.carBrandId = carBrandId;
    }

    public int getCarModelId() {
        return carModelId;
    }

    public void setCarModelId(int carModelId) {
        this.carModelId = carModelId;
    }

    public int getCarColorId() {
        return carColorId;
    }

    public void setCarColorId(int carColorId) {
        this.carColorId = carColorId;
    }

    public int getMotorcycleBrandId() {
        return motorcycleBrandId;
    }

    public void setMotorcycleBrandId(int motorcycleBrandId) {
        this.motorcycleBrandId = motorcycleBrandId;
    }

    public int getMotorcycleModelId() {
        return motorcycleModelId;
    }

    public void setMotorcycleModelId(int motorcycleModelId) {
        this.motorcycleModelId = motorcycleModelId;
    }

    public int getMotorcycleColorId() {
        return motorcycleColorId;
    }

    public void setMotorcycleColorId(int motorcycleColorId) {
        this.motorcycleColorId = motorcycleColorId;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getCarBrandName() {
        return carBrandName;
    }

    public void setCarBrandName(String carBrandName) {
        this.carBrandName = carBrandName;
    }

    public String getMotorcycleBrandName() {
        return motorcycleBrandName;
    }

    public void setMotorcycleBrandName(String motorcycleBrandName) {
        this.motorcycleBrandName = motorcycleBrandName;
    }

    public String getCarModelName() {
        return carModelName;
    }

    public void setCarModelName(String carModelName) {
        this.carModelName = carModelName;
    }

    public String getMotorcycleModelName() {
        return motorcycleModelName;
    }

    public void setMotorcycleModelName(String motorcycleModelName) {
        this.motorcycleModelName = motorcycleModelName;
    }


    public String getMotorcycleColorName() {
        return motorcycleColorName;
    }

    public void setMotorcycleColorName(String motorcycleColorName) {
        this.motorcycleColorName = motorcycleColorName;
    }

    public String getCarColorName() {
        return carColorName;
    }

    public void setCarColorName(String carColorName) {
        this.carColorName = carColorName;
    }

    public boolean isBankAccountVerified() {
        return bankAccountVerified;
    }

    public void setBankAccountVerified(boolean bankAccountVerified) {
        this.bankAccountVerified = bankAccountVerified;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
