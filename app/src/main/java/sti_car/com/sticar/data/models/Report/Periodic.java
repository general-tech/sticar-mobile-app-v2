package sti_car.com.sticar.data.models.Report;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by femmy on 17/12/2017.
 */

public class Periodic implements Serializable {

    @SerializedName("allReport")
    private Lifetime mAllReport;

    @SerializedName("dailyReport")
    private Daily mDailyReport;

    @SerializedName("monthlyReport")
    private Monthly mMonthlyReport;

    @SerializedName("todayReport")
    private Today mTodayReport;

    public Lifetime getAllReport() {
        return mAllReport;
    }

    public void setAllReport(Lifetime allReport) {
        mAllReport = allReport;
    }

    public Daily getDailyReport() {
        return mDailyReport;
    }

    public void setDailyReport(Daily dailyReport) {
        mDailyReport = dailyReport;
    }

    public Monthly getMonthlyReport() {
        return mMonthlyReport;
    }

    public void setMonthlyReport(Monthly monthlyReport) {
        mMonthlyReport = monthlyReport;
    }

    public Today getTodayReport() {
        return mTodayReport;
    }

    public void setTodayReport(Today todayReport) {
        mTodayReport = todayReport;
    }
}
