package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 14/12/2017.
 */

public class SendTrack {

    @SerializedName("latitude")
    Double lat;
    @SerializedName("longitude")
    Double lng;
    @SerializedName("distance")
    Integer lastTotalDistance;
    @SerializedName("credit")
    Integer lastTotalCredit;


    public void setLat(Double mLat) {
        this.lat = mLat;
    }

    public Double getLat() {
        return lat;
    }


    public void setLng(Double mLng) {
        this.lng = mLng;
    }

    public Double getLng() {
        return lng;
    }

    public void setLastTotalDistance(Integer mLastTotalDistance) {
        this.lastTotalDistance = mLastTotalDistance;
    }

    public Integer getLastTotalDistance() {
        return lastTotalDistance;
    }


    public void setLastTotalCredit(Integer mLastTotalCredit) {
        this.lastTotalCredit = mLastTotalCredit;
    }

    public Integer getLastTotalCredit() {
        return lastTotalCredit;
    }
}
