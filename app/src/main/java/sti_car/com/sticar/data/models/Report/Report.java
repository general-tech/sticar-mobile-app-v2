package sti_car.com.sticar.data.models.Report;

import java.io.Serializable;

/**
 * Created by femmy on 31/12/2017.
 */

public class Report implements Serializable {

    private Today today;

    private Daily daily;

    private Periodic periodic;

    private Monthly monthly;

    private Lifetime lifetime;

    public Report(){

    }

    public Today getToday() {
        return today;
    }

    public void setToday(Today today) {
        this.today = today;
    }

    public Daily getDaily() {
        return daily;
    }

    public void setDaily(Daily daily) {
        this.daily = daily;
    }

    public Periodic getPeriodic() {
        return periodic;
    }

    public void setPeriodic(Periodic periodic) {
        this.periodic = periodic;
    }

    public Monthly getMonthly() {
        return monthly;
    }

    public void setMonthly(Monthly monthly) {
        this.monthly = monthly;
    }

    public Lifetime getLifetime() {
        return lifetime;
    }

    public void setLifetime(Lifetime lifetime) {
        this.lifetime = lifetime;
    }
}
