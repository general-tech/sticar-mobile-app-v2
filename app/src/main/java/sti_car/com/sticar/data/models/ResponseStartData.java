package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 14/12/2017.
 */

public class ResponseStartData {

    @SerializedName("status")
    private String mStatus;
    @SerializedName("tripId")
    private Long mTripId;
    @SerializedName("tripUUID")
    private String mTripUUID;

    @SerializedName("message")
    String mMessage;


    @SerializedName("distanceTodayInMeter")
    String mDistanceTodayInMeters;


    @SerializedName("benefitToday")
    String mBenefitCreditToday;


    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }


    public String getmDistanceTodayInMeters() {
        return mDistanceTodayInMeters;
    }

    public void setmDistanceTodayInMeters(String mDistanceTodayInMeters) {
        this.mDistanceTodayInMeters = mDistanceTodayInMeters;
    }


    public String getmBenefitCreditToday() {
        return mBenefitCreditToday;
    }

    public void setmBenefitCreditTodays(String mBenefitCreditToday) {
        this.mBenefitCreditToday = mBenefitCreditToday;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Long getTripId() {
        return mTripId;
    }

    public void setTripId(Long tripId) {
        mTripId = tripId;
    }

    public String getTripUUID() {
        return mTripUUID;
    }

    public void setTripUUID(String tripUUID) {
        mTripUUID = tripUUID;
    }
}
