package sti_car.com.sticar.data.models.Reponses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 19/12/2017.
 */

public class ResponseAllStatus {

    @SerializedName("code")
    Long mCode;
    @SerializedName("data")
    AllStatus mData;
    @SerializedName("language")
    String mLanguage;
    @SerializedName("message")
    String mMessage;
    @SerializedName("status")
    Boolean mStatus;

    public Long getmCode() {
        return mCode;
    }

    public void setmCode(Long mCode) {
        this.mCode = mCode;
    }

    public AllStatus getmData() {
        return mData;
    }

    public void setmData(AllStatus mData) {
        this.mData = mData;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public Boolean getmStatus() {
        return mStatus;
    }

    public void setmStatus(Boolean mStatus) {
        this.mStatus = mStatus;
    }
}
