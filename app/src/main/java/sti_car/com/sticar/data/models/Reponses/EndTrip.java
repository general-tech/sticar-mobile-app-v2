package sti_car.com.sticar.data.models.Reponses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 15/12/2017.
 */

public class EndTrip {

    @SerializedName("code")
    Long mCode;
    /* @SerializedName("data")
    DataM xData;*/
    @SerializedName("language")
    String mLanguage;
    @SerializedName("message")
    String mMessage;
    @SerializedName("status")
    Boolean mStatus;

    public Long getmCode() {
        return mCode;
    }

    public void setmCode(Long mCode) {
        this.mCode = mCode;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public Boolean getmStatus() {
        return mStatus;
    }

    public void setmStatus(Boolean mStatus) {
        this.mStatus = mStatus;
    }
}
