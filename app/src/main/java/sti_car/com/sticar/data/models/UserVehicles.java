package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 22/02/2018.
 */

public class UserVehicles {

    @SerializedName("car")
    private Vehicle car;

    @SerializedName("motorcycle")
    private Vehicle motorcycle;

    public Vehicle getCar() {
        return car;
    }

    public void setCar(Vehicle car) {
        this.car = car;
    }

    public Vehicle getMotorcycle() {
        return motorcycle;
    }

    public void setMotorcycle(Vehicle motorcycle) {
        this.motorcycle = motorcycle;
    }
}
