package sti_car.com.sticar.data.models.realm;

import io.realm.RealmObject;

/**
 * Created by femmy on 19/12/2017.
 */

public class TripToday extends RealmObject {

    private String dateToday;
    private String distanceToday;
    private String creditToday;

    public String  getDateToday() {
        return dateToday;
    }

    public void setDateToday(String dateToday) {
        this.dateToday = dateToday;
    }


    public String  getDistanceToday() {
        return distanceToday;
    }

    public void setDistanceToday(String distanceToday) {
        this.distanceToday = distanceToday;
    }

    public String  getCreditToday() {
        return creditToday;
    }

    public void setCreditToday(String creditToday) {
        this.creditToday = creditToday;
    }

    public double getDistanceTodayDouble(){
        return Double.parseDouble(this.distanceToday);
    }

}
