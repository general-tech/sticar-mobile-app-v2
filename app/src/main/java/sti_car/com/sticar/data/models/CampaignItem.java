package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by femmy on 09/01/2018.
 */

public class CampaignItem {
    @SerializedName("advertiserName")
    private String mAdvertiserName;
    @SerializedName("areaName")
    private String mAreaName;
    @SerializedName("campaignId")
    private Long mCampaignId;
    @SerializedName("campaignName")
    private String mCampaignName;
    @SerializedName("campaignUUID")
    private String mCampaignUUID;
    @SerializedName("contractMonth")
    private double mContractMonth;
    @SerializedName("credit")
    private double mCredit;
    @SerializedName("dailyKm")
    private double mDailyKm;
    @SerializedName("endDate")
    private String mEndDate;
    @SerializedName("endHour")
    private String mEndHour;
    @SerializedName("imageUrl")
    private String mImageUrl;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("startDate")
    private String mStartDate;
    @SerializedName("startHour")
    private String mStartHour;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("wrappingId")
    private Long mWrappingId;
    @SerializedName("wrappingName")
    private String mWrappingName;
    @SerializedName("wrappingUUID")
    private String mWrappingUUID;

    public String getAdvertiserName() {
        return mAdvertiserName;
    }

    public void setAdvertiserName(String advertiserName) {
        mAdvertiserName = advertiserName;
    }

    public String getAreaName() {
        return mAreaName;
    }

    public void setAreaName(String areaName) {
        mAreaName = areaName;
    }

    public Long getCampaignId() {
        return mCampaignId;
    }

    public void setCampaignId(Long campaignId) {
        mCampaignId = campaignId;
    }

    public String getCampaignName() {
        return mCampaignName;
    }

    public void setCampaignName(String campaignName) {
        mCampaignName = campaignName;
    }

    public String getCampaignUUID() {
        return mCampaignUUID;
    }

    public void setCampaignUUID(String campaignUUID) {
        mCampaignUUID = campaignUUID;
    }

    public double getContractMonth() {
        return mContractMonth;
    }

    public void setContractMonth(Long contractMonth) {
        mContractMonth = contractMonth;
    }

    public double getCredit() {
        return mCredit;
    }

    public void setCredit(double credit) {
        mCredit = credit;
    }

    public double getDailyKm() {
        return mDailyKm;
    }

    public void setDailyKm(double dailyKm) {
        mDailyKm = dailyKm;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public String getEndDateFormat(){
        String pattern_old = "yyyy-MM-dd";
        String pattern_new = "dd MMM yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern_old);
        SimpleDateFormat simpleDateFormatNew = new SimpleDateFormat(pattern_new);
        try {
            Date date = simpleDateFormat.parse(mEndDate);
            return simpleDateFormatNew.format(date);
        }catch (Exception e){
            return mEndDate;
        }
    }

    public void setEndDate(String endDate) {
        mEndDate = endDate;
    }

    public String getEndHour() {
        return mEndHour;
    }

    public void setEndHour(String endHour) {
        mEndHour = endHour;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public String getStartDateFormat(){
        String pattern_old = "yyyy-MM-dd";
        String pattern_new = "dd MMM yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern_old);
        SimpleDateFormat simpleDateFormatNew = new SimpleDateFormat(pattern_new);
        try {
            Date date = simpleDateFormat.parse(mStartDate);
            return simpleDateFormatNew.format(date);
        }catch (Exception e){
            return mStartDate;
        }
    }

    public void setStartDate(String startDate) {
        mStartDate = startDate;
    }

    public String getStartHour() {
        return mStartHour;
    }

    public void setStartHour(String startHour) {
        mStartHour = startHour;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Long getWrappingId() {
        return mWrappingId;
    }

    public void setWrappingId(Long wrappingId) {
        mWrappingId = wrappingId;
    }

    public String getWrappingName() {
        return mWrappingName;
    }

    public void setWrappingName(String wrappingName) {
        mWrappingName = wrappingName;
    }

    public String getWrappingUUID() {
        return mWrappingUUID;
    }

    public void setWrappingUUID(String wrappingUUID) {
        mWrappingUUID = wrappingUUID;
    }

}
