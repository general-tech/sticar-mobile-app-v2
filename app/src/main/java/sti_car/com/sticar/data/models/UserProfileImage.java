package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 15/02/2018.
 */

public class UserProfileImage {

    @SerializedName("img_blob")
    private String imgBlob;

    public String getImgBlob() {
        return imgBlob;
    }

    public void setImgBlob(String imgBlob) {
        this.imgBlob = imgBlob;
    }
}
