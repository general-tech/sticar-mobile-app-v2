package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by femmy on 14/12/2017.
 */

public class SendDataTracker {
    @SerializedName("tripUUID")
    String tripUUID;
    @SerializedName("trips")
    ArrayList<SendTrack> trips;

    public String getTripUUID() {
        return tripUUID;
    }

    public void setTripUUID(String tripUUID) {
        this.tripUUID = tripUUID;
    }

    public List<SendTrack> getTrips() {
        return trips;
    }

    public void setTrips(ArrayList<SendTrack> trips) {
        this.trips = trips;
    }
}
