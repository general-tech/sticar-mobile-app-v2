package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 17/02/2018.
 */

public class UserProfile {

    @SerializedName("fullname")
    private String fullname;

    @SerializedName("email")
    private String email;

    @SerializedName("phone_number")
    private String phone_number;

    @SerializedName("photo_url")
    private String photoURL;

    @SerializedName("password")
    private String password;

    @SerializedName("confirm_password")
    private String confirmPassword;

    @SerializedName("old_password")
    private String oldPassword;

    @SerializedName("bank_code")
    private String bankCode;

    @SerializedName("bank_account_number")
    private String bankAccountNumber;

    @SerializedName("bank_account_name")
    private String bankAccountName;

    @SerializedName("bank_account_verified")
    private boolean bankAccountVerified;

    @SerializedName("car_brand_id")
    private int carBrandId;

    @SerializedName("car_brand_name")
    private String carBrandName;

    @SerializedName("car_model_id")
    private int carModelId;

    @SerializedName("car_model_name")
    private String carModelName;

    @SerializedName("car_color_id")
    private int carColorId;

    @SerializedName("car_color_name")
    private String carColorName;

    @SerializedName("motorcycle_brand_id")
    private int motorcycleBrandId;

    @SerializedName("motorcycle_brand_name")
    private String motorcycleBrandName;

    @SerializedName("motorcycle_model_id")
    private int motorcycleModelId;

    @SerializedName("motorcycle_model_name")
    private String motorcycleModelName;

    @SerializedName("motorcycle_color_id")
    private int motorcycleColorId;

    @SerializedName("motorcycle_color_name")
    private String motorcycleColorName;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public void setPhoneNumber(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public boolean isBankAccountVerified() {
        return bankAccountVerified;
    }

    public void setBankAccountVerified(boolean bankAccountVerified) {
        this.bankAccountVerified = bankAccountVerified;
    }

    public int getCarModelId() {
        return carModelId;
    }

    public void setCarModelId(int carModelId) {
        this.carModelId = carModelId;
    }

    public String getCarModelName() {
        return carModelName;
    }

    public void setCarModelName(String carModelName) {
        this.carModelName = carModelName;
    }

    public int getCarColorId() {
        return carColorId;
    }

    public void setCarColorId(int carColorId) {
        this.carColorId = carColorId;
    }

    public String getCarColorName() {
        return carColorName;
    }

    public void setCarColorName(String carColorName) {
        this.carColorName = carColorName;
    }

    public int getMotorcycleBrandId() {
        return motorcycleBrandId;
    }

    public void setMotorcycleBrandId(int motorcycleBrandId) {
        this.motorcycleBrandId = motorcycleBrandId;
    }

    public String getMotorcycleBrandName() {
        return motorcycleBrandName;
    }

    public void setMotorcycleBrandName(String motorcycleBrandName) {
        this.motorcycleBrandName = motorcycleBrandName;
    }

    public int getMotorcycleModelId() {
        return motorcycleModelId;
    }

    public void setMotorcycleModelId(int motorcycleModelId) {
        this.motorcycleModelId = motorcycleModelId;
    }

    public int getCarBrandId() {
        return carBrandId;
    }

    public void setCarBrandId(int carBrandId) {
        this.carBrandId = carBrandId;
    }

    public String getCarBrandName() {
        return carBrandName;
    }

    public void setCarBrandName(String carBrandName) {
        this.carBrandName = carBrandName;
    }

    public String getMotorcycleModelName() {
        return motorcycleModelName;
    }

    public void setMotorcycleModelName(String motorcycleModelName) {
        this.motorcycleModelName = motorcycleModelName;
    }

    public int getMotorcycleColorId() {
        return motorcycleColorId;
    }

    public void setMotorcycleColorId(int motorcycleColorId) {
        this.motorcycleColorId = motorcycleColorId;
    }

    public String getMotorcycleColorName() {
        return motorcycleColorName;
    }

    public void setMotorcycleColorName(String motorcycleColorName) {
        this.motorcycleColorName = motorcycleColorName;
    }

    public boolean hasCar(){
        if(this.carBrandId != 0 && this.carModelId !=0 && this.carColorId != 0){
            return true;
        }
        return false;
    }

    public boolean hasMotorCycle(){
        if(this.motorcycleBrandId != 0 && this.motorcycleModelId !=0 && this.motorcycleColorId != 0){
            return true;
        }
        return false;
    }
}
