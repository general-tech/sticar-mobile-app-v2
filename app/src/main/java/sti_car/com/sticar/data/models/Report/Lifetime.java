package sti_car.com.sticar.data.models.Report;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by femmy on 17/12/2017.
 */

public class Lifetime implements Serializable {

    @SerializedName("credit")
    private double credit;

    @SerializedName("distance")
    private double distance;

    public Double getCredit() {
        return credit;
    }

    public void setCredit(double mCredit) {
        credit = mCredit;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(double mDistance) {
        distance = mDistance;
    }
}
