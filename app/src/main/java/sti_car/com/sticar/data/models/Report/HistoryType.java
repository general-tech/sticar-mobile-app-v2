package sti_car.com.sticar.data.models.Report;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 11/01/2018.
 */

public class HistoryType {

    @SerializedName("transactionTypeId")
    private Double transactionTypeId;

    @SerializedName("transactionTypeName")
    private String transactionTypeName;

    public Double getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(Double transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public String getTransactionTypeName() {
        return transactionTypeName;
    }

    public void setTransactionTypeName(String transactionTypeName) {
        this.transactionTypeName = transactionTypeName;
    }
}
