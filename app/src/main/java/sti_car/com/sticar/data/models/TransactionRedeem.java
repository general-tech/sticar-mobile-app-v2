package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 13/02/2018.
 */

public class TransactionRedeem {

    @SerializedName("transaction_id")
    private int transactionID;

    @SerializedName("driver_id")
    private int driverID;

    @SerializedName("transaction_type_id")
    private int transactionTypeID;

    @SerializedName("total")
    private double total;

    @SerializedName("datetime")
    private String datetime;

    @SerializedName("status")
    private int status;

    @SerializedName("img_blob")
    private String imgBlob;

    public int getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(int transactionID) {
        this.transactionID = transactionID;
    }

    public int getDriverID() {
        return driverID;
    }

    public void setDriverID(int driverID) {
        this.driverID = driverID;
    }

    public int getTransactionTypeID() {
        return transactionTypeID;
    }

    public void setTransactionTypeID(int transactionTypeID) {
        this.transactionTypeID = transactionTypeID;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getImgBlob() {
        return imgBlob;
    }

    public void setImgBlob(String imgBlob) {
        this.imgBlob = imgBlob;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
