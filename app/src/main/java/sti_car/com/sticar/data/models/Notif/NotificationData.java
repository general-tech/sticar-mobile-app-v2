package sti_car.com.sticar.data.models.Notif;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by femmy on 10/01/2018.
 */

public class NotificationData {

    @SerializedName("items")
    private List<NotificationItem> items;

    @SerializedName("meta")
    private NotificationMeta meta;

    @SerializedName("offerId")
    private Long offerId;

    @SerializedName("offerUUID")
    private String offerUUID;

    @SerializedName("campaignUUID")
    private String campaignUUID;

    @SerializedName("wrappingUUID")
    private String wrappingUUID;

    public List<NotificationItem> getItems() {
        return items;
    }

    public void setItems(List<NotificationItem> items) {
        this.items = items;
    }

    public NotificationMeta getMeta() {
        return meta;
    }

    public void setMeta(NotificationMeta meta) {
        this.meta = meta;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public String getOfferUUID() {
        return offerUUID;
    }

    public void setOfferUUID(String offerUUID) {
        this.offerUUID = offerUUID;
    }

    public String getCampaignUUID() {
        return campaignUUID;
    }

    public void setCampaignUUID(String campaignUUID) {
        this.campaignUUID = campaignUUID;
    }

    public String getWrappingUUID() {
        return wrappingUUID;
    }

    public void setWrappingUUID(String wrappingUUID) {
        this.wrappingUUID = wrappingUUID;
    }
}
