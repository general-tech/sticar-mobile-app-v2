package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 11/12/2017.
 */

public class UserHome {

    @SerializedName("layout")
    private String layout;

    @SerializedName("params")
    private UserParam params;

    @SerializedName("groupId")
    private String groupId;

    @SerializedName("message")
    private String message;

    public static final String STATUS_LAYOUT_PENDING_JOIN = "pendingJoin";
    public static final String STATUS_LAYOUT_REQUEST_JOIN = "requestJoin";
    public static final String STATUS_LAYOUT_TRIP_STARTED = "tripStart";
    public static final String STATUS_LAYOUT_TRIP_STOPPED = "tripStop";

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public UserParam getParams() {
        return params;
    }

    public void setParams(UserParam params) {
        this.params = params;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
