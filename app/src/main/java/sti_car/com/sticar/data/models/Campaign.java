package sti_car.com.sticar.data.models;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;

/**
 * Created by femmy on 11/12/2017.
 */

public class Campaign {

    public static final int CAMPAIGN_STATUS_ACTIVE = 1;

    public static final int CAMPAIGN_STATUS_STARTED = 2;

    public static final int CAMPAIGN_STATUS_FULL = 4;

    @SerializedName("id")
    private int id;

    @SerializedName("uuid")
    private String uuid;

    @SerializedName("wrapping_uuid")
    private String wrappingUuid;

    @SerializedName("wrapping_credit")
    private double wrappingCredit;

    @SerializedName("name")
    private String name;

    @SerializedName("advertiser_name")
    private String advertiserName;

    @SerializedName("wrapping_type")
    private String wrappingType;

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("start_at")
    private String startAt;

    @SerializedName("end_at")
    private String endAt;

    @SerializedName("area")
    private String area;

    @SerializedName("credit")
    private double credit;

    @SerializedName("advertiserId")
    private Long mAdvertiserId;

    @SerializedName("areaDescription")
    private String mAreaDescription;

    @SerializedName("areaId")
    private Long mAreaId;

    @SerializedName("campaignId")
    private Long mCampaignId;

    @SerializedName("campaignUUID")
    private String mCampaignUUID;

    @SerializedName("contractMonth")
    private Long mContractMonth;

    @SerializedName("createdAt")
    private String mCreatedAt;

    @SerializedName("daily_km")
    private double dailyKm;

    @SerializedName("duration")
    private int duration;

    @SerializedName("endDate")
    private String mEndDate;

    @SerializedName("endHour")
    private String mEndHour;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("quota")
    private Long mQuota;

    @SerializedName("radiusKm")
    private String mRadiusKm;

    @SerializedName("startDate")
    private String mStartDate;

    @SerializedName("startHour")
    private String mStartHour;

    @SerializedName("status")
    private int status;

    @SerializedName("timeZone")
    private Long mTimeZone;

    @SerializedName("updatedAt")
    private String mUpdatedAt;

    @SerializedName("advertiser")
    private Advertiser advertiser;

    @SerializedName("type")
    private CampaignType type;

    @SerializedName("message")
    private String message;

    private static final int ONE_DAY = 86400000;

    private static final int ONE_HOUR = 3600000;

    private static final int ONE_MINUTE = 60000;

    public static String DAYS = "days";

    public String HOURS = "hours";

    public String MINUTES = "minutes";

    public Long getmAdvertiserId() {
        return mAdvertiserId;
    }

    public void setmAdvertiserId(Long mAdvertiserId) {
        this.mAdvertiserId = mAdvertiserId;
    }

    public String getmAreaDescription() {
        return mAreaDescription;
    }

    public void setmAreaDescription(String mAreaDescription) {
        this.mAreaDescription = mAreaDescription;
    }

    public Long getmAreaId() {
        return mAreaId;
    }

    public void setmAreaId(Long mAreaId) {
        this.mAreaId = mAreaId;
    }

    public Long getmCampaignId() {
        return mCampaignId;
    }

    public void setmCampaignId(Long mCampaignId) {
        this.mCampaignId = mCampaignId;
    }

    public String getmCampaignName() {
        return name;
    }

    public void setmCampaignName(String mCampaignName) {
        this.name = mCampaignName;
    }

    public String getmCampaignUUID() {
        return mCampaignUUID;
    }

    public void setmCampaignUUID(String mCampaignUUID) {
        this.mCampaignUUID = mCampaignUUID;
    }

    public Long getmContractMonth() {
        return mContractMonth;
    }

    public void setmContractMonth(Long mContractMonth) {
        this.mContractMonth = mContractMonth;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public double getDailyKm() {
        return dailyKm;
    }

    public void setdailyKm(double mDailyKm) {
        this.dailyKm = mDailyKm;
    }

    public String getmEndDate() {
        return mEndDate;
    }

    public void setmEndDate(String mEndDate) {
        this.mEndDate = mEndDate;
    }

    public String getmEndHour() {
        return mEndHour;
    }

    public void setmEndHour(String mEndHour) {
        this.mEndHour = mEndHour;
    }


    public Long getmQuota() {
        return mQuota;
    }

    public void setmQuota(Long mQuota) {
        this.mQuota = mQuota;
    }

    public String getmRadiusKm() {
        return mRadiusKm;
    }

    public void setmRadiusKm(String mRadiusKm) {
        this.mRadiusKm = mRadiusKm;
    }

    public String getmStartDate() {
        return mStartDate;
    }

    public void setmStartDate(String mStartDate) {
        this.mStartDate = mStartDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStartDateFormat(){
        String pattern_old = "yyyy-MM-dd";
        String pattern_new = "dd MMM yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern_old);
        SimpleDateFormat simpleDateFormatNew = new SimpleDateFormat(pattern_new);
        try {
            Date date = simpleDateFormat.parse(startAt);
            return simpleDateFormatNew.format(date);
        }catch (Exception e){
            return  mStartDate;
        }
    }

    public String getEndDateFormat(){
        String pattern_old = "yyyy-MM-dd";
        String pattern_new = "dd MMM yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern_old);
        SimpleDateFormat simpleDateFormatNew = new SimpleDateFormat(pattern_new);
        try {
            Date date = simpleDateFormat.parse(endAt);
            return simpleDateFormatNew.format(date);
        }catch (Exception e){
            return  mEndDate;
        }
    }

    public boolean isRunning(){
        DateFormat eformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date = new Date();
//        String endTime = mEndDate +" "+mEndHour;
//        String startTime = mStartDate +" "+mStartHour;
        String endTime      = this.endAt;
        String startTime    = this.startAt;
        try {
            Date dEndDate = eformat.parse(endTime);
            Date dStartDate = eformat.parse(startTime);
            if(date.getTime() >=  dStartDate.getTime() && date.getTime() <= dEndDate.getTime()){
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public long getEndDateLong(){
        String pattern_old = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern_old);
        try {
            Date date = simpleDateFormat.parse(mEndDate);
            return date.getTime();
        }catch (Exception e){
            return  0;
        }
    }

    private long getTimesDiff(){
        long diff = 0;
        try {
            String endTime = endAt;
            DateFormat eformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            Date dEndDate = eformat.parse(endTime);
            Date date = new Date();
            diff = dEndDate.getTime() - date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diff;
    }

    public String getElapsedTimesValue(){
        long diff = getTimesDiff();
        Log.d(AppConstant.APP_TAG, "diff : "+diff);
        long retval;
        if(diff > ONE_DAY){
            retval = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } else if(diff > ONE_HOUR){
            retval = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
        } else {
            retval = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
        }
        if(retval > 9){
            return String.valueOf(retval);
        } else {
            return String.valueOf("0"+retval);
        }
    }

    public String getElapsedTimesLabel(){
        long diff = getTimesDiff();
        String label;
        if(diff > ONE_DAY){
            label = DAYS;
        } else if(diff > ONE_HOUR){
            label = HOURS;
        } else {
            label = MINUTES;
        }
        return label;
    }

    public String getAdvertiserName(){
        if(advertiser != null){
            return advertiser.getFullname();
        }
        return "";
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    public Long getmTimeZone() {
        return mTimeZone;
    }

    public void setmTimeZone(Long mTimeZone) {
        this.mTimeZone = mTimeZone;
    }

    public String getmUpdatedAt() {
        return mUpdatedAt;
    }

    public void setmUpdatedAt(String mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getmStartHour() {
        return mStartHour;
    }

    public void setmStartHour(String mStartHour) {
        this.mStartHour = mStartHour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CampaignType getType() {
        return type;
    }

    public void setType(CampaignType type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getWrappingType() {
        return wrappingType;
    }

    public void setWrappingType(String wrappingType) {
        this.wrappingType = wrappingType;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getWrappingUuid() {
        return wrappingUuid;
    }

    public void setWrappingUuid(String wrappingUuid) {
        this.wrappingUuid = wrappingUuid;
    }

    public double getWrappingCredit() {
        return wrappingCredit;
    }

    public void setWrappingCredit(double wrappingCredit) {
        this.wrappingCredit = wrappingCredit;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getStartAt(){
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public void setEndAt(String endAt) {
        this.endAt = endAt;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }
}
