package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 10/01/2018.
 */

public class JoinAdsParam {

    @SerializedName("campaign_uuid")
    private String campaignUUID;
    @SerializedName("wrapping_uuid")
    private String wrappingUUID;

    public String getCampaignUUID() {
        return campaignUUID;
    }

    public void setCampaignUUID(String mCampaignUUID) {
        campaignUUID = mCampaignUUID;
    }

    public String getWrappingUUID() {
        return wrappingUUID;
    }

    public void setWrappingUUID(String mWrappingUUID) {
        wrappingUUID = mWrappingUUID;
    }
}
