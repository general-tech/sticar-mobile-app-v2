package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 14/12/2017.
 */

public class ResponseStart {

    @SerializedName("code")
    private Long mCode;
    @SerializedName("data")
    private ResponseStartData mData;
    @SerializedName("language")
    private String mLanguage;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private Boolean mStatus;

    public Long getCode() {
        return mCode;
    }

    public void setCode(Long code) {
        mCode = code;
    }

    public ResponseStartData getData() {
        return mData;
    }

    public void setData(ResponseStartData data) {
        mData = data;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }
}
