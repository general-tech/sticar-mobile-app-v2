package sti_car.com.sticar.data.models.realm;

import io.realm.RealmObject;

/**
 * Created by femmy on 14/12/2017.
 */

public class DataTrack  extends RealmObject {
    private String mId;
    private String mLat;
    private String mLng;
    private String mLastTotalDistance;
    private String mLastTotalCredit;
    private String mStatus;

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmId() {
        return mId;
    }

    public void setmLat(String mLat) {
        this.mLat = mLat;
    }

    public String getmLat() {
        return mLat;
    }


    public void setmLng(String mLng) {
        this.mLng = mLng;
    }

    public String getmLng() {
        return mLng;
    }

    public void setmLastTotalDistance(String mLastTotalDistance) {
        this.mLastTotalDistance = mLastTotalDistance;
    }

    public String getmLastTotalDistance() {
        return mLastTotalDistance;
    }


    public void setmLastTotalCredit(String mLastTotalCredit) {
        this.mLastTotalCredit = mLastTotalCredit;
    }

    public String getmLastTotalCredit() {
        return mLastTotalCredit;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmStatus() {
        return mStatus;
    }
}
