package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 29/01/2018.
 */

public class MotorColor {

    @SerializedName("code")
    private String code;

    @SerializedName("name")
    private String name;
}
