package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

import sti_car.com.sticar.App;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppUtil;

/**
 * Created by femmy on 11/12/2017.
 */

public class UserAccount {

    @SerializedName("code")
    private Long mCode;

    @SerializedName("language")
    private String mLanguage;

    @SerializedName("message")
    private String mMessage;

    @SerializedName("status")
    private Boolean mStatus;

    @SerializedName("data")
    private UserData data;


    public Long getmCode() {
        return mCode;
    }

    public void setmCode(Long mCode) {
        this.mCode = mCode;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public Boolean getmStatus() {
        return mStatus;
    }

    public void setmStatus(Boolean mStatus) {
        this.mStatus = mStatus;
    }

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    public void setHomeLayout(String layout){
        UserData mUserData  = this.getData();
        UserHome userHome   = mUserData.getmHome();
        userHome.setLayout(layout);
        mUserData.setmHome(userHome);
        this.setData(mUserData);
    }

    public double getCurrentPriceCampaign(){
        double priceAds = 0;
        UserData mData = this.getData();
        if(mData != null){
            Campaign mCampaign  = mData.getCampaign();
            priceAds            = mCampaign.getWrappingCredit();
        }
        return priceAds;
    }

    public String getDailyLimitKM(){
        String dailyKMLimit = "";
        UserData mUserData      = this.getData();
        if(mUserData != null){
            UserHome userHome       = mUserData.getmHome();
            if(userHome != null){
                UserParam userParam     = userHome.getParams();
                if(userParam != null){
                    dailyKMLimit =  userParam.getDailyKM();
                }
            }
        }
        return dailyKMLimit;
    }

    public double getDailyLimitMeter(){
        double limitMeters = 0;
        UserData mData      = this.getData();
        Campaign mCampaign  = mData.getCampaign();
        if(mCampaign != null){
            double dailykm = mCampaign.getDailyKm();
            limitMeters = dailykm * 1000;
        }
        return limitMeters;
    }

    public StatusCampaign getStatusCampaign(){
        if(this.getData() != null){
            return this.getData().getStatusCampaign();
        }
        return null;
    }

    public Campaign getCampaign(){
        // userAccountModel.getData().getStatusCampaign().getCampaign().getEndDateLong();
        if(this.getData() != null){
            return this.getData().getCampaign();
        }
        return null;
    }

    public boolean isLoggedIn(){
        UserData data = this.getData();
        if(data != null){
            String sessType = data.getSessionType();
            if(sessType != null){
                return sessType.equalsIgnoreCase(AppConstant.SESSION_TYPE_DRIVER);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void setSessionAsLoggedIn(){
        UserData mData = getData();
        mData.setSessionType(AppConstant.SESSION_TYPE_DRIVER);
        this.setData(mData);
    }

    public void setCampaign(Campaign campaign){
        UserData mData = this.getData();
        mData.setCampaign(campaign);
        this.setData(mData);

    }

    public Driver getDriver(){
        if(getData() != null){
            return getData().getDriver();
        }
        return null;
    }

    public void setDriver(Driver mdriver){
        UserData mdata = this.data;
        mdata.setDriver(mdriver);
        this.data = mdata;
    }

    public UserProfile getUserProfile(){
        if(getData() != null){
            return getData().getProfile();
        }
        return null;
    }

    public void setProfile(UserProfile userProfile){
        UserData mdata = this.data;
        mdata.setProfile(userProfile);
        this.data = mdata;
    }

}
