package sti_car.com.sticar.data.models.Reponses;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ronald on 11/6/2016.
 */

public class AllStatus implements Serializable {

    @SerializedName("layout")
    String layout;
    @SerializedName("params")
    JsonObject params;
    @SerializedName("groupId")
    String groupId;
    @SerializedName("clientGroupId")
    String clientGroupId;
    @SerializedName("message")
    String message;


    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public JsonObject getParams() {
        return params;
    }


    public void setParams(JsonObject params) {
        this.params = params;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setClientGroupId(String clientGroupId) {
        this.clientGroupId = clientGroupId;
    }

    public String getClientGroupId() {
        return clientGroupId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
