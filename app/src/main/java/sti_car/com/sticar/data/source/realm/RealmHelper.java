package sti_car.com.sticar.data.source.realm;

import android.util.Log;

import sti_car.com.sticar.data.models.realm.TripToday;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.Realm;
import io.realm.Sort;
import sti_car.com.sticar.utils.AppConstant;

/**
 * Created by femmy on 03/01/2018.
 */
@Named("RealmHelper")
public class RealmHelper {

    private Realm realm;

    @Inject
    public RealmHelper(@Named("Realm") Realm realm){
        this.realm = realm;
        if (this.realm.isClosed()){
            Log.d(AppConstant.APP_TAG, "realm is closed ------------------");
            this.realm = Realm.getDefaultInstance();
        }
    }

    public TripToday getTripToday(){
        TripToday mTripToday;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        final String formattedDate = df.format(c.getTime());
        mTripToday = realm.where(TripToday.class)
                .equalTo("dateToday", formattedDate)
                .findAllSorted("distanceToday", Sort.DESCENDING)
                .where()
                .findFirst();
        if(mTripToday == null){
            realm.beginTransaction();
            mTripToday = realm.createObject(TripToday.class);
            mTripToday.setDateToday(formattedDate);
            mTripToday.setDistanceToday("0");
            mTripToday.setCreditToday("0");
            realm.commitTransaction();
        }
        return mTripToday;
    }
}
