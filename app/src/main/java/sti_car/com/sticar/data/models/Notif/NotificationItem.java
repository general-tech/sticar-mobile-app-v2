package sti_car.com.sticar.data.models.Notif;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 10/01/2018.
 */

public class NotificationItem {

    @SerializedName("notification_uuid")
    private String notificationUUID;

    @SerializedName("image_url")
    private String advertiserImage;

    @SerializedName("name")
    private String name;

    @SerializedName("title")
    private String title;

    @SerializedName("category")
    private String category;

    @SerializedName("description")
    private String description;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("imageUrl")
    private String imageUrl;

    @SerializedName("data")
    private NotificationData data;

    @SerializedName("status")
    private String status;

    @SerializedName("subject")
    private String subject;

    @SerializedName("notificationName")
    private String notificationName;

//    public String getNotificationUUID() {
//        return notificationUUID;
//    }
//
//    public void setNotificationUUID(String notificationUUID) {
//        this.notificationUUID = notificationUUID;
//    }
//
//    public String getAdvertiserImage() {
//        return advertiserImage;
//    }
//
//    public void setAdvertiserImage(String advertiserImage) {
//        this.advertiserImage = advertiserImage;
//    }
//
//    public String getAdvertiserName() {
//        return advertiserName;
//    }
//
//    public void setAdvertiserName(String advertiserName) {
//        this.advertiserName = advertiserName;
//    }
//
//    public String getCreatedAt() {
//        return createdAt;
//    }
//
//    public void setCreatedAt(String createdAt) {
//        this.createdAt = createdAt;
//    }
//
//    public String getImageUrl() {
//        return imageUrl;
//    }
//
//    public void setImageUrl(String imageUrl) {
//        this.imageUrl = imageUrl;
//    }
//
//    public NotificationData getData() {
//        return data;
//    }
//
//    public void setData(NotificationData data) {
//        this.data = data;
//    }
//
//    public String getCategory() {
//        return category;
//    }
//
//    public void setCategory(String category) {
//        this.category = category;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getSubject() {
//        return subject;
//    }
//
//    public void setSubject(String subject) {
//        this.subject = subject;
//    }
//
//    public String getNotificationName() {
//        return notificationName;
//    }
//
//    public void setNotificationName(String notificationName) {
//        this.notificationName = notificationName;
//    }
}
