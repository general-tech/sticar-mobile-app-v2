package sti_car.com.sticar.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by femmy on 14/12/2017.
 */

public class ContractUUIDBody {
    @SerializedName("contractUUID")
    String contractUUID;

    public String getContractUUID() {
        return contractUUID;
    }

    public void setContractUUID(String contractUUID) {
        this.contractUUID = contractUUID;
    }
}
