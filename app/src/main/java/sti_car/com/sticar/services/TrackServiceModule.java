package sti_car.com.sticar.services;

import android.content.Context;
import android.content.SharedPreferences;

import sti_car.com.sticar.App;
import sti_car.com.sticar.data.source.realm.RealmHelper;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 14/12/2017.
 */
@Module
public class TrackServiceModule {

    TrackService mService;

    private final CompositeSubscription mSubscription;

    private final Realm realm;

    private RealmHelper mRealHelper;

    private SessionManager mSessionManager;

    private App mApp;

    private NetworkUtil mNetworkUtil;

    private SharedPreferences SP;

    private SharedPrefHelper mSharedPrefHelper;

    TrackServiceModule(TrackService service) {
        mService                = service;
        mApp                    = App.get(mService);
        mSubscription           = new CompositeSubscription();
        realm                   = Realm.getDefaultInstance();
        mSessionManager         = new SessionManager(mApp);
        mNetworkUtil            = new NetworkUtil(mApp);
        SP                      = mApp.getSharedPreferences(AppConstant.SHARED_PREF_KEY, Context.MODE_PRIVATE);
        mSharedPrefHelper       = new SharedPrefHelper(SP);
        mRealHelper             = new RealmHelper(realm);
    }

    @Provides
    TrackService provideMyService() {
        return mService;
    }

    @Provides
    @Named("RetrofitSubscription")
    public CompositeSubscription provideCompositeSubscription(){
        return mSubscription;
    }

    @Provides
    @Named("RealmHelper")
    public RealmHelper provideRealmHelper(){
        return mRealHelper;
    }

    @Provides
    public Realm provideRealm(){
        return realm;
    }

    @Provides
    public SessionManager provideSessionManager(){
        return mSessionManager;
    }

    @Provides
    @Named("SharedPrefHelper")
    public SharedPrefHelper provideSharedPrefHelper(){
        return mSharedPrefHelper;
    }

    @Provides
    public NetworkUtil provideNetworkUtil(){
        return mNetworkUtil;
    }



}
