package sti_car.com.sticar.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sti_car.rschsticar10.R;

import org.json.JSONException;
import org.json.JSONObject;

import sti_car.com.sticar.data.models.Reponses.Response;
import sti_car.com.sticar.data.models.TodayTrip;
import sti_car.com.sticar.utils.AppUtil;
import sti_car.com.sticar.data.models.ContractUUIDBody;
import sti_car.com.sticar.data.models.Reponses.EndTrip;
import sti_car.com.sticar.data.models.Reponses.StartTrip;
import sti_car.com.sticar.data.models.Reponses.TrackFeed;
import sti_car.com.sticar.data.models.SendDataTracker;
import sti_car.com.sticar.data.models.SendTrack;
import sti_car.com.sticar.data.models.Track;
import sti_car.com.sticar.data.models.TripUUIDBody;
import sti_car.com.sticar.data.models.UserAccount;
import sti_car.com.sticar.data.models.UserHome;
import sti_car.com.sticar.data.models.realm.TripToday;
import sti_car.com.sticar.data.source.realm.RealmHelper;
import sti_car.com.sticar.data.source.sp.SPConstant;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.services.DaggerTrackServiceComponent;
import sti_car.com.sticar.ui.main.MainActivity;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.NetworkUtil;
import sti_car.com.sticar.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.Realm;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 12/12/2017.
 */

public class TrackService extends Service implements
        LocationListener,ConnectivityReceiver.ConnectivityReceiverListener {

    @Inject
    SessionManager mSessionManager;

    private UserAccount mUserAccount;

    @Named("RetrofitSubscription")
    @Inject
    CompositeSubscription mSubscriptions;

    @Inject
    NetworkUtil mNetworkUtil;

    @Named("SharedPrefHelper")
    @Inject
    SharedPrefHelper mSPHelper;

    @Named("RealmHelper")
    @Inject
    RealmHelper mRealmHelper;

    @Inject
    Realm realm;
    private LocationManager mLocService;
    private Location mLastLocation;
    private boolean isGPSEnabled;
    private boolean mGpsIsStarted = false;
    private boolean isConnectedInternet = false;
    private Notification.Builder mNotifBuilder;
    private NotificationManager mNM;

    private Notification.Builder mNotifAlertBuilder;
    private Notification mNotifAlert;


    private Notification mNotification;
    private Handler mHandler;

    private boolean isNetworkEnabled;
    private String tripUUID;
    private List<Track> arraySingleTrack        = new ArrayList<>();
    private List<List<Track>> arrayMultiTrack   = new ArrayList<List<Track>>();
    private ArrayList<SendTrack> sendArrayTotal = new ArrayList<SendTrack>();

    private ArrayList<SendTrack> mArrayTrack    = new ArrayList<SendTrack>();
    private ArrayList<ArrayList<SendTrack>> mArrayMultiTrack    = new ArrayList<ArrayList<SendTrack>>();

    private TripUUIDBody tripUUIDBody           = new TripUUIDBody();
    private double latitude                     = 0;
    private double longitude                    = 0;
    private double altitude                     = 0;
    private double priceAds                     = 0;
    private double CurrentTripDistance, CurrentTripCredit, MaxDistanceTripMeters;
    private int lastKMPosted = 0;
    private SendTrack lastTrack;

    private ServiceMessage mServiceMessage;

    private TripToday mTripToday;


    @Override
    public void onCreate() {
        super.onCreate();
        DaggerTrackServiceComponent.builder()
                .trackServiceModule(new TrackServiceModule(this))
                .build().inject(this);
        AppUtil.setConnectivityListener(this);
        mUserAccount            = mSessionManager.getUserAccount();
        mGpsIsStarted           = true;
        isConnectedInternet     = AppUtil.isConnectedToInternet(TrackService.this);

        init();
    }

    private void init(){
        mServiceMessage     = new ServiceMessage(this);
        mNM                 = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        buildNotifTitleBar();
        buildNotifAlert();
        mHandler            = new Handler();
        mLocService         = (LocationManager) getSystemService(LOCATION_SERVICE);
        isGPSEnabled        = mLocService.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled    = mLocService.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            Toast.makeText(this, "Network & GPS is not available",Toast.LENGTH_SHORT).show();
        } else {
            if (isGPSEnabled) {
                // Get last location by GPS
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mLocService.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        AppUtil.getPeriod(),
                        AppUtil.getDistancePeriod(), this);
                if (mLocService != null) {
                    mLastLocation = mLocService
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (mLastLocation != null) {
                        latitude        = mLastLocation.getLatitude();
                        longitude       = mLastLocation.getLongitude();
                        altitude        = mLastLocation.getAltitude();
                    }
                }
            } else {
                // Get last location by Network
                if (mLastLocation == null) {
                    mLocService.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            AppUtil.getPeriod(),
                            AppUtil.getDistancePeriod(), this);
                    if (mLocService != null) {
                        mLastLocation = mLocService
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (mLastLocation != null) {
                            latitude = mLastLocation.getLatitude();
                            longitude = mLastLocation.getLongitude();
                            altitude = mLastLocation.getAltitude();
                        }
                    }
                }
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startTrip();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        mNM.cancel(AppConstant.TRACK_SERVICE_NOTIFICATION_TRIP_START_STOP_ID);
        mNM.cancel(AppConstant.TRACK_SERVICE_NOTIFICATION_ALERT_ID);
        mHandler.removeCallbacksAndMessages(null);
        mSessionManager.setUserAccount(mUserAccount);
        stopTrip();
        gpsStop();
        stopSelf();
        super.onDestroy();
    }

    private void startTrip(){
        mGpsIsStarted = true;
        mNM.notify(AppConstant.TRACK_SERVICE_NOTIFICATION_TRIP_START_STOP_ID,mNotification);
        priceAds                = mUserAccount.getCurrentPriceCampaign();
        mTripToday              = mRealmHelper.getTripToday();
        double totalTripTodayMeter = mTripToday.getDistanceTodayDouble() * 1000;
        if(totalTripTodayMeter >= mUserAccount.getDailyLimitMeter()){
            MaxDistanceTripMeters = 0;
        } else {
            MaxDistanceTripMeters   = mUserAccount.getDailyLimitMeter() - (mTripToday.getDistanceTodayDouble() * 1000);
        }

        Log.d(AppConstant.APP_TAG,"getDailyLimitMeter : "+mUserAccount.getDailyLimitMeter()+ " | (mTripToday.getDistanceTodayDouble() * 1000) : "+(mTripToday.getDistanceTodayDouble() * 1000));
        tripUUID                = mUserAccount.getData().getTodayTrip().getUuid();

        SendTrack sendTrackModel = new SendTrack();
        sendTrackModel.setLat(latitude);
        sendTrackModel.setLng(longitude);
        sendTrackModel.setLastTotalDistance(0);
        sendTrackModel.setLastTotalCredit(0);
        lastTrack = sendTrackModel;
        mArrayTrack.add(lastTrack);
    }


    private void stopTrip(){
        updateTripToday();
        saveArrayTrip();
        if(isConnectedInternet){
            SendDataTracker sendDataTracker = new SendDataTracker();
            sendDataTracker.setTripUUID(tripUUID);
            sendDataTracker.setTrips(mArrayTrack);
            mArrayMultiTrack.add(mArrayTrack);
            mSubscriptions.add(mNetworkUtil.getRetrofitV2().sendTrackerFeed(sendDataTracker)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<TrackFeed>() {
                        @Override
                        public void onCompleted() { }

                        @Override
                        public void onError(Throwable e) {}

                        @Override
                        public void onNext(TrackFeed responsTrackFeed) {
                            postEndTrip();
                        }
                    })
            );
        }
    }

    private void postEndTrip(){
        mSubscriptions.add(mNetworkUtil.getRetrofitV2().stopTrip(tripUUIDBody)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<EndTrip>() {
                @Override
                public void onCompleted() {}

                @Override
                public void onError(Throwable e) {}

                @Override
                public void onNext(EndTrip endTrip) {
                    clearArrayTrip();
                    Toast.makeText(TrackService.this,"Trip Stop",Toast.LENGTH_SHORT).show();
                }
            })
        );
    }

    private void saveArrayTrip() {
        Gson gson = new Gson();
        String jsonTripArray = gson.toJson(mArrayTrack);
        mSPHelper.put(SPConstant.SP_KEY_UNPOSTED_TRIP_ARRAYTRACK, jsonTripArray);
        mSPHelper.put(SPConstant.SP_KEY_UNPOSTED_TRIP_TRIPUUID, tripUUID);
    }

    private void clearArrayTrip(){
        mSPHelper.deleteSavedData(SPConstant.SP_KEY_UNPOSTED_TRIP_ARRAYTRACK);
        mSPHelper.deleteSavedData(SPConstant.SP_KEY_UNPOSTED_TRIP_TRIPUUID);
    }


    private void calculateTrip(){
        // init single track
        double mTrackDistance       = getCurrentTripDistance();
        CurrentTripDistance         = lastTrack.getLastTotalDistance() + mTrackDistance;

        if(CurrentTripDistance > MaxDistanceTripMeters){
            CurrentTripCredit           = (MaxDistanceTripMeters / 1000) * priceAds;
            mServiceMessage.sendUpdateDailyLimit();
        } else {
            CurrentTripCredit           = (CurrentTripDistance / 1000) * priceAds;
        }

        SendTrack trackModel = new SendTrack();
        trackModel.setLat(latitude);
        trackModel.setLng(longitude);
        trackModel.setLastTotalDistance(Double.valueOf(CurrentTripDistance).intValue());
        trackModel.setLastTotalCredit(Double.valueOf(CurrentTripCredit).intValue());
        mArrayTrack.add(trackModel);
        lastTrack = trackModel;
        checkAndPushDataPerKM();
        saveArrayTrip();
        updateNotifTrip();
        mServiceMessage.SendUpdateTotal(CurrentTripDistance,CurrentTripCredit);
    }

    private void checkAndPushDataPerKM(){
        int KMnth                   = Double.valueOf(Math.floor(CurrentTripDistance / 1000)).intValue();
        if(KMnth > lastKMPosted){
            lastKMPosted = KMnth;
            if(isConnectedInternet){
                SendDataToServer();
            }
        }
    }

    private void showNotifAlert(CharSequence msgText ){
        mNotifAlertBuilder.setContentText(msgText);
        mNM.notify(AppConstant.TRACK_SERVICE_NOTIFICATION_ALERT_ID, mNotifAlertBuilder.build());
    }

    private void hideNotifAlert(){
        mNM.cancel(AppConstant.TRACK_SERVICE_NOTIFICATION_ALERT_ID);
    }

    private void updateTripToday(){
        double newTotalDistanceToday, newTotalCreditToday;
        newTotalDistanceToday          = mTripToday.getDistanceTodayDouble() + (CurrentTripDistance/1000);
        if(CurrentTripDistance > MaxDistanceTripMeters){
            newTotalCreditToday            = (mUserAccount.getDailyLimitMeter()/1000) * priceAds;
        } else {
            newTotalCreditToday            = newTotalDistanceToday * priceAds;
        }
        realm.beginTransaction();
        mTripToday.setDistanceToday(String.valueOf(newTotalDistanceToday));
        mTripToday.setCreditToday(String.valueOf(newTotalCreditToday));
        realm.commitTransaction();
        mServiceMessage.sendUpdateTotalDaily(newTotalDistanceToday,newTotalCreditToday);
    }

    private void updateNotifTrip(){
        CharSequence contentText = "Distance : "+AppUtil.formatDistance((CurrentTripDistance/1000))
                +"KM -  Credits :"+AppUtil.formatCurrencyIDR(CurrentTripCredit);
        mNotifBuilder.setContentText(contentText);
        mNM.notify(AppConstant.TRACK_SERVICE_NOTIFICATION_TRIP_START_STOP_ID, mNotifBuilder.build());
    }

    private void buildNotifAlert(){
        CharSequence text = "Alert";
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
        mNotifAlertBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_logo_sticar_bw)
                .setTicker(text)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(getText(R.string.app_name))
                .setContentText(text)
                .setContentIntent(contentIntent)
                .setVibrate(new long[] { 1000, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setOngoing(true);
        mNotifAlert = mNotifAlertBuilder.build();
    }

    private void buildNotifTitleBar(){
        CharSequence text = "Running";
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
        mNotifBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_logo_sticar_bw)
                .setTicker(text)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(getText(R.string.app_name) + " Trip is running...")
                .setContentText(text)
                .setContentIntent(contentIntent)
                .setOngoing(true);
        mNotification = mNotifBuilder.build();
    }

    private void SendDataToServer(){
        SendDataTracker sendDataTracker = new SendDataTracker();
        sendDataTracker.setTripUUID(tripUUID);
        Log.d(AppConstant.APP_TAG, "TripUUID : "+tripUUID);
        final ArrayList<SendTrack> mSendTrackSent = mArrayTrack;
        sendDataTracker.setTrips(mArrayTrack);
        mArrayMultiTrack.add(mSendTrackSent);
        mArrayTrack = new ArrayList<SendTrack>();

        mSubscriptions.add(mNetworkUtil.getRetrofitV2().sendTrackerFeed(sendDataTracker)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<TrackFeed>() {
                @Override
                public void onCompleted() {
                    //sendMessage("hideDialog", "");
                }

                @Override
                public void onError(Throwable e) {
                    // sendMessage("hideDialog", "");
                }

                @Override
                public void onNext(TrackFeed responsTrackFeed) {
                    // if send data status not equal 200 then throwback trank sent to array track in order to try resend in the next push
                    if(Integer.parseInt(responsTrackFeed.getCode()) != 200){
                        ArrayList<SendTrack> mArrayTemp = new ArrayList<SendTrack>();
                        mArrayTemp.addAll(mSendTrackSent);
                        mArrayTemp.addAll(mArrayTrack);
                        mArrayTrack = mArrayTemp;
                        Toast.makeText(TrackService.this,"Push data track error, error code : "+responsTrackFeed.getCode(),Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(TrackService.this,"Push data track success.",Toast.LENGTH_SHORT).show();
                    }

                }
            })
        );
    }

    private double getCurrentTripDistance(){
        double currTripDist;
        Location locPrev    = new Location("locationA");
        locPrev.setLatitude(lastTrack.getLat());
        locPrev.setLongitude(lastTrack.getLng());

        Location locCurr    = new Location("locationA");
        locCurr.setLatitude(latitude);
        locCurr.setLongitude(longitude);
        currTripDist        = locPrev.distanceTo(locCurr);
        return currTripDist;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        boolean LOCATION_IS_MOCKED;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            LOCATION_IS_MOCKED  = location.isFromMockProvider();
        } else {
            LOCATION_IS_MOCKED = !Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION).equals("0");
        }

        if(LOCATION_IS_MOCKED && AppConstant.APP_IS_PRODUCTION){
            Toast.makeText(this, "Location is mocked ",Toast.LENGTH_SHORT).show();
        } else {
            mLastLocation       = location;
            latitude            = mLastLocation.getLatitude();
            longitude           = mLastLocation.getLongitude();
            altitude            = mLastLocation.getAltitude();
            calculateTrip();
//            if (mLastLocation != null) {
//                speed = 0;
//                if (location.getTime() - mLastLocation.getTime() > 0) {
//                    speed = Math.floor(mLastLocation.distanceTo(location) / ((location.getTime() - mLastLocation.getTime()) / 1000) * 3.6);
//                }
//                mAzimuth        = mLastLocation.bearingTo(location);
//            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(AppConstant.APP_TAG,"onStatusChanged : provider-"+provider+", status-"+status);
    }

    @Override
    public void onProviderEnabled(String provider) {
        hideNotifAlert();
    }

    @Override
    public void onProviderDisabled(String provider) {
        CharSequence msg = "GPS not available. the trip will be stopped and saved locally.";
        showNotifAlert(msg);
        //this.onDestroy();
    }

    private synchronized void gpsStop() {
        if (mGpsIsStarted) {
            if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mLocService.removeUpdates(this);
            mGpsIsStarted = false;
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        isConnectedInternet = isConnected;
        if(!isConnectedInternet) {
            CharSequence msg = "No internet connection. Trip will be saved locally";
            showNotifAlert(msg);
        } else {
            hideNotifAlert();
        }
    }

    private class ServiceMessage {

        Context mCtx;

        Intent mIntent;

        ServiceMessage(Context mContext){
            mCtx = mContext;
        }

        void SendUpdateTotal(double totalDistance, double totalCredit){
            JSONObject obj      = new JSONObject();
            try {
                obj.put("distance",AppUtil.formatDistance(totalDistance /1000)+" KM");
                obj.put("benefit",AppUtil.formatCurrencyIDR(totalCredit));
                send(AppConstant.TRACK_SERIVCE_MSG_KEY_UPDATE_TOTAL, obj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        void sendUpdateTotalDaily(double mTotalDistance, double mTotalCredit){
            JSONObject obj      = new JSONObject();
            try {
                Log.d(AppConstant.APP_TAG, "mTotalDistance = "+mTotalDistance);
                obj.put("distance",AppUtil.formatDistance(mTotalDistance)+" KM");
                obj.put("benefit",AppUtil.formatCurrencyIDR(mTotalCredit));
                send(AppConstant.TRACK_SERIVCE_MSG_KEY_UPDATE_TOTAL_TODAY, obj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        void sendUpdateDailyLimit(){
            int dailylimit = (int) mUserAccount.getDailyLimitMeter()/1000;
            send(AppConstant.TRACK_SERIVCE_MSG_KEY_UPDATE_LIMIT,"Today trip has reached daily limit ("+dailylimit+" KM)");
        }

        void send(String type, String value){
            mIntent = new Intent(AppConstant.INTENT_FILTER_KEY_GPS_EVENT);
            mIntent.putExtra("type", type);
            mIntent.putExtra("value", value);
            LocalBroadcastManager.getInstance(mCtx).sendBroadcast(mIntent);
        }
    }
}
