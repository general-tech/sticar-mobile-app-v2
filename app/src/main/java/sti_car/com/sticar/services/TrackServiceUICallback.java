package sti_car.com.sticar.services;

import android.location.Location;

/**
 * Created by femmy on 14/12/2017.
 */

public interface TrackServiceUICallback {

    void onNetworkGPSNotAvailable();

    void onInitialized(double lat, double lng, double alt);

    void onServiceStart();

    void locationIsMocked();

    void trackServiceOnLocationChanged(Location location);

    void onTripStarted();

}
