package sti_car.com.sticar.services;

import dagger.Component;

/**
 * Created by femmy on 14/12/2017.
 */

@Component(modules=TrackServiceModule.class)
public interface TrackServiceComponent {
    void inject(TrackService service);
}
