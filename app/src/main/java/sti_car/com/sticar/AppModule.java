package sti_car.com.sticar;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import sti_car.com.sticar.data.source.realm.RealmHelper;
import sti_car.com.sticar.data.source.sp.SharedPrefHelper;
import sti_car.com.sticar.utils.AppConstant;
import sti_car.com.sticar.utils.AppHelper;
import sti_car.com.sticar.utils.SessionManager;
import sti_car.com.sticar.utils.annot.PerActivity;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by femmy on 08/12/2017.
 */
@Module
public class AppModule {

    private final App mApp;

    private final CompositeSubscription mSubscription;

    private Realm realm;

    private SharedPrefHelper SPHelper;

    private SharedPreferences SP;

    private AppHelper mAppHelper;


    private RealmHelper mRealmHelper;

    public AppModule(App mApp) {
        this.mApp = mApp;
        mSubscription = new CompositeSubscription();
        SP = mApp.getSharedPreferences(AppConstant.SHARED_PREF_KEY, Context.MODE_PRIVATE);
        SPHelper = new SharedPrefHelper(SP);
        mAppHelper = new AppHelper(mApp);

        Realm.init(mApp);
        realm = Realm.getDefaultInstance();

        mRealmHelper = new RealmHelper(realm);

    }

    @Provides
    @Singleton
    public App provideApp() {
        return mApp;
    }

    @Provides
    @Named("AppContext")
    public Context provideContext(){
        return mApp;
    }

    @Provides
    @Named("RetrofitSubscription")
    public CompositeSubscription provideCompositeSubscription(){
        return mSubscription;
    }


    @Provides
    @PerActivity
    public SessionManager provideSessionManager(){
        return new SessionManager(mApp);
    }

    @Provides
    SharedPreferences provideSharedPreferences() {
        return SP;
    }

    @Provides
    @Named("Realm")
    Realm provideRealm(){
        if(realm == null || realm.isClosed()){
            Realm.init(mApp);
            realm = Realm.getDefaultInstance();
        }
        return realm;
    }

    @Provides
    @Named("SharedPrefHelper")
    SharedPrefHelper provideSharedPrefHelper(){
        return SPHelper;
    }

    @Provides
    @Named("AppHelper")
    AppHelper provideAppHelper(){
        return mAppHelper;
    }

    @Provides
    @Named("RealmHelper")
    RealmHelper provideRealmHelper(){
        return mRealmHelper;
    }
}
